%% Create_MechControlInit_MAT
% -------------------------------------------------------------------------
% Create_MechControlInit_MAT creates a Matlab workspace file (.mat) for 
% initialization of nms_model2G_EcoWalk.slx. The initialization via .mat 
% files is used for optimization only.
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       15-12-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Clear Workspace
clear; 
close all; 
clc;

% ---------------- Prevent calling script by chance; comment out when using 
error ('Create_MechControlInit_MAT: Script call not intended');


%% Load Files 
% ---- Mechanics Config. and Control Inputs from original model (Geyer2010)
run('nms_model_MechInit.m')
run('nms_model_ControlInit.m')

% ----------------------- Init Custom Parameters (nms_model2G_Buchmann.slx) 
% run('ground_param_1to3seg_FGCM.m')
run('ground_param_nonlinear_Geyer.m')
run('nms_model_MechInit_AddOns.m')

% ------------------------------------------ Adapt Parameter Set if desired
% case_no = 1; % No adaptations
% run('nms_model_ControlAdaptation.m');
% clear case_no

%% Store Var-Names for Preallocation in Optimization
init_vars = whos;

save ('nms_model_MechControlInit_FGCM', init_vars(:).name);
save ('nms_model_MechControlInit_var_names_FGCM', 'init_vars');

%% Clear Workspace
clear; 
close all; 
clc;