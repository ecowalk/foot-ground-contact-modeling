%% Ground_param_1to3seg_FGCM.m
% -------------------------------------------------------------------------
% Ground_param_1to3seg_FGCM.m - Set mechanical parameters for the six 
% different FGCMs in the library (1s-lA, 1s-hA, 2s-TJ, 2s-MTJ, 3s-nW, 3s-W)
%
% This parameter setup includes: 
%    1. foot segment geometries, masses and inertias,
%    2. ground interaction parameters.
%    3. parameters for plantar fascia(=aponeurosis) and plantar ligaments
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      05-05-2022, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------
% #########################################################################
% References:
%
% [1] Geyer, Hartmut; Herr, Hugh (2010): A muscle-reflex model that encodes 
%     principles of legged mechanics produces human walking dynamics and 
%     muscle activities. In: IEEE Transactions on Neural Systems and 
%     Rehabilitation Engineering : a Publication of the IEEE Engineering in 
%     Medicine and Biology Society 18 (3), S. 263–273. 
%     DOI: 10.1109/TNSRE.2010.2047592.
% [2] Natali, A. N., Pavan, P. G., and Stecco, C. “A constitutive model for 
%     the mechanical characterization of the plantar fascia”. In: 
%     Connective Tissue Research 51.5 (Feb. 2010), pp. 337–346. 
%     DOI: 10.3109/03008200903389127.
% [3] Brown, P. “Contact Modelling for Forward Dynamics of Human Motion”. 
%     PhD thesis, Feb. 2017.
%
%
% #########################################################################

%% Ankle joint connections for relaxed foot

% height of the ankle joint above the floor (no need to change for 1s-lA)
Foot.ankle_height = 0.08; 

% To midtarsal joint (MTJ) 
x_mtj =  0.07;          %[m] x distance from ankle joint to MTJ
y_mtj = -0.014;         %[m] y distance from ankle joint to MTJ

% To metatarsophalangeal joint (=toe joint, TJ) (mtpj) 
x_mtpj =  0.13;         %[m] x distance from ankle joint to TJ
y_mtpj = -0.06;         %[m] y distance from ankle joint to TJ

%% Ankle joint to contact elements (CE) for relaxed foot

% only x_needed
x_CE_heel = -0.04;
x_CE_ball =  0.13;
x_CE_toe  =  0.185;

%% CEs radius
Foot.R.heel = 0.05;         %[m] heel circle radius
Foot.R.ball = 0.0573;       %[m] heel circle radius
Foot.R.toe  = 0.0;          %[m] toe circle radius

%% Ankle joint to PA/PL attachements for relaxed foot
x_PA_heel = -0.02;
y_PA_heel = -0.06;
x_PA_ball =  0.13;
y_PA_ball = -0.06;

x_PL_heel = -0.02;
y_PL_heel = -0.06;
x_PL_ball =  0.13;
y_PL_ball = -0.06;

%% Convert geometry for the relaxed foot into local coordinate systems
Foot.midtarsal.X  = x_mtj;          %[m] x distance from ankle joint to MTJ
Foot.midtarsal.Y  = y_mtj;          %[m] y distance from ankle joint to MTJ
Foot.metatarsal.X = x_mtpj - x_mtj; %[m] x distance from MTJ to TJ   
Foot.metatarsal.Y = y_mtpj - y_mtj; %[m] y distance from MTJ to TJ

Foot.CE.heel.X  = x_CE_heel;                                %[m] x distance from ankle joint to CE
Foot.CE.heel.Y  = Foot.R.heel - Foot.ankle_height;          %[m] y distance from ankle joint to CE
Foot.CE.ball.X  = x_CE_ball   - x_mtj;                      %[m] x distance from MTJ to CE
Foot.CE.ball.Y  = Foot.R.ball - Foot.ankle_height - y_mtj;  %[m] y distance from MTJ to CE
Foot.CE.ball.Xb = 0;                                        %[m] x distance from MTJ to CE for FGCMs with toe
Foot.CE.ball.Yb = Foot.R.ball - Foot.ankle_height - y_mtpj; %[m] y distance from MTJ to CE for FGCMs with toe
Foot.CE.toe.X   = x_CE_toe    - x_mtpj ;                    %[m] x distance from TJ to CE
Foot.CE.toe.Y   = Foot.R.toe  - Foot.ankle_height - y_mtpj; %[m] y distance from TJ to CE

Foot.PA.heel.X = x_PA_heel;         %[m] x distance from ankle joint to PA attachement
Foot.PA.heel.Y = y_PA_heel;         %[m] y distance from ankle joint to PA attachement
Foot.PA.ball.X = x_PA_ball - x_mtj; %[m] x distance from MTJ to PA attachement
Foot.PA.ball.Y = y_PA_ball - y_mtj; %[m] y distance from MTJ to PA attachement

Foot.PL.heel.X = x_PL_heel;         %[m] x distance from ankle joint to PL attachement
Foot.PL.heel.Y = y_PL_heel;         %[m] y distance from ankle joint to PL attachement
Foot.PL.ball.X = x_PL_ball - x_mtj; %[m] x distance from MTJ to PL attachement
Foot.PL.ball.Y = y_PL_ball - y_mtj; %[m] y distance from MTJ to PL attachement

%% FGCM mass and inertia
Foot.CG.heel.X =  0.05;
Foot.CG.heel.Y = -0.05;
Foot.CG.ball.X =  0;
Foot.CG.ball.Y =  0;
Foot.CG.toe.X  =  0;
Foot.CG.toe.Y  =  0;

Foot.m.heel = 1.16;
Foot.m.ball = 0;
Foot.m.toe  = 0;
Foot.m_F    = Foot.m.heel + Foot.m.ball + Foot.m.toe;

Foot.in.heel = 0.005;
Foot.in.ball = 0;
Foot.in.toe  = 0;

%% Parameters for plantar aponeurosis (PA) and plantar ligaments (PL)
Foot.PA.R_toe = 0.01;  
Foot.PA.phi_0 = deg2rad(0); % TJ angle in relaxed state
Foot.PA.l_0   = norm([x_PA_heel y_PA_heel]-[x_PA_ball y_PA_ball]) - ...
                                               Foot.PA.phi_0*Foot.PA.R_toe;
Foot.PA.k     = 340;
Foot.PA.alpha = 9;
Foot.PA.A     = 50;
Foot.PA.d     = 3;

Foot.PL.l_0   = norm([x_PL_heel y_PL_heel]-[x_PL_ball y_PL_ball]) - ...
                                               Foot.PA.phi_0*Foot.PA.R_toe;
Foot.PL.k     = 340;
Foot.PL.alpha = 9;
Foot.PL.A     = 120;
Foot.PL.d     = 30;

%% Limits for TJ; stiffness and damping coefficients for the bumpstop
Foot.TJlimit.low  = deg2rad(-5);
Foot.TJlimit.high = deg2rad(90);

% Coefficients (from [1])
Foot.TJlimit.clow  = 0.3 / (pi/180); %[Nm/rad] soft block reference joint stiffness;
Foot.TJlimit.chigh = 0.0 / (pi/180); %[Nm/rad] soft block reference joint stiffness;
Foot.TJlimit.dlow  = 1 * pi/180;     %[rad/s]  soft block maximum joint stop relaxation speed
Foot.TJlimit.dhigh = 0 * pi/180;     %[rad/s]  soft block maximum joint stop relaxation speed

% *********************************************************************** %
% 2. Ground Interaction Model % 
% *********************************************************************** %

% -------------------------------------------------------------------------
% 2.1 Vertical component
% -------------------------------------------------------------------------

% -------------------------------  stiffness of vertical ground interaction
Ground.k_gy = (2*(Foot.m_F + Body.m_S + Body.m_T) + Body.m_HAT)*g/0.01;%[N/m]
Ground.v_gy_max = 0.03; %[m/s] max. relax. speed vertical ground interaction


% -------------------------------------------------------------------------
% 2.2 Horizontal component
% -------------------------------------------------------------------------

Ground.mu_slide = 0.8;  %sliding friction coefficient
Ground.vLimit   = 0.01; %[m/s] sliding to stiction transition velocity limit

% --------------------------------- stiffness of horizontal ground stiction
Ground.k_gx = (2*(Foot.m_F + Body.m_S +Body.m_T) + Body.m_HAT)*g/0.1; %[N/m]

Ground.v_gx_max = 0.03; %[m/s] max relax. speed horizontal ground stiction
Ground.mu_stick = 0.9;  %stiction to sliding transition coefficient

