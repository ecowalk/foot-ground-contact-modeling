%% Ground_param_nonlinear_Geyer.m
% -------------------------------------------------------------------------
% Ground_param_nonlinear_Geyer.m - Set mechanical parameters of non-linear, 
% one segmented foot-ground contact model described in Geyer 2010
%
% This parameter setup includes: 
%    1. foot segment geometries, masses and inertias,
%    2. ground interaction parameters.
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       05-05-2022, Munich
% Last modified:    23-11-2022, Munich 
% 
% -------------------------------------------------------------------------
% #########################################################################
% References:
%
% [1] Geyer, Hartmut; Herr, Hugh (2010): A muscle-reflex model that encodes 
%     principles of legged mechanics produces human walking dynamics and 
%     muscle activities. In: IEEE Transactions on Neural Systems and 
%     Rehabilitation Engineering : a Publication of the IEEE Engineering in 
%     Medicine and Biology Society 18 (3), S. 263–273. 
%     DOI: 10.1109/TNSRE.2010.2047592.
%
% #########################################################################

% *********************************************************************** %
% 1. BIPED SEGMENTATION 
% *********************************************************************** %
% -------------------------------------------------------------------------
% 1.1 General Foot Segment
% -------------------------------------------------------------------------
% Note: CS1 is position of fore-foot impact point and contacts the 
% adjoining ground.

Foot.L_F   = 0.2;   %[m] foot length
Foot.D1G_F = 0.14;  %[m] distance CS1 to CG (local center of gravity)
                    % critical to project
Foot.D12_F = 0.16;  %[m] distance CS1 to C2 (ankle joint)
Foot.D13_F = 0.20;  %[m] distance CS1 to CS3 (heel pad impact point)
Foot.m_F   = 1.25;  %[kg] foot mass
Foot.In_F  = 0.005; %[kg*m^2] foot inertia with respect to axis ball-CG-heel
                    % (scaled from other papers)

% *********************************************************************** %
% 2. Ground Interaction Model % 
% *********************************************************************** %
% -------------------------------------------------------------------------
% 2.1 Vertical component
% -------------------------------------------------------------------------
% -------------------------------- stiffness of vertical ground interaction
Ground.k_gy = (2*(Foot.m_F +Body.m_S +Body.m_T) + Body.m_HAT)*g/0.01;%[N/m]
Ground.v_gy_max = 0.03; %[m/s] max. relax. speed vertical ground interaction

% -------------------------------------------------------------------------
% 2.2 Horizontal component
% -------------------------------------------------------------------------
Ground.mu_slide = 0.8;  %      sliding friction coefficient
Ground.vLimit   = 0.01; %[m/s] sliding to stiction transition velocity limit
% --------------------------------- stiffness of horizontal ground stiction
Ground.k_gx = (2*(Foot.m_F +Body.m_S +Body.m_T) + Body.m_HAT)*g/0.1; %[N/m]

Ground.v_gx_max = 0.03; %[m/s] max relax. speed horizontal ground stiction
Ground.mu_stick = 0.9;  %      stiction to sliding transition coefficient
