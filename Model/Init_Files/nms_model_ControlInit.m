%% NMS_Model_ControlInit.m 
% -------------------------------------------------------------------------
% nms_model_ControlInit.m  -  Set neural control parameters of the model
%                             as well as initial conditions and simulink 
%                             control parameters.
% -------------------------------------------------------------------------
% 
% © H.Geyer, 5 October 2006
% Annotations, Structs by Alexandra Buchmann, 
%                                           Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       05-10-2006
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

% *********************************************************************** %
% 1. General Neural Control Parameters 
% *********************************************************************** %
% feedback delays
Neural.LongDelay  = 0.020; % ankle joint muscles [s]
Neural.MidDelay   = 0.010; % knee joint muscles [s]
Neural.ShortDelay = 0.005; % hip joint muscles [s]


% *********************************************************************** %
% 2. Specific Control Parameters 
% *********************************************************************** %
% -------------------------------------------------------------------------
% 2.1 Stance-Leg Feedback Control 
% -------------------------------------------------------------------------
% soleus (self, F+)
Reflex.GainSOL      =  1.2/Muscle.FmaxSOL; %[1/N]
Reflex.PreStimSOL   =         0.01; %[]

% soleus on tibialis anterior (F-)
Reflex.GainSOLTA    =  0.3/Muscle.FmaxSOL; %[1/N]
Reflex.PreStimTA    =          0.01; %[]

% tibialis (self, L+, stance & swing)
Reflex.GainTA       =         1.1;   %[]
Reflex.LceOffsetTA  =       1-0.5*Muscle.w; %[loptTA]

% gastrocnemius (self, F+)
Reflex.GainGAS      =   1.1/Muscle.FmaxGAS; %[1/N] 
Reflex.PreStimGAS   =         0.01; %[]

% vasti group (self, F+)
Reflex.GainVAS      =    1.15/Muscle.FmaxVAS; %[1/N]
Reflex.PreStimVAS   =         0.08; %[]

% knee overextension on vasti (Phi-, directional)
Reflex.GainKneOverExt = 2;%
Reflex.KneePh23Offset = 170*pi/180;

% swing initiation
Reflex.K_swing = 0.25;%0.25

% -------------------------------------------------------------------------
% 2.1 Swing-leg Feedback Control 
% -------------------------------------------------------------------------
% Fly
% -------------------------------------------------------------------------
% hip flexors (self, L+, swing)
Reflex.LceOffsetHFL =        0.65; %[loptHFL]
Reflex.PreStimHFL   =        0.01; %[] 
Reflex.GainHFL      =         0.35; %[] 

% balance offset shift (Delta Theta at take-off)
Reflex.GainDeltaTheta = 2 /100*180/pi; %[percent/deg]

% Catch
% -----
% hip flexor from hamstring stretch reflex (L-, swing)
Reflex.LceOffsetHAM =        0.85; %[loptHAM]
Reflex.GainHAMHFL   =           4; %[]

% hamstring group (self, F+, swing)
Reflex.GainHAM      =  0.65/Muscle.FmaxHAM; %[1/N]
Reflex.PreStimHAM   =         0.01; %[]

% gluteus group (self, F+, swing)
Reflex.GainGLU      =  0.4/Muscle.FmaxGLU; %[1/N]
Reflex.PreStimGLU   =         0.01; %[]

% -------------------------------------------------------------------------
% 2.2 Stance-Leg HAT Reference Posture PD-Control
% -------------------------------------------------------------------------
% stance hip joint position gain
Reflex.PosGainGG   = 1/(30*pi/180); %[1/rad]

% stance hip joint speed gain
Reflex.SpeedGainGG = 0.2; %[s/rad] 

% stance posture control muscles pre-stimulation
Reflex.PreStimGG   = 0.05; %[]

% stance reference posture
Reflex.Theta0      = 6*pi/180; %[rad]


% *********************************************************************** %
% 3. Initial Conditions and Simulation Control 
% *********************************************************************** %
% -------------------------------------------------------------------------
% 3.1 Initial Conditions
% -------------------------------------------------------------------------
% initial locomotion speed
Init.vx0 = 1.3; %[m/s] 

% left (stance) leg ankle, knee and hip joint angles
Init.Lphi120  =  85*pi/180; %[rad]
Init.Lphi230  = 175*pi/180; %[rad]
Init.Lphi340  = 175*pi/180; %[rad]

% right (swing) leg ankle, knee and hip joint angles
Init.Rphi120  =  90*pi/180; %[rad]
Init.Rphi230  = 175*pi/180; %[rad]
Init.Rphi340  = 140*pi/180; %[rad]

% -------------------------------------------------------------------------
% 3.2 Simulation Control
% -------------------------------------------------------------------------
% Animations
% -------------------------------------------------------------------------
% integrator max time step
ts_max = 1e-1;




