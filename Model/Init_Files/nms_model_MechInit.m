%% NMS_Model_MechInit.m 
% -------------------------------------------------------------------------
% NMS_Model_MechInit.m  - Set mechanical parameters of the nms-model
%
% This parameter setup includes: 
%    1. segment geometries, masses and inertias,
%    2. muscle-skeleton mechanical links,
%    3. muscle mechanics
%
% -------------------------------------------------------------------------
% 
% © H.Geyer, 5 October 2006
% Adaptations, Annotations, Structs by Alexandra Buchmann, 
%                                           Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       05-10-2006
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------

% Gravity
g = 9.81;

% % ********************************************************************* %
% % 1. BIPED SEGMENTATION 
% % ********************************************************************* %
% % -----------------------------------------------------------------------
% % 1.1 General Foot Segment
% % -----------------------------------------------------------------------
% % See Foot_Ground_Contact

% -------------------------------------------------------------------------
% 1.2 General Shank Segment
% -------------------------------------------------------------------------
% Note: CS1 is position of ankle joint.

Body.L_S    = 0.5;  %[m]      shank length
Body.D1G_S  = 0.3;  %[m]      distance CS1 to CG (local center of gravity)
Body.D12_S  = Body.L_S;  %[m]      distance CS1 to C2 (knee joint)
Body.m_S    = 3.5;  %[kg]     shank mass
Body.In_S   = 0.05; %[kg*m^2] shank inertia with respect to axis 
                    %         ankle-CG-knee (scaled from other papers)

% -------------------------------------------------------------------------
% 1.3 General Thigh Segment
% -------------------------------------------------------------------------
% Note: CS1 is position of knee joint.

Body.L_T    = 0.5;      %[m]      total thigh length
Body.D1G_T  = 0.3;      %[m]      distance CS1 to CG (local gravity center)
Body.D12_T  = Body.L_T; %[m]      distance CS1 to C2 (hip joint) 
Body.m_T    = 8.5;      %[kg]     thigh mass
Body.In_T   = 0.15;     %[kg*m^2] thigh inertia with respect to axis 
                        %         ball-CG-heel (scaled from other papers)

% -------------------------------------------------------------------------
% 1.4 General Head-Arms-Trunk (HAT) Segment
% -------------------------------------------------------------------------
Body.L_HAT   = 0.8;  %[m]      HAT length
Body.D1G_HAT = 0.35; %[m]      distance hip to center of gravity
Body.m_HAT   = 53.5; %[kg]     HAT mass
Body.In_HAT  = 3;    %[kg*m^2] HAT inertia (scaled from other papers)

% -------------------------------------------------------------------------
% 1.5 Thigh Segment Pressure Sheet
% -------------------------------------------------------------------------
Body.DeltaThRef     = 5e-3; %[m] reference compression corresponding to 
                            %    steady-state with HAT mass
Body.k_pressure     = Body.m_HAT * 9.81 / Body.DeltaThRef; 
                            %[N/m] interaction stiffness
Body.v_max_pressure = 0.5;  %[m/s] max relaxation speed (one side)
Body.ThPos          = 4/5;  % position in thigh in length of thigh, 
                            % measured from lower thigh end

% -------------------------------------------------------------------------
% 1.6 Joint Soft Limits
% -------------------------------------------------------------------------

Joint.phi12_LowLimit  =  70*pi/180; %[rad] ankle angle soft limit
Joint.phi12_UpLimit   = 130*pi/180; %[rad] ankle "
Joint.phi23_UpLimit   = 175*pi/180; %[rad] knee  "
Joint.phi34_UpLimit   = 230*pi/180; %[rad] hip   "


Joint.c_jointstop     = 0.3 / (pi/180); %[Nm/rad] soft block reference 
                                        %                   joint stiffness
Joint.w_max_jointstop = 1 * pi/180;     %[rad/s]  soft block maximum joint 
                                        %             stop relaxation speed

                                        
% *********************************************************************** %
% 2. BIPED MUSCLE-SKELETON-LINKS 
% *********************************************************************** %
% -------------------------------------------------------------------------
% 2.1 Ankle Joint Specific Link Parameters
% -------------------------------------------------------------------------
% SOLeus attachement
Joint.rSOL       =       0.05; % [m]   radius 
Joint.phimaxSOL  = 110*pi/180; % [rad] angle of maximum lever contribution
Joint.phirefSOL  =  80*pi/180; % [rad] reference angle where MTU length 
                               %              equals sum of lopt and lslack
Joint.phirefSOL  =  80*pi/180; % [rad] reference angle where MTU length 
                               %              equals sum of lopt and lslack 
Joint.rhoSOL     =        0.5; % muscle pennation angle

% Tibialis Anterior attachement
Joint.rTA       =        0.04;
Joint.phimaxTA   =  80*pi/180;
Joint.phirefTA   = 110*pi/180; 
Joint.rhoTA      =        0.7; 

% GAStrocnemius attachement (ankle joint)
Joint.rGASa      =       0.05; 
Joint.phimaxGASa = 110*pi/180; 
Joint.phirefGASa =  80*pi/180; 
Joint.rhoGASa    =        0.7; 

% GAStrocnemius attachement (knee joint)
Joint.rGASk      =       0.05; 
Joint.phimaxGASk = 140*pi/180; 
Joint.phirefGASk = 165*pi/180; 
Joint.rhoGASk    =        0.7; 

% VAStus group attachement
Joint.rVAS       =       0.06; 
Joint.phimaxVAS  = 165*pi/180; 
Joint.phirefVAS  = 125*pi/180; 
Joint.rhoVAS     =        0.7; 
                       
% HAMstring group attachement (knee)
Joint.rHAMk      =       0.05; 
Joint.phimaxHAMk = 180*pi/180; 
Joint.phirefHAMk = 180*pi/180; 
Joint.rhoHAMk    =        0.7; 
                         
% HAMstring group attachement (knee)
Joint.rHAMh      =       0.08; 
Joint.phirefHAMh = 155*pi/180;  
Joint.rhoHAMh    =        0.7; 

% GLUtei group attachement
Joint.rGLU       =       0.10; 
Joint.phirefGLU  = 150*pi/180;     
Joint.rhoGLU     =        0.5; 
                         
% Hip FLexor group attachement
Joint.rHFL       =       0.10; 
Joint.phirefHFL  = 180*pi/180; 
Joint.rhoHFL     =        0.5; 


% *********************************************************************** %
% 3. BIPED MUSCLE MECHANICS 
% *********************************************************************** %
% -------------------------------------------------------------------------
% 3.1 Shared Muscle Tendon Parameters
% -------------------------------------------------------------------------
% excitation-contraction coupling
Muscle.preA =  0.01; %[] preactivation
Muscle.tau  =  0.01; %[s] delay time constant

% contractile element (CE) force-length relationship
Muscle.w    =   0.56; %[lopt] width
Muscle.c    =   0.05; %[]; remaining force at +/- width

% CE force-velocity relationship
Muscle.N    =   1.5; %[Fmax] eccentric force enhancement
Muscle.K    =     5; %[] shape factor

% Series elastic element (SE) force-length relationship
Muscle.eref =  0.04; %[lslack] tendon reference strain

% -------------------------------------------------------------------------
% 3.2 Muscle-Specific Parameters
% -------------------------------------------------------------------------
% soleus muscle
Muscle.FmaxSOL    = 4000; % maximum isometric force [N]
Muscle.loptSOL    = 0.04; % optimum fiber length CE [m]
Muscle.vmaxSOL    =    6; % maximum contraction velocity [lopt/s]
Muscle.lslackSOL  = 0.26; % tendon slack length [m]

% gastrocnemius muscle
Muscle.FmaxGAS    = 1500; % maximum isometric force [N]
Muscle.loptGAS    = 0.05; % optimum fiber length CE [m]
Muscle.vmaxGAS    =   12; % maximum contraction velocity [lopt/s]
Muscle.lslackGAS  = 0.40; % tendon slack length [m]

% tibialis anterior
Muscle.FmaxTA     =  800; % maximum isometric force [N]
Muscle.loptTA     = 0.06; % optimum fiber length CE [m]
Muscle.vmaxTA     =   12; % maximum contraction velocity [lopt/s]
Muscle.lslackTA   = 0.24; % tendon slack length [m]

% vasti muscles
Muscle.FmaxVAS    = 6000; % maximum isometric force [N]
Muscle.loptVAS    = 0.08; % optimum fiber length CE [m]
Muscle.vmaxVAS    =   12; % maximum contraction velocity [lopt/s]
Muscle.lslackVAS  = 0.23; % tendon slack length [m]

% hamstring muscles
Muscle.FmaxHAM   = 3000; % maximum isometric force [N]
Muscle.loptHAM   = 0.10; % optimum fiber length CE [m]
Muscle.vmaxHAM   =   12; % maximum contraction velocity [lopt/s]
Muscle.lslackHAM = 0.31; % tendon slack length [m]

% glutei muscles
Muscle.FmaxGLU   = 1500; % maximum isometric force [N]
Muscle.loptGLU   = 0.11; % optimum fiber length CE [m]
Muscle.vmaxGLU   =   12; % maximum contraction velocity [lopt/s]
Muscle.lslackGLU = 0.13; % tendon slack length [m]

% glutei muscles
Muscle.FmaxHFL   = 2000; % maximum isometric force [N]
Muscle.loptHFL   = 0.11; % optimum fiber length CE [m]
Muscle.vmaxHFL   =   12; % maximum contraction velocity [lopt/s]
Muscle.lslackHFL = 0.10; % tendon slack length [m]














