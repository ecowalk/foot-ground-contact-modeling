%% nms_model_MechControlInit_AddOns.m
% -------------------------------------------------------------------------
% nms_model_MechControlInit_AddOns.m - set additional model parameters for 
% nms model: 
%   - joint damping and stiffness, 
%   - output data sampling rate, 
%   - parameters for the metabolic energy calculation [2]
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       19-07-2022, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------
%
% #########################################################################
% References:  
% 
% [1] Buchmann, Alexandra; Kiss, Bernadett; Badri-Spröwitz, 
%     Alexander Thomas; Renjewski, Daniel: Power to the Springs: 
%     Passive Elements are Sufficient to Drive Push-Off in Human Walking. 
%     In: Robotics in Natural Settings, S. 21–32. 
%     Online: https://link.springer.com/chapter/10.1007/978-3-031-15226-9_5
% 
% [2] Umberger, Brian R.; Gerritsen, Karin G. M.; Martin, Philip E. (2003): 
%     A model of human muscle energy expenditure. In: Computer Methods in 
%     Biomechanics and Biomedical Engineering 6 (2), S. 99–111. 
%     DOI: 10.1080/1025584031000091678.
%
% #########################################################################

%% Init Custom Parameters (nms_model2G_EcoWalk.slx)

% --------------------------------------------- Joint Stiffnesses & Damping
Joint.k_phi34 = 0;        % Joint Spring Stiffness Hip
Joint.c_phi34 = 0;        % Joint Damping Coefficient Hip

Joint.k_phi23 = 0;        % Joint Spring Stiffness Knee
Joint.c_phi23 = 0;        % Joint Damping Coefficient Knee 

Joint.k_phi12 = 0;        % Joint Spring Stiffness Ankle
Joint.c_phi12 = 0;        % Joint Damping Coefficient Ankle

% ------------------------------------------- Sampling Rate for Data Output
Fs_out  = 250; %[Hz]
Fpass   = 30;       % Fpass = passband frequency of lowpass filter [Hz]; 

% ------------- Initialize Parameters for Energy Calculation (Optimization)
% -- From Umberger et al. 2003 - A Model of Human Muscle Energy Expenditure
Metab.FT_VAS = 50;
Metab.FT_GAS = 50;
Metab.FT_HAM = 35;
Metab.FT_GLU = 45;
Metab.FT_SOL = 20;
Metab.FT_TA  = 25;
Metab.FT_HFL = 50; % Iliopsoas 

% ----------------------------------- Total body mass (for CoT calculation)
Body.m_bw = Body.m_HAT + (Foot.m_F + Body.m_S + Body.m_T)*2; 

