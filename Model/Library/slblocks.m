function blkStruct = slblocks
% -------------------------------------------------------------------------
% blkStruct = slblocks specifies that the library 'Custom_Library_EcoWalk'
% appears in the Library Browser with the name 'Custom Library EcoWalk'
% -------------------------------------------------------------------------
%
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       02-02-2020, Munich
% Last modified:    20-07-2022, Munich 
% 
% -------------------------------------------------------------------------
% 
% #########################################################################
% References:  
% 
% [1] https://www.mathworks.com/help/simulink/ug/adding-libraries-to-the-library-browser.html
%
% #########################################################################

    Browser.Library     = 'Custom_Library_EcoWalk';
    Browser.Name        = 'Custom Library EcoWalk';
    blkStruct.Browser   = Browser;