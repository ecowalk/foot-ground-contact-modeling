# Neuromuscular Simulation Model
The **EcoWalker** is a neuromuscular model of human gait based on *Geyer 2010* [1]. The model runs in Simulink R2019a and higher using the [Simscape Multibody][5] toolbox.

For the current model version ```nms_model2G_EcoWalk.slx```, we have adapted and extended the original model presented in [1] by:
1. ```Energy Expenditure```: Metabolic energy calculation based on *Umberger 2003* [3]
2. ```Body Mechanics```: Six different foot-ground contact models (plus the original from [1])
3. ```Error & State Handling```: Simulation abort for defined error states 
4. ```Visualization & Model Analysis```: Creates the simulation output file for visualization and model analysis
5. ```Custom_Library_EcoWalk.slx```: Model-specific library blocks with different foot models and metabolic energy calculation (see [# Model Features and Adaptations](#Model-Features-and-Adaptations))

## Run the Model:
To run the NMS, open ```Run_nms_model2G_EcoWalk.m``` in *Model/* and press play. The SimMechanics Editor will open in Matlab and show a video of the walking model. The script will automatically save the simulation results in the folder *Simulation_Results/*.  
**Please understand the workflow for running the model in Run_nms_model2G_EcoWalk.m before diving into other scripts or functions!**  

Workflow of ```Run_nms_model2G_EcoWalk.m```:
1. Find the current working directory
2. Add all relevant folders and subfolders to the Matlab path 
3. Initialize default model parameters: 
	* Define model name ```mdl``` and ```simtime```
	* Init_Files\ ```nms_model_MechInit.m``` [1]
	* Init_Files\ ```nms_model_ControlInit.m``` [1]
	* Init_Files\ ```ground_param_nonlinear_Geyer.m``` [1] or ```ground_param_1to3seg_FGCM.m```
4. Add or customize additional parameters:
	* Init_Files\ ```nms_model_MechInit_AddOns.m```: see [# Model Features and Adaptations](#Model-Features-and-Adaptations)
5. Simulate model with ```sim``` command
6. Save results in *Simulation_Results\Case_no*:
	* A video recording of the SimMechanics Explorer
	* A ```Results_Case_no.mat``` file containing the simulation output
	* A ```Workspace.mat``` file containing all the workspace variables that were used to run the simulation

## Model Structure
The model is divided into six subsystems:
1. ```Body Mechanics```: multibody simulation with seven rigid bodies (thigh, shank, foot per leg, and head-arm-trunk segment) connected by revolute joints representing the skeletal structure of the body in 2D
2. ```Muscle Actuation```: Hill-type muscle model for seven major muscles per leg (GAS, SOL, TA, VAS, HAM, GLU, HFL)
3. ```Neural Control```: reflex-based, decentralized neural control system with a state machine to switch between stance and swing reflexes for each leg [1]
4. ```Visualization & Model Analysis```: set model output data using [Matlab Simulink To Workspace][4] blocks - you can check the contents of the ```simout``` struct by calling ```simout``` in the Matlab command window after the simulation is finished
5. ```Energy Expenditure```: metabolic energy calculation for each muscle and the whole model in total based on *Umberger 2003* [3]
6. ```Error & State Handling```: stop simulation in case the head-arm-trunk (HAT) segment falls below 1m or the upper body velocity is negative

<img src="Signal_Layers_Figure.png" height="250">

<img src="Signal_Layers_Details_Table.png" height="250">


## Model Features and Adaptations: 
**Implemented foot-ground contact models (FGCMs) in ```nms_model2G_EcoWalk.slx```** *Buchmann 2024* [6]:   
The ``Custom_Library_EcoWalk.slx`` library contains six newly developed FGCMs and the original/reference FGCM used by Geyer and Herr [1].  
- To simulate with a specific FGCM, open ```nms_model2G_EcoWalk.slx``` and replace the currently used FGCM with the desired FGCM by dragging and dropping it from the library browser.   
- For the FGCMs ```1 segment low ankle``` and ```Original foot model from (Geyer et al. 2010)```, the Rigid Transform block "active for FGCMs WITHOUT arch" must be activated.  
- All other models work with "active for FGCMs WITH arch."  
- You must initialize the model with the appropriate file (```ground_param_nonlinear_Geyer.m``` for the original foot model from *Geyer 2010* [1], ```ground_param_1to3seg_FGCM.m``` for all others).  

Please note that all models work with the default reflex values, but some may work best after optimizing the reflex gains. 

**Parameter defined in ```nms_model_MechInit_AddOns.m```**:
- Rotational joint spring stiffness and damping for all joints (ankle, knee, hip)
- Simulation output data sampling frequency ```Fs_out```
- Passband frequency for lowpass GRF filter ```Fpass```
- Muscle parameters for energy calculation, according to *Umberger 2003* [3]


## References
[1]: H. Geyer and H. Herr, "A Muscle-Reflex Model That Encodes Principles of Legged Mechanics Produces Human Walking Dynamics and Muscle Activities," in IEEE Transactions on Neural Systems and Rehabilitation Engineering, vol. 18, no. 3, pp. 263-273, June 2010, DOI: 10.1109/TNSRE.2010.2047592 .

[2]: Buchmann, Alexandra; Kiss, Bernadett; Badri-Spröwitz, Alexander Thomas; Renjewski, Daniel: Power to the Springs: Passive Elements are Sufficient to Drive Push-Off in Human Walking. In: Robotics in Natural Settings, S. 21–32, DOI: 10.1007/978-3-031-15226-9_5. 

[3]: Umberger, Brian R.; Gerritsen, Karin G. M.; Martin, Philip E. (2003): A model of human muscle energy expenditure. In: Computer Methods in Biomechanics and Biomedical Engineering 6 (2), S. 99–111. DOI: 10.1080/1025584031000091678 .

[4]: <https://de.mathworks.com/help/simulink/slref/toworkspace.html> "Matlab Simulink To Workspace"

[5]: <https://de.mathworks.com/products/simscape-multibody.html> "Simscape Multibody"

[6]: Buchmann, Alexandra; Wenzler, Simon; Welte, Lauren; Renjewski, Daniel (2024): The effect of including a mobile arch, toe joint, and joint coupling on predictive neuromuscular simulations of human walking. In: Sci Rep 14 (1), S. 14879. DOI: 10.1038/s41598-024-65258-z.
