%% Run_nms_model2G_EcoWalk.m
% -------------------------------------------------------------------------
% Run_nms_model2G_EcoWalk runs nms_model2G_EcoWalk.slx and saves the 
% simulation output with case numbers for analysis and visualization
%
% The script performs the following tasks:
%           1) Run nms_model2G_EcoWalk.slx for selected cases defined in 
%              *nms_model_ControlAdaptation*
% 
%           -------------------- save_simulation --------------------------
%           2) Creates new directory in *Simulation_Results/Case_No*
%           3) Records video of each simulation trial
%           4) Saves simulation output file in *Result_CaseNo*
%           5) Saves workspace data (without simulation output file) 
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       20-08-2021, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------
%
% #########################################################################
% References:  
% 
% [1] Geyer, Hartmut; Herr, Hugh (2010): A muscle-reflex model that encodes 
%     principles of legged mechanics produces human walking dynamics and 
%     muscle activities. In: IEEE Transactions on Neural Systems and 
%     Rehabilitation Engineering : a Publication of the IEEE Engineering in 
%     Medicine and Biology Society 18 (3), p.263–273. 
%     DOI: 10.1109/TNSRE.2010.2047592.
%
% #########################################################################

clear; close all; clc;

%% Set Current Working Directory 
path    = pwd; 
str_pos = strfind(path, 'foot-ground-contact-modeling');
fpath   = path( 1 : (str_pos+length('foot-ground-contact-modeling')-1) ); 

%% Add Matlab Path and 
addpath ([fpath '\Model\Init_Files']); 
addpath ([fpath '\Model\Library']); 

%% -------------------------------------------------- Init Model Parameters
mdl     = 'nms_model2G_EcoWalk'; % Models name
simtime = '15';                  % Simulation time in seconds

% ---- Mechanics config. and control inputs from original model (Geyer2010)
run('nms_model_MechInit.m')
run('nms_model_ControlInit.m')

% ----------------------- Init custom parameters (nms_model2G_Buchmann.slx)
run('ground_param_1to3seg_FGCM.m')
% run('ground_param_nonlinear_Geyer.m')
run('nms_model_MechInit_AddOns.m')

% ---------------------------------------- Adapt parameters for experiments
case_no = 1; % Used as a name ending when saving the results

% ------------- Load simulation to workers and define simulation parameters
load_system(mdl);
set_param(mdl,'StopTime',simtime,'FastRestart','off',...
          'SimMechanicsOpenEditorOnUpdate','on','SimulationMode','normal');
  
%% --------------------------------------------------------- Run Simulation
simout  = sim('nms_model2G_EcoWalk');

%% ----------------------------------------------------------- Save Results
save_simulation(mdl, simout, fpath, case_no, Fpass, Fs_out, g,...
                       Body, Foot, Ground, Init, Joint, Metab,...
                                  Muscle, Neural, Reflex, ts_max, simtime);


   

