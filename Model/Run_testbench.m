%% Run_nms_model2G_EcoWalk.m
% -------------------------------------------------------------------------
% Run_testbench runs fgcm_testbench.slx and creates simout as output.
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      08-12-2023, Munich
% Last modified:    08-12-2023, Munich
% 
% -------------------------------------------------------------------------
%
% #########################################################################
% References:  
% 
% [1]   Welte, L., Kelly, L. A., Lichtwark, G. A., and Rainbow, M. J. 
%       “Influence of the windlass mechanism on arch spring mechanics 
%       during dynamic foot arch deformation”. In: J. R. Soc. Interface 
%       15.145 (2018). DOI: 10.1098/rsif.2018.0270
% 
% 
% #########################################################################

clear; close all; clc;
%% Set TJ angle in deg
phi_toe=30;

%% Set Current Working Directory 
path    = pwd; 
str_pos = strfind(path, 'foot-ground-contact-modeling');
fpath   = path( 1 : (str_pos+length('foot-ground-contact-modeling')-1) ); 

%% Add Matlab Path and 
addpath ([fpath '\Model\Init_Files']); 
addpath ([fpath '\Model\Library']); 

%% -------------------------------------------------- Init Model Parameters
mdl     = 'fgcm_testbench'; % Models name
simtime = '4';              % Simulation time in seconds

% ---- Mechanics config. and control inputs from original model (Geyer2010)
run('nms_model_MechInit.m')
run('nms_model_ControlInit.m')

% ----------------------- Init custom parameters (nms_model2G_Buchmann.slx)
run('ground_param_1to3seg_FGCM.m')
run('nms_model_MechInit_AddOns.m')

% ------------- Load simulation to workers and define simulation parameters
load_system(mdl);
set_param(mdl,'StopTime',simtime,'FastRestart','off',...
          'SimMechanicsOpenEditorOnUpdate','on','SimulationMode','normal');
  
%% --------------------------------------------------------- Run Simulation
simout  = sim(mdl);

