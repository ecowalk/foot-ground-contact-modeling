%% nms_model_ControlAdaptation
% -------------------------------------------------------------------------
% nms_model_ControlAdaptation adapts specified control parameters for the 
% neuromuscular simulation (nms_model2G_EcoWalk.slx)
% -------------------------------------------------------------------------
%
% Run nms_model_ControlAdaptation always AFTER nms_model_ControlInit.m and 
% nms_model_MechInit.m
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       20-08-2021, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Case Switch
switch case_no
    case 1 % -------------------------------------- Intact - no adaptations
        
    case 2
        load([fpath '\Model\Init_Files\Foot_Ground_Contact\FGCM_optimization_results\' case_opt]);

    otherwise 
        error('nms_model_ControlAdaptation: Invalid Case_No!');
end