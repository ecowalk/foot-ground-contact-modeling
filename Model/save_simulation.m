function [] = save_simulation(mdl, simout, fpath, case_no, Fpass, Fs_out, g,...
                                Body, Foot, Ground, Init, Joint, Metab,...
                                   Muscle, Neural, Reflex, ts_max, simtime)
% -------------------------------------------------------------------------
% [] = save_simulation(mdl, simout, fpath, case_no, Fpass, Fs_out, g,...
%                                Body, Foot, Ground, Init, Joint, Metab,...
%                                Muscle, Neural, Reflex, Spring, ts_max)
% 
% Save simulation output for Simulink model:
%           1) checks simulation for errors or prematurely ending
%           2) creates a new directory named *Simulation_Results/Case_No*
%           3) records the video shown in the SimMechnicsExplorer
%           4) saves simulation simout named *Result_CaseNo*
%           5) saves remaining workspace variables used to run the model
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       08-12-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Check simulation for errors or prematurely ending
% ---------- Check simulation for errors and warn if ~isempty(ErrorMessage)
isempty_error = string(simout.ErrorMessage) == '';
if ~isempty_error
    warning(['save_simulation: Simulation ended on errors. ' ...
                                               char(simout.ErrorMessage)]);
            
    proceed = input("Do you want to proceed anyway? (1 = yes; 0 = no)");  
    mustBeMember(proceed,[0, 1]);
    assert(proceed, 'save_simulation: function aborted due to user input.');
end

% ---------------------------------------- Check prematurely simulation end
prematurely_end = max(simout.tout) < str2double(simtime);
if prematurely_end
    warning(['save_simulation: Simulation ended prematurely. ' ...
      char(simout.SimulationMetadata.ExecutionInfo.StopEventDescription)]);
            
    proceed = input("Do you want to proceed anyway? (1 = yes; 0 = no)");  
    mustBeMember(proceed,[0, 1]);
    assert(proceed, 'save_simulation: function aborted due to user input.');
end

%% Save results and video
        % ----------------------------------------- Create saving directory
        TestNo    = num2str(case_no);
        save_path = [fpath '\Simulation_Results\Case' TestNo];
        
        mkdir (save_path)

        % ----------------------------------------- Record simulation video 
        smwritevideo(mdl,[save_path '\Video_' TestNo]);

        % ------------------------------------- Save simulation output file
        eval(['Result_' TestNo '= simout;'])
        save([save_path '\Result_' TestNo], ['Result_' TestNo]);

        % ------------------------------ Save remaining workspace variables
        save([save_path '\Workspace.mat'], 'case_no', 'Fpass', 'Fs_out',... 
                        'g', 'Body', 'Foot', 'Ground', 'Init', 'Joint',... 
                        'Metab', 'Muscle', 'Neural', 'Reflex', 'ts_max',...
                                                                'simtime');
end

