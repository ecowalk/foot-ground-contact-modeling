function [] = Assign_xopt(x_opt, var_opt)
arguments 
    x_opt   (1,:) double 
    var_opt (1,:) char
end
%% Assign_xopt
% -------------------------------------------------------------------------
% [] = Assign_xopt(x_opt, var_opt) assignes the optimization variable x_opt
% to choosen variables caller workspace  
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       24-11-2022, Munich
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------

switch var_opt % --------------------------- Assign optimization parameters
    case 'Gains'
    assert(length(x_opt)==10, ...
                      'Assign_xopt: Case Gains x_opt size does not match!')
        
        evalin('caller','Reflex.GainVAS        = x_opt (1);'); 
        evalin('caller','Reflex.GainTA         = x_opt (2);'); 
        evalin('caller','Reflex.GainSOLTA      = x_opt (3);');
        evalin('caller','Reflex.GainSOL        = x_opt (4);'); 
        evalin('caller','Reflex.GainKneOverExt = x_opt (5);'); 
        evalin('caller','Reflex.GainHFL        = x_opt (6);'); 
        evalin('caller','Reflex.GainHAMHFL     = x_opt (7);'); 
        evalin('caller','Reflex.GainHAM        = x_opt (8);'); 
        evalin('caller','Reflex.GainGLU        = x_opt (9);'); 
        evalin('caller','Reflex.GainGAS        = x_opt (10);'); 

    case 'Fmax'
    assert(length(x_opt)==7, ...
                       'Assign_xopt: Case Fmax x_opt size does not match!')
                   
        evalin('caller','Muscle.FmaxVAS = x_opt (1);'); 
        evalin('caller','Muscle.FmaxTA  = x_opt (2);'); 
        evalin('caller','Muscle.FmaxSOL = x_opt (3);');
        evalin('caller','Muscle.FmaxHFL = x_opt (4);');
        evalin('caller','Muscle.FmaxHAM = x_opt (5);');
        evalin('caller','Muscle.FmaxGLU = x_opt (6);');
        evalin('caller','Muscle.FmaxGAS = x_opt (7);');

    case 'Init_Cond'
    assert(length(x_opt)==7, ...
                  'Assign_xopt: Case Init_Cond x_opt size does not match!')
                   
        evalin('caller','Init.vx0       = x_opt (1);');
        evalin('caller','Init.Lphi120   = x_opt (2);'); 
        evalin('caller','Init.Lphi230   = x_opt (3);'); 
        evalin('caller','Init.Lphi340   = x_opt (4);'); 
        evalin('caller','Init.Rphi120   = x_opt (5);'); 
        evalin('caller','Init.Rphi230   = x_opt (6);'); 
        evalin('caller','Init.Rphi340   = x_opt (7);'); 

    otherwise
        error(['Assign_xopt: var_opt not supported. Check spelling' ... 
          ' or call "help nms_model_Optimization" for more information.']);
end
end



