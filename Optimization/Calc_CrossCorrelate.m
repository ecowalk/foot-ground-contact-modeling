function [r_Corr] = Calc_CrossCorrelate(simout,fs,p,q)
% -------------------------------------------------------------------------
% [r_Corr] = Calc_CrossCorrelate(simout,fs,p,q) calculates the reward value
% r_Corr evaluating the normalized cross correlation values and delays of
% muscle activations, joint angles and joint moments for stance phase
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       14-12-2021, Munich
% Last modified:    16-12-2021, Munich 
% 
% -------------------------------------------------------------------------

[out_human] = Resample_Human_Data(fs,p,q);
[out_sim]   = Resample_Sim_Data(simout,fs,p,q);
[out_corr]  = CrossCorrelate(out_sim, out_human);

corr.stance = structfun(@double, out_corr.stance, 'uniform', 1);
R_ACT       = corr.stance(1:7);     % [HFL GLU HAM VAS GAS SOL TA]
Del_ACT     = corr.stance(8:14);    % [HFL GLU HAM VAS GAS SOL TA]
R_phi       = corr.stance(15:17);   % [Ankle, Knee, Hip]
R_T         = corr.stance(18:20);   % [Ankle, Knee, Hip]
Del_T       = corr.stance(21:23);   % [Ankle, Knee, Hip]

r_Corr_R    = 1/(sum(1-R_ACT)+sum(1-R_phi)+sum(1-R_T));
r_Corr_Del  = 1/(sum(Del_T) + sum(Del_ACT));

r_Corr      = r_Corr_R + r_Corr_Del;
end

