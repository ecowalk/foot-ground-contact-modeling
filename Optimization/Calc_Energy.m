function [CoT] = Calc_Energy(MetabolicMeasure, simout, m_bw, g)
% -------------------------------------------------------------------------
% [CoT] = Calc_Energy(MetabolicMeasure, simout, m_bw, g) calculates the
% energy expenditure of neuromuscular model. 
% 
% Avaliable Options for "MetabolicMeasure": 
%                        1)'Umberger2003' - See References in Main
%                        3)'ACT_Squared'  - Sum of Squared Muscle ACT
%                        4)'F_MTU'        - Sum of Total muscle work
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       17-12-2021, Munich
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------

switch MetabolicMeasure
    case 'Umberger2003' % -- Model of Total Human Muscle Energy Expenditure 
        Metabolic_Cost  = simout.MetabEnergy_Umberger;
        
    case 'ACT_Squared'  % Right Riemann Sum of ACT^2 
        delta_T         = diff(simout.ACT.Time);                
        ACT_squared     = simout.ACT.Data.^2;                   
        Energy_ACT      = (ACT_squared(2:end,:)).*delta_T ;     
        Metabolic_Cost  = sum(sum(Energy_ACT,1))*1e3;      
    
    case 'F_MTU'        % Right Riemann Sum of Total Concentric Muscle Work
                        % -------------------- Negative value = contraction
        delta_l         = max(0,-diff(simout.L_MTU.Data));         
        delta_T         = diff(simout.L_MTU.Time);   
        Energy_FMTU     = (simout.F_MTU.Data(2:end,:).*delta_l).*delta_T;                                                     
        Metabolic_Cost  = sum(sum(Energy_FMTU,1))*1e3;          

    otherwise 
     error(['Calc_Energy: given "MetabolicMeasure" not supported. '...
      'Check spelling and call "help Calc_Energy" for more information.']);
end
        % ------------------------------------- Calculate Cost of Transport
        CoT = Metabolic_Cost/(m_bw*g*simout.PosHAT.Data(end,1));  
end

