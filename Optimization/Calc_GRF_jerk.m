function [GRFs_jerk_norm] = Calc_GRF_jerk(simout, start_time)
% -------------------------------------------------------------------------
% [GRFs_jerk_norm] = Calc_GRF_jerk(simout, start_time)  calculates the sum 
% (right riemann sum) of x and y ground reaction force jerks from start_time 
% to end normalized to the maximum walking distance
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       17-12-2021, Munich
% Last modified:    04-02-2021, Munich 
% 
% -------------------------------------------------------------------------

start_index     = find(simout.GRFs.Time >= start_time, 1, 'first');

% Left Foot (numerical differentiation)
LGRF_jerk_x     = diff(simout.GRFs.Data(start_index:end,3));
LGRF_jerk_y     = diff(simout.GRFs.Data(start_index:end,4));

% Right Foot (numerical differentiation)
RGRF_jerk_x     = diff(simout.GRFs.Data(start_index:end,9));
RGRF_jerk_y     = diff(simout.GRFs.Data(start_index:end,10));

delta_T         = diff(simout.GRFs.Time(start_index:end));

sum_x           = abs(LGRF_jerk_x).*delta_T + abs(RGRF_jerk_x).*delta_T;
sum_y           = abs(LGRF_jerk_y).*delta_T + abs(RGRF_jerk_y).*delta_T;

x_jerk_norm     = sum(sum_x)/(simout.PosHAT.Data(end,1));
y_jerk_norm     = sum(sum_y)/(simout.PosHAT.Data(end,1));

GRFs_jerk_norm  = x_jerk_norm + y_jerk_norm;

end

