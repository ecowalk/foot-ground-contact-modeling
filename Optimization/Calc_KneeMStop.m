function [SumMStop] = Calc_KneeMStop(simout, start_time)
% -------------------------------------------------------------------------
% [SumMStop] = Calc_KneeMStop(simout, start_time) calculates the integral 
% (right riemann sum) over the knee stop moments in both legs from 
% start_time to end 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       13-01-2022, Munich
% Last modified:    04-02-2022, Munich 
% 
% -------------------------------------------------------------------------

start_index  = find(simout.GRFs.Time >= start_time, 1, 'first');

delta_T      = diff(simout.KneeMStop.Time(start_index:end));

LMStop       = abs(simout.KneeMStop.Data((start_index+1):end,1)).*delta_T;
RMStop       = abs(simout.KneeMStop.Data((start_index+1):end,2)).*delta_T;

LMStop_norm  = sum(LMStop)/(simout.PosHAT.Data(end,1));
RMStop_norm  = sum(RMStop)/(simout.PosHAT.Data(end,1));

SumMStop     = sum(LMStop_norm) + sum(RMStop_norm);

end

