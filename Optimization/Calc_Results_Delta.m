function [delta] = Calc_Results_Delta(var_opt, x_opt, Reflex, Muscle, Init)
%% Calc_Results_Delta
% -------------------------------------------------------------------------
% [delta] = Calc_Results_Delta(var_opt, x_opt, Reflex, Muscle, Init)
% calculates and displays the difference between initial guess and 
% optimized solution 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       24-11-2022, Munich
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------

switch var_opt
case 'Gains'
    
    delta.VAS        = (Reflex.GainVAS        - x_opt (1)); 
    delta.TA         = (Reflex.GainTA         - x_opt (2)); 
    delta.SOLTA      = (Reflex.GainSOLTA      - x_opt (3));
    delta.SOL        = (Reflex.GainSOL        - x_opt (4)); 
    delta.KOE        = (Reflex.GainKneOverExt - x_opt (5)); 
    delta.HFL        = (Reflex.GainHFL        - x_opt (6)); 
    delta.HAMHFL     = (Reflex.GainHAMHFL     - x_opt (7)); 
    delta.HAM        = (Reflex.GainHAM        - x_opt (8)); 
    delta.GLU        = (Reflex.GainGLU        - x_opt (9)); 
    delta.GAS        = (Reflex.GainGAS        - x_opt (10)); 
    
    disp('Deviation from Initial Guess (Gains):')
    disp(delta);
            
case 'Fmax'
    delta.VAS    = (Muscle.FmaxVAS - x_opt (1)); 
    delta.TA     = (Muscle.FmaxTA  - x_opt (2)); 
    delta.SOL    = (Muscle.FmaxSOL - x_opt (3)); 
    delta.HFL    = (Muscle.FmaxHFL - x_opt (4)); 
    delta.HAM    = (Muscle.FmaxHAM - x_opt (5)); 
    delta.GLU    = (Muscle.FmaxGLU - x_opt (6)); 
    delta.GAS    = (Muscle.FmaxGAS - x_opt (7)); 
    
    disp('Deviation from Initial Guess (Fmax):')
    disp(delta);

case 'Init_Cond'
    delta.vx0        = (Init.vx0     - x_opt (1)); 
    delta.Lphi120    = (Init.Lphi120 - x_opt (2)); 
    delta.Lphi230    = (Init.Lphi230 - x_opt (3)); 
    delta.Lphi340    = (Init.Lphi340 - x_opt (4));
    delta.Rphi120    = (Init.Rphi120 - x_opt (5)); 
    delta.Rphi230    = (Init.Rphi230 - x_opt (6)); 
    delta.Rphi340    = (Init.Rphi340 - x_opt (7)); 
    
    disp('Deviation from Initial Guess (Init_Cond):')
    disp(delta);

otherwise
    error(['Eval_OPT_results: var_opt is not supported. '...
              'Check Spelling or call "help nms_model_Optimization"'...
                                     ' to see valid options for var_opt']);
end
end

