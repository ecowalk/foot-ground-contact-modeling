function [acc_HAT] = Calc_acc_HAT(simout, start_time)
% -------------------------------------------------------------------------
% [acc_HAT] = Calc_acc_HAT(simout, start_time) calculates the integral 
% (right riemann sum) over HAT accelerations from start_time to end in x 
% and y directions normalized to the walking distance
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       30-12-2021, Munich
% Last modified:    03-02-2022, Munich 
% 
% -------------------------------------------------------------------------

start_index     = find(simout.GRFs.Time >= start_time, 1, 'first');

acc_HAT_x       = diff(simout.VelHAT.Data(start_index:end,1));
acc_HAT_y       = diff(simout.VelHAT.Data(start_index:end,2));

delta_T         = diff(simout.VelHAT.Time(start_index:end));

acc_sum_x       = abs(acc_HAT_x).*delta_T;
acc_sum_y       = abs(acc_HAT_y).*delta_T;

acc_sum_x_norm  = sum(acc_sum_x)/(simout.PosHAT.Data(end,1));
acc_sum_y_norm  = sum(acc_sum_y)/(simout.PosHAT.Data(end,1));

acc_HAT         = acc_sum_x_norm + acc_sum_y_norm;

end

