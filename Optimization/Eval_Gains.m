%% Eval_Gains
% -------------------------------------------------------------------------
% Eval_Gains creates a table listing all optimization results of the files
% given in "filesNames" 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       10-11-2023, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

% --------------------- Create an empty table to store the extracted values
tableData = table();

% ------------ List of outputfile names (including their path if necessary)
fileNames = {'Out_Opt_1s-hA.mat', 'Out_Opt_1s-lA.mat', 'Out_Opt_2s-MTJ.mat', ...
    'Out_Opt_2s-TJ.mat', 'Out_Opt_3s-nW.mat', 'Out_Opt_3s-W.mat'};

% ----------------------------------------------------------- For all files
for i = 1:length(fileNames)
    load(fileNames{i}); % Load the .mat file
    
    gainKneOverExt = Reflex.GainKneOverExt; % Extract variables of interest
    gainHAMHFL     = Reflex.GainHAMHFL;
    gainGAS        = Reflex.GainGAS * Muscle.FmaxGAS;
    gainTA         = Reflex.GainTA;
    gainVAS        = Reflex.GainVAS * Muscle.FmaxVAS;
    gainSOLTA      = Reflex.GainSOL * Muscle.FmaxSOL;
    gainSOL        = Reflex.GainSOL * Muscle.FmaxSOL;
    gainHFL        = Reflex.GainHFL;
    gainHAM        = Reflex.GainHAM * Muscle.FmaxHAM;
    gainGLU        = Reflex.GainGLU * Muscle.FmaxGLU;
    
    % ----------------------------------- Add extracted values to the table
    newRow = table(gainKneOverExt, gainHAMHFL, gainGAS, gainTA, gainVAS, ...
        gainSOLTA, gainSOL, gainHFL, gainHAM, gainGLU);
    
    tableData = [tableData; newRow];
end

disp(tableData); % Display the table


