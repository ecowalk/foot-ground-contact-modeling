%% Eval_OPT_results
% -------------------------------------------------------------------------
% Eval_OPT_results evalutes the results of a finished optimization 
% returnig the difference between initial guess and result, running the 
% simulation and saving the results (workspace and figures) in the current 
% directory
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       15-12-2021, Munich
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------
%% Intialize Model Parameters (Initial Guess)
Init_Params(init_case);

%% Set Path to Save Results
fpath_opt = pwd;

%% Assign Values from Optimization
Calc_Results_Delta(var_opt, x_opt, Reflex, Muscle, Init);
Assign_xopt(x_opt, var_opt);

%% Run Simulation with Optimization Results
out_opt = sim(mdl,'FastRestart','off','SimulationMode','normal',...
                    'StopTime','20','SimMechanicsOpenEditorOnUpdate','on');

%% Save Results
date_time = datestr(datetime('now'),30); % ---- 'yyyymmddTHHMMSS' (ISO 8601)
filename = ['Out_Opt_' date_time];
save(fullfile(fpath_opt, filename));