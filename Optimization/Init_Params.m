function [] = Init_Params(Init_Case)
arguments 
    Init_Case (1,:) char
end
%% Init_Params
% -------------------------------------------------------------------------
% Init_Params initializes the parameters to run nms_model2G_EcoWalk.slx by
% assigning all relevant parameters to the caller workspace 
%
% Supported Init Cases:
%           - 'Geyer2010': basic parameter set as presented in Geyer 2010
%           - 'FGCM2023': six different foot ground contact models 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       04-02-2022, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------

switch Init_Case
    case 'Geyer2010' %-----------------------Original parameters from paper
        
    assignin('caller','Var_names','nms_model_MechControlInit_var_names_Geyer.mat');
    assignin('caller','init_vars','init_vars');
    assignin('caller','File_name','nms_model_MechControlInit_Geyer.mat');

    evalin('caller','load(Var_names, init_vars)');
    evalin('caller','load(File_name, init_vars.name)');

    evalin('caller','clearvars Var_names init_vars File_name')
    
    case 'FGCM2023' %------------------Parameters for different foot models
        
    assignin('caller','Var_names','nms_model_MechControlInit_var_names_FGCM.mat');
    assignin('caller','init_vars','init_vars');
    assignin('caller','File_name','nms_model_MechControlInit_FGCM.mat');

    evalin('caller','load(Var_names, init_vars)');
    evalin('caller','load(File_name, init_vars.name)');

    evalin('caller','clearvars Var_names init_vars File_name')
    
    otherwise
        error(['Init_Params: Init_Case not supported.'...
        'Check spelling or call "help Init_Params" to see valid options.'])
end

end