function [x_0, ub, lb, nvars] = Init_xopt(var_opt, Reflex, Muscle, Init)
%% Init_xopt
% -------------------------------------------------------------------------
% [x_0, ub, lb, nvars] = Init_xopt(var_opt, Reflex, Muscle, Init) 
% initializes the optimization variable x_opt and sets upper and lower
% bounds for optimization
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       24-11-2022, Munich
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------

switch var_opt %--------------------------Select optimization parameter set
    case 'Gains'
     x_0 = [Reflex.GainVAS;         % 0.82 - 13.5
            Reflex.GainTA;          % 0.55 - 3.2
            Reflex.GainSOLTA;       % 0    - inf
            Reflex.GainSOL;         % 0.97 - 2.17
            Reflex.GainKneOverExt;  % 0 - inf
            Reflex.GainHFL;         % 0.17 - 3
            Reflex.GainHAMHFL;      % 0 - 100
            Reflex.GainHAM;         % 0 - 0.67
            Reflex.GainGLU;         % 0 - 0.52
            Reflex.GainGAS];        % 0 - inf
            
        nvars = length (x_0);
        %------------Upper and Lower bounds (see Geyer et al. 2010 Table I)
        ub = ones(nvars,1).*[13.5/Muscle.FmaxVAS; 
                             3.2; 
                             inf; 
                             2.17/Muscle.FmaxSOL; 
                             inf; 
                             3; 
                             100; 
                             0.67/Muscle.FmaxHAM; 
                             0.52/Muscle.FmaxGLU; 
                             inf];
                        
        lb = ones(nvars,1).*[0.82/Muscle.FmaxVAS; 
                             0.55; 
                             0   /Muscle.FmaxSOL; 
                             0.97/Muscle.FmaxSOL; 
                             0; 
                             0.17; 
                             0; 
                             0   /Muscle.FmaxHAM; 
                             0   /Muscle.FmaxGLU; 
                             0   /Muscle.FmaxGAS];

    case 'Fmax'
     x_0 = [Muscle.FmaxVAS; 
            Muscle.FmaxTA; 
            Muscle.FmaxSOL;
            Muscle.FmaxHFL;
            Muscle.FmaxHAM;
            Muscle.FmaxGLU;
            Muscle.FmaxGAS];
        
        nvars = length (x_0);
        %---------------Upper bound: maximum 10 times original muscle force
        ub = ones(nvars,1).*x_0*10;
        lb = zeros(nvars,1);
        
    case 'Init_Cond' 
     x_0 = [Init.vx0; 
            Init.Lphi120; 
            Init.Lphi230; 
            Init.Lphi340; 
            Init.Rphi120; 
            Init.Rphi230; 
            Init.Rphi340]; 
            
      nvars = length (x_0);
        
        % Upper/lower bound values (RoM for Joints) see [7,8] & Geyer Model
       ub (1) = 10;             % vx0 
       ub (2) = deg2rad (130);  % Lphi120 - Ankle; 
       ub (3) = deg2rad (175);  % Lphi230 - Knee; 
       ub (4) = deg2rad (230);  % Lphi340 - Hip; 
       ub (5) = deg2rad (130);  % Rphi120; 
       ub (6) = deg2rad (175);  % Rphi230; 
       ub (7) = deg2rad (230);  % Rphi340
       ub     = ub';
        
       lb (1) = 0;              % vx0 
       lb (2) = deg2rad (70);   % Lphi120; 
       lb (3) = deg2rad (30);   % Lphi230; 
       lb (4) = deg2rad (55);   % Lphi340; 
       lb (5) = deg2rad (70);   % Rphi120; 
       lb (6) = deg2rad (30);   % Rphi230; 
       lb (7) = deg2rad (55);   % Rphi340
       lb     = lb';
            
    otherwise
        error(['Init_xopt: var_opt is not supported. '...
              'Check spelling or call "help nms_model_Optimization"'...
                                     ' to see valid options for var_opt']);
end

end

