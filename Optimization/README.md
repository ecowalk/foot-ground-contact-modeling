# Optimization 
You can optimize the gait of the NMS model using the optimization framework provided in this folder described in *Buchmann 2022* [1]. The whole framework is more comprehensive than described in the paper, so read this README carefully before starting.

## How to Run an Optimization
**A few notes at the beginning:** 
- Running the optimization is computationally expensive. Start with a few generations (5-10) and iterations (max. 10) to explore how the algorithm works. 
- Do not run the complete optimization on a standard laptop, as the CPU and RAM are usually too weak. Only run the code on a powerful desktop PC or gaming laptop.
- If the optimization is very fast on default settings (less than a few minutes), this is a strong indicator that you are doing something wrong ;-)
- **Example**: on a *64-bit, 12-core Intel i9-10900K CPU at 3.7 GHz with 64GB RAM* computer in *parallel computing* with a *precompiled model in accelerator mode with fast restart*, the optimization can take up to 6h in total depending on the quality of your initial guess.

To run the optimization, open ```main_OPT.m```. You can keep all default optimization settings for a first try. For more information about the available options, see the [# Feature Overview](#Feature-Overview) below or call ```help nms_model_Optimization``` and ```help costFun``` in the Matlab command window.  
Open ```Set_Opts_ALG.m``` to change the number of generations and iterations for the optimization algorithm (generations 5-10 and iterations max. 10 for a first try). Then go back to ```main_OPT.m``` and press run.  

Matlab will automatically precompile an acceleration target for the model, set up a parallel computing pool, and run the optimization. For the default settings using the GA algorithm, a bar graph shows the individual cost function contributions for each non-stall generation. When the optimization is complete, the results are automatically saved with a timestamp. 

---
## Feature Overview
This section provides an overview of the features implemented in this optimization framework. For more information, see the function descriptions available by calling ```help nms_model_Optimization``` 

**Optimization Settings** in ```main_OPT.m```:
1. ```parFlag```: Parallel computing on/off 			
2. ```accFlag```: Accelerator mode with fast restart
3. ```init_case```: ```'Geyer2010'``` parameter set used to initialize the model
4. ```simtime```: simulation time in seconds
5. ```MetabolicMeasure```: type of metabolic measures 
	* ```'Umberger2003'``` 	- method as described in *Umberger 2003* [2]
	* ```'ACT_Squared'``` 	- muscle activations squared
	* ```'F_MTU'``` 		- the sum of total contractile muscle forces 
6. ```var_opt```: variable set to be optimized
	* ```'Gains'``` 	- 11 muscle model reflex gains (*default*)
	* ```'Fmax'``` 		- 7 maximum muscle forces (one for each muscle group)
	* ```'Init_Cond'``` - 7 initial conditions: initial velocity and initial joint angles
7. ```Opt_ALG```: optimization algorithm used to solve the problem 
	* ```'particleswarm'``` - Bounded constrained optimization using particle swarm optimization [Matlab PSO][4]
	* ```'GA'``` 			- Constrained optimization with genetic algorithm [Matlab GA][3]
	* ```'fmincon'``` 		- Gradient-based constrained minimum of a function [Matlab fmincon][5]  

**Functions**: 
- ```Set_Opts_ALG.m```: set individual settings for all optimization algorithms, e.g., swarm size, number of generations, etc.
- ```costFun.m```: multi-objective cost function as described in *Buchmann 2022* [1] (call ```help costFun``` for more info)
- ```Eval_OPT_results.m```: save the optimization result, the entire workspace, and all figures in the current directory 

## References: 
[1]: Buchmann, Alexandra; Kiss, Bernadett; Badri-Spröwitz, Alexander Thomas; Renjewski, Daniel: Power to the Springs: Passive Elements are Sufficient to Drive Push-Off in Human Walking. In: Robotics in Natural Settings, S. 21–32, DOI: 10.1007/978-3-031-15226-9_5.

[2]: Umberger, Brian R.; Gerritsen, Karin G. M.; Martin, Philip E. (2003): A model of human muscle energy expenditure. In: Computer Methods in Biomechanics and Biomedical Engineering 6 (2), S. 99–111. DOI: 10.1080/1025584031000091678.

[3]: <https://de.mathworks.com/help/gads/ga.html> "Matlab GA"

[4]: <https://de.mathworks.com/help/gads/particleswarm.html> "Matlab particleswarm"

[5]: <https://de.mathworks.com/help/optim/ug/fmincon.html> "Matlab fmincon"
