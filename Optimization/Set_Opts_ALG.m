function [opts] = Set_Opts_ALG(Opt_ALG, parFlag)
% -------------------------------------------------------------------------
% [opts] = Set_Opts_ALG(Opt_ALG, parFlag) sets optimization settings using 
% optimoptions(Opt_ALG) e.g. MaxIterations, Plots, Termination Criteria...
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       16-12-2021, Munich
% Last modified:    11-09-2021, Munich 
% 
% -------------------------------------------------------------------------
%% --------------------------------------------------------- Fmincon - Opts
%{
% Fmincon - Also used as a hybrid function for GA and PSO
%            Default properties:
%                     Algorithm: 'interior-point'
%                CheckGradients: 0
%           ConstraintTolerance: 1.0000e-06
%                       Display: 'final'
%      FiniteDifferenceStepSize: 'sqrt(eps)'
%          FiniteDifferenceType: 'forward'
%          HessianApproximation: 'bfgs'
%                    HessianFcn: []
%            HessianMultiplyFcn: []
%                   HonorBounds: 1
%        MaxFunctionEvaluations: 3000
%                 MaxIterations: 1000
%                ObjectiveLimit: -1.0000e+20
%           OptimalityTolerance: 1.0000e-06
%                     OutputFcn: []
%                       PlotFcn: []
%                  ScaleProblem: 0
%     SpecifyConstraintGradient: 0
%      SpecifyObjectiveGradient: 0
%                 StepTolerance: 1.0000e-10
%           SubproblemAlgorithm: 'factorization'
%                      TypicalX: 'ones(numberOfVariables,1)'
%                   UseParallel: 0
%
% 
%% ------------------------------------- Particle Swarm Optimization - Opts
%
%  Default properties:
%                   CreationFcn: @pswcreationuniform
%                       Display: 'final'
%             FunctionTolerance: 1.0000e-06
%                     HybridFcn: []
%                  InertiaRange: [0.1000 1.1000]
%            InitialSwarmMatrix: []
%              InitialSwarmSpan: 2000
%                 MaxIterations: '200*numberofvariables'
%            MaxStallIterations: 20
%                  MaxStallTime: Inf
%                       MaxTime: Inf
%          MinNeighborsFraction: 0.2500
%                ObjectiveLimit: -Inf
%                     OutputFcn: []
%                       PlotFcn: []
%          SelfAdjustmentWeight: 1.4900
%        SocialAdjustmentWeight: 1.4900
%                     SwarmSize: 'min(100,10*numberofvariables)'
%                   UseParallel: 0
%                 UseVectorized: 0
%
%% ----------------------------------------------- Generic Algorithm - Opts
%
%            Default properties:
%           ConstraintTolerance: 1.0000e-03
%                   CreationFcn: @gacreationuniform
%                  CrossoverFcn: @crossoverscattered
%             CrossoverFraction: 0.8000
%                       Display: 'final'
%                    EliteCount: '0.05*PopulationSize'
%                  FitnessLimit: -Inf
%             FitnessScalingFcn: @fitscalingrank
%             FunctionTolerance: 1.0000e-06
%                     HybridFcn: []
%       InitialPopulationMatrix: []
%        InitialPopulationRange: []
%           InitialScoresMatrix: []
%                MaxGenerations: '100*numberOfVariables'
%           MaxStallGenerations: 50
%                  MaxStallTime: Inf
%                       MaxTime: Inf
%                   MutationFcn: {@mutationgaussian  [1]  [1]}
%  NonlinearConstraintAlgorithm: 'auglag'
%                     OutputFcn: []
%                       PlotFcn: []
%                PopulationSize: '50 when numberOfVariables <= 5, else 200'
%                PopulationType: 'doubleVector'
%                  SelectionFcn: @selectionstochunif
%                   UseParallel: 0
%                 UseVectorized: 0
%}

%% Set Options
opts_fmincon = optimoptions('fmincon',...
                            'Display','iter',...
                            'MaxIterations', 10,...
                            'PlotFcn',@optimplotfval,...
                            'UseParallel', parFlag);
                        
switch Opt_ALG
    case 'fmincon' 
    opts = opts_fmincon;

    case 'particleswarm'
    opts_PSO = optimoptions('particleswarm',...
                            'Display','iter',...         
                            'SwarmSize', 200,...
                            'OutputFcn',@psoplotcont,...
                            'PlotFcn',@pswplotbestf,...
                            'UseParallel', parFlag,...
                            'MaxIterations', 500); 
    opts = opts_PSO;

    case 'GA'
    opts_GA = optimoptions( 'GA',...
                            'Display','iter',...
                            'MaxGenerations', 1000,... 
                            'PopulationSize', 200,...
                            'OutputFcn',@gaplotcont,...
                            'PlotFcn', @gaplotbestf,...
                            'UseParallel', parFlag); 
    opts = opts_GA;     
    
    otherwise
        error(['Set_Opts_ALG: choosen var_opt is not supported. '...
                'Check spelling or call "help nms_model_Optimization"'...
                ' to see avaliable options.']);  
end
end

