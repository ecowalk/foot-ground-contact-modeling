function [] = Set_Sim_Param(parFlag, accFlag, mdl, simtime)
% -------------------------------------------------------------------------
% [] = Set_Sim_Param(parFlag, accFlag, mdl, simtime) sets model parameters 
% for nms_model2G_Buchmann.slx to enable accelerator mode, fast restart and
% paralell computing
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       16-12-2021, Munich
% Last modified:    24-11-2022, Munich 
% 
% -------------------------------------------------------------------------

%% ------- Default settings without accelerator mode and paralell computing
load_system(mdl);
set_param(mdl,'SimulationMode','normal','FastRestart','on',...
                'SimMechanicsOpenEditorOnUpdate','off','StopTime',simtime);
set_param(mdl,'StopTime',simtime);        

%% ------------------------------------- Settings for accelerator mode only

if (~parFlag && ~isempty(gcp('nocreate'))) % ------- Close exsiting parpool
delete(gcp)    
end

if (accFlag && ~parFlag)
        load_system(mdl);
        set_param(mdl,'SimulationMode','accelerator','FastRestart','on',...
                       'SimMechanicsOpenEditorOnUpdate','off',...
                       'StopTime',simtime);
end

%% ----------------------------------  Settings for paralell computing only
if (parFlag && ~accFlag)
    if isempty(gcp('nocreate'))
        parpool; % ----------------------------- Uses default saved profile
    end
 
        parfevalOnAll(@load_system,0,mdl);
        parfevalOnAll(@set_param,0,mdl,...
                        'SimulationMode','normal','FastRestart','on',...
                        'SimMechanicsOpenEditorOnUpdate','off',...
                        'StopTime',simtime);
end


%% ------------------- Settings for accelerator mode and paralell computing 
if (parFlag && accFlag)
    % ------------------------ Build accelerator target before stating pool
    load_system(mdl);
    set_param(mdl,'SimulationMode','accelerator');
    
    if isempty(gcp('nocreate'))
        parpool; % ----------------------------- Uses default saved profile
    end
    
        parfevalOnAll(@load_system,0,mdl);
        parfevalOnAll(@set_param,0,mdl,...
                        'SimulationMode','accelerator','FastRestart','on',...
                        'SimMechanicsOpenEditorOnUpdate','off',...
                        'StopTime',simtime);
end
end

