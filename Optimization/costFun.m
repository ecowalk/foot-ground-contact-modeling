function [reward] = costFun (x_opt, mdl, init_case, simtime, MetabolicMeasure, var_opt, OutputFun)
arguments
    x_opt               (1,:) double
    mdl                 (1,:) char
    init_case           (1,:) char
    simtime             (1,:) char
    MetabolicMeasure    (1,:) char
    var_opt             (1,:) char
    OutputFun           logical
end
% -------------------------------------------------------------------------
% [reward] = costFun (x_opt, mdl, init_case, simtime, MetabolicMeasure,...
%                                                       var_opt, OutputFun) 
%
% cost function for optimization; details on contributing measures see code
% 
% Inputs: x_opt     - parameter vector for optimiztation
%         mdl       - model name
%         init_case - set of init variables to run the model  
%         simtime   - desired simulation time
%
%         Metabolic Measures: 1)'Umberger2003' - See References in Main
%                             2)'ACT_Squared'  - Sum of Squared Muscle ACT
%                             3)'F_MTU'        - Sum of Total muscle force
%
%         var_opt: Set parameters to be optimized 
%                             1)'Gains'
%                             2)'Fmax'
%                             3)'Init_Cond'
%
%         OutputFun: if true, the contributions to the rewardfunction are
%                    returned as a vector (for plotting contributions
%                    during optimization)
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       14-12-2021, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Assign optimization parameters -----------------------------------------

%---------------------------------------------- Initialize model parameters
Init_Params(init_case);
Assign_xopt(x_opt, var_opt);

%% Run Simulation with New Set of Parameters ------------------------------
try
simout = sim(mdl,'SrcWorkspace','current','TimeOut',3*str2double(simtime));
skip_eval = false;
    
catch  %--------------------------------------------------MException struct
    % fprintf(2,'%s\n',e.identifier); %---------------Display error message
    skip_eval = true;
end

%% Evaluate Results -------------------------------------------------------
if ~skip_eval 
% Set Step to Start Evaluation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
try
change_index   = find((diff(simout.Stance.data(:,1))~=0)~=0)';
step_eval      = 3;
start_time     = out.Stance.time(change_index(step_eval*2)-1);

catch %If model does not reach step_eval, full simulationtime is considered
    
start_time     = 0;
end

% 1 - Effort Measure Metabolic ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CoT             = Calc_Energy(MetabolicMeasure, simout, Body.m_bw, g); 
weight_CoT      = 5.25e2;
min_CoT         = 1;
exp_CoT         = 1;

    C_CoT           = weight_CoT        * CoT^exp_CoT           * min_CoT;

% 2 - Effort Measure - ACT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if ~strcmp(MetabolicMeasure, 'ACT_Squared')
CoT_ACT         = Calc_Energy('ACT_Squared', simout, Body.m_bw, g); 
weight_CoT_ACT  = 1e2;
min_CoT_ACT     = 1;
exp_CoT_ACT     = 1;
else % -------------------- Ignore if ACT is used as metabolic measure only 
CoT_ACT         = 0; 
weight_CoT_ACT  = 0;
min_CoT_ACT     = 0;
exp_CoT_ACT     = 0;
end

    C_CoT_ACT       = weight_CoT_ACT  * CoT_ACT^exp_CoT_ACT  * min_CoT_ACT;


% 3 - GRFs Jerk ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GRFjerk         = Calc_GRF_jerk(simout, start_time);
weight_GRFjerk  = 3;
min_GRFjerk     = 1;
exp_GRFjerk     = 1;

    C_GRFjerk       = weight_GRFjerk  * GRFjerk^exp_GRFjerk  * min_GRFjerk;

% 4 - Global Gait Measure: Early fail ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
tFall           = str2double(simtime) - simout.tout(end);
weight_tFall    = 1e5;
min_tFall       = 1; 
exp_tFall       = 2;

    C_tFall         = weight_tFall    * tFall^exp_tFall      * min_tFall;

% 5 - Head Acceleration ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
accHAT          = Calc_acc_HAT(simout, start_time);
weight_accHAT   = 10e3;
min_accHAT      = 1;
exp_accHAT      = 1;

    C_accHAT        = weight_accHAT   * accHAT^exp_accHAT     * min_accHAT;

% 6 - Knee & Ankle Stop Moment ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SumMStop_Knee           = Calc_KneeMStop(simout, start_time);
weight_SumMStop_Knee    = 15;
min_SumMStop_Knee       = 1;
exp_SumMStop_Knee       = 1;

    C_SumMStop_Knee  = weight_SumMStop_Knee * SumMStop_Knee^exp_SumMStop_Knee ...
                                                       * min_SumMStop_Knee;

SumMStop_Ankle          = Calc_AnkleMStop(simout, start_time);
weight_SumMStop_Ankle   = 20;
min_SumMStop_Ankle      = 1;
exp_SumMStop_Ankle      = 2;  

    C_SumMStop_Ankle    = weight_SumMStop_Ankle * SumMStop_Ankle^exp_SumMStop_Ankle ...
                                                      * min_SumMStop_Ankle;
       
else %---If simulation stopped due to errors (e.g. in Ground Contact Model)
    if skip_eval 
        C_CoT            = 0;
        C_CoT_ACT        = 0;
        C_GRFjerk        = 0;
        C_accHAT         = 0;
        C_SumMStop_Knee  = 0;
        C_SumMStop_Ankle = 0;
        
        C_tFall          = 1e5;
    end
end

% All contributions to reward function (for plotting) ~~~~~~~~~~~~~~~~~~~~~
C       = [C_CoT; C_CoT_ACT; C_GRFjerk; C_accHAT; C_SumMStop_Knee; ...
                                                C_SumMStop_Ankle; C_tFall];
                  
% Final reward function to be minimized ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
reward  = sum(C);

if OutputFun %---------------------Output contributions vector for plotting
reward  = C;
end

end