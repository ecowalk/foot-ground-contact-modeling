%% main_OPT
% -------------------------------------------------------------------------
% main_OPT runs optimization methods for nms_model2G_EcoWalk.slx
% 
% Script supports three different optimization algorithms and three 
% different variable combinations for optimization. See
%   > "help nms_model_Optimization" for more details on the avaliable
%   options
%   > "help costFun" to see more details on avaliable optimization
%   objectives
% 
% Options to speed up the simulation process:
%   > paralell computing (parFlag = true) 
%   > accelerator mode (accFlag = true)  
%   > all simulations use the 'FastRestart'-Option by default
% 
% Options for used algorithms see "help Set_Opts_ALG"
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------
%{
%##########################################################################
% References:
% ------------------------------------------------------ Metabolic Measures 
% [1] Umberger, Brian R.; Gerritsen, Karin G. M.; Martin, Philip E. (2003): 
%     A model of human muscle energy expenditure. In: Computer Methods in 
%     Biomechanics and Biomedical Engineering 6 (2), S. 99–111. 
%     DOI: 10.1080/1025584031000091678.
%
% [2] Haeufle, Daniel F. B.; Siegel, Johannes; Hochstein, Stefan; Gussew, 
%     Alexander; Schmitt, Syn; Siebert, Tobias et al. (2020): Energy 
%     Expenditure of Dynamic Submaximal Human Plantarflexion Movements: 
%     Model Prediction and Validation by in-vivo Magnetic Resonance 
%     Spectroscopy. In: Frontiers in Bioengineering and Biotechnology 8, 
%     S. 622. DOI: 10.3389/fbioe.2020.00622.
%
% [3] Umberger, Brian R. (2010): Stance and swing phase costs in human 
%     walking. In: J. R. Soc. Interface. 7 (50), S. 1329–1340. 
%     DOI: 10.1098/rsif.2010.0084.
%
% [4] OpenSim Webinar - Metabolic cost modeling: experimental validation 
%     and predictive simulations; url: https://www.youtube.com/watch?v=XR-qCwuIJIo
%
% [5] SCONE EffortMeasure - https://scone.software/doku.php?id=ref:effort_measure
% 
% ------------------------------------------------------ Neurmuscular Model
% [6] Geyer, Hartmut; Herr, Hugh (2010): A muscle-reflex model that encodes 
%     principles of legged mechanics produces human walking dynamics and 
%     muscle activities. In: IEEE Transactions on Neural Systems and 
%     Rehabilitation Engineering : a Publication of the IEEE Engineering in 
%     Medicine and Biology Society 18 (3), S. 263–273. 
%     DOI: 10.1109/TNSRE.2010.2047592.
%
% ------------------------------------------------------------ Joint Limits 
% [7] Grimmer, Martin; Elshamanhory, Ahmed A.; Beckerle, Philipp (2020): 
%     Human Lower Limb Joint Biomechanics in Daily Life Activities: A 
%     Literature Based Requirement Analysis for Anthropomorphic Robot 
%     Design. In: Front. Robot. AI 7, S. 13. DOI: 10.3389/frobt.2020.00013.
%
% [8] Abu El Kasem, Shimaa T.; Aly, Sobhy M.; Kamel, Ehab M.; Hussein, 
%     Hisham M. (2020): Normal active range of motion of lower extremity 
%     joints of the healthy young adults in Cairo, Egypt. In: Bull Fac Phys 
%     Ther 25 (1), S. 1–7. DOI: 10.1186/s43161-020-00005-9.
% 
% ---------------------------------------------------- Cost function design 
% [9] Veerkamp, K.; Waterval, N. F. J.; Geijtenbeek, T.; Carty, C. P.; 
%     Lloyd, D. G.; Harlaar, J.; van der Krogt, M. M. (2021): Evaluating 
%     cost function criteria in predicting healthy gait. In: Journal of 
%     Biomechanics 123, S. 110530. DOI: 10.1016/j.jbiomech.2021.110530.
%
% [10] Falisse, Antoine; Serrancolí, Gil; Dembia, Christopher L.; Gillis, 
%      Joris; Jonkers, Ilse; Groote, Friedl de (2019): Rapid predictive 
%      simulations with complex musculoskeletal models suggest that diverse 
%      healthy and pathological human gaits can emerge from similar control 
%      strategies. In: J. R. Soc. Interface. 16 (157), S. 20190402. 
%      DOI: 10.1098/rsif.2019.0402.
%
% [11] Song, Seungmoon; Geyer, Hartmut (2018): Predictive neuromechanical 
%      simulations indicate why walking performance declines with ageing. 
%      In: The Journal of Physiology 596 (7), S. 1199–1210. 
%      DOI: 10.1113/JP275166.
% 
% [12] Song, Seungmoon; Geyer, Hartmut (2015): A neural circuitry that 
%      emphasizes spinal feedback generates diverse behaviours of human 
%      locomotion. In: The Journal of Physiology 593 (16), S. 3493–3511. 
%      DOI: 10.1113/JP270228.
%
% #########################################################################
% -------------------------------------------------------------------------
%}

%% Clean Workspace - Include Folders to Path ------------------------------
clear; close all; clc

cd ..
addpath ([pwd '\Model']); 
addpath ([pwd '\Model\Init_Files']); 
addpath ([pwd '\Model\Library']); 
addpath ([pwd '\Optimization']);
cd([pwd '\Optimization']);

%% Optimization Settings --------------------------------------------------
accFlag     = true;    %-----------------Accelerator mode with fast restart
parFlag     = true;    %--------------------------Paralell computing option

%---Global variables for optim. output fun (plotting costFun contributions)
global mdl simtime MetabolicMeasure var_opt objective init_case

mdl              = 'nms_model2G_EcoWalk';
init_case        = 'Geyer2010';      % 'FGCM2023'; Init model parameters; 
                                     % Initial guess for x_opt
simtime          = '15';
MetabolicMeasure = 'Umberger2003';   % Calc_Energy.m for options
var_opt          = 'Gains';          % nms_model_Optimization.m for options          
objective        = 'Reference';      % costFun.m for options
Opt_ALG          = 'GA';             % nms_model_Optimization.m for options       

%-------------Prepare Paralell Computing, Accelerator Mode and Fast Restart
load_system(mdl);
set_param(mdl,'FastRestart','off'); %---Fast restart off to change settings
Set_Sim_Param(parFlag, accFlag, mdl, simtime);

%---------------------------------------Options for Optimization Algorithms
[opts] = Set_Opts_ALG(Opt_ALG, parFlag); 

%-----------------------------Run Optimization for nms_model2G_EcoWalk.slx
[x_opt,fval,exitflag,output] = nms_model_Optimization (mdl, init_case,...
                                       MetabolicMeasure, Opt_ALG, opts,...
                                                         var_opt, simtime); 

%------------------------------------Evaluate and Save optimization results
Eval_OPT_results;