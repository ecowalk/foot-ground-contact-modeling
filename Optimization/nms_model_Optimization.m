function [x,fval,exitflag,output] = nms_model_Optimization (mdl, init_case, MetabolicMeasure, Opt_ALG, opts, var_opt, simtime)
arguments
    mdl                   (1,:)  char 
    init_case             (1,:)  char
    MetabolicMeasure      (1,:)  char
    Opt_ALG               (1,:)  char
    opts                                       
    var_opt               (1,:)  char   
    simtime               (1,:)  char
end
%% nms_model_Optimiztion
% -------------------------------------------------------------------------
% [x,fval,exitflag,output] = nms_model_Optimization(mdl, init_case,...
%                                MetabolicMeasure, Opt_ALG, opts, var_opt,...
%                                                                  simtime)
%
% Optimizes neuromuscular model by varying var_opt using different matabolic 
% measures and optimization algorithms (Opt_ALG)
% 
% Inputs: 
%
% - mdl:        simulink model name 
% - init_case:  set of init variables to run the model  
% 
% - MetabolicMeasures:  1) 'Umberger2003' - See references in main file
%                       2) 'ACT_Squared'  - Sum of squared Muscle ACT
%                       3) 'F_MTU'        - Sum of total conc. muscle force
% 
% - Opt_ALG - Optimization Algorithm
%                       1) 'particleswarm'  Particle Swarm Optimization
%                       2) 'GA'             Genetic algorithm
%                       3) 'fmincon'        Find minimum of constrained 
%                                         nonlinear multivariable function
% 
% - opts: optimization algorithm options (created with optimoptions())
%
% - var_opt: Set parameters to be optimized 
%                       1) 'Gains'
%                       2) 'Fmax'
%                       3) 'Init_Cond'
%
% - simtime: desired simulation time (char, e.g. '10' for 10 seconds)
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       09-12-2021, Munich
% Last modified:    11-09-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Initialize Optimization Parameters

% Mechanics Configuration and Control Inputs - Initial Parameters ---------
Init_Params(init_case);

[x_0, ub, lb, nvars] = Init_xopt(var_opt, Reflex, Muscle, Init);
     
%% Optimization Algorithm
OutputFun   = false; %-------return summed reward value (not contributions)
fun         =  @(x_opt) costFun (x_opt, mdl, init_case, simtime, MetabolicMeasure, ...
                                                       var_opt, OutputFun);

switch Opt_ALG %------------------------------Choose optimization algorithm
    
    case 'particleswarm'
    %----------------------------------------------Size: SwarmSize-by-nvars
        opts.InitialSwarmMatrix  = repmat(x_0',[opts.SwarmSize 1]);
        [x,fval,exitflag,output] = particleswarm(fun,nvars,lb,ub,opts);
        
    case 'GA'
    %-----------------------------------------Size: PopulationSize-by-nvars
        opts.InitialPopulationMatrix = repmat(x_0',[opts.PopulationSize 1]);
        A       = [];
        b       = [];
        Aeq     = [];
        beq     = [];
        nonlcon = [];
    % intcon = 1:nvars; %-------------Option for mixed integer optimization
    
        [x,fval,exitflag,output] = ga(fun,nvars,A,b,Aeq,beq,lb,ub,nonlcon,opts);
    
    case 'fmincon' 
        A       = [];
        b       = [];
        Aeq     = [];
        beq     = [];
        nonlcon = [];

        [x,fval,exitflag,output] = fmincon(fun,x_0,A,b,Aeq,beq,lb,ub,nonlcon,opts);

    otherwise 
        error(['nms_model_Optimization: Opt_ALG is not supported. '...
     'Check spelling or call "help nms_model_Optimization" for details.']);
end
end