function stop = psoplotcont(optimValues,state)
% -------------------------------------------------------------------------
% stop = psoplotcont(optimValues,state) saves and plots (bar plot) the 
% individual contributions to the reward function for the best particle in 
% non-stall each generation 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       16-12-2021, Munich
% Last modified:    11-09-2022, Munich 
% 
% -------------------------------------------------------------------------

stop = false; % ~~~~~~~~~~~~~~~~~~~~~~~~~ Function does not stop the solver

% ---------------------------------------------------- Get global variables
global mdl simtime MetabolicMeasure var_opt init_case

% ------------------------------ Declare static variables for plot function
persistent best_C Cbest_fig Cbest_ax leg_names 

switch state
   case 'init' 
      OutputFun = true; %---------Return individual contributions to reward
      fun_C     =  @(x_opt) costFun (x_opt, mdl, init_case, simtime, ...
                                     MetabolicMeasure, var_opt, OutputFun);  
                                        
      x_opt     = optimValues.bestx; %--------Best individual of population
      
      Contr     = fun_C(x_opt);                                
      best_C    = Contr;
      
      Cbest_fig = figure; % ---- Create bar plot to visulaize contributions
      Cbest_ax  = axes;
      names     = {'CoT';'CoT_{ACT}';'GRF_{jerk}';'acc_{HAT}';'M_{Stop, Knee}';...
                                             'M_{Stop, Ankle}';'t_{Fall}'};         
      leg_names = {'Init'};
      
      bar(Cbest_ax, best_C);
      Cbest_leg = legend('Init');
      set(Cbest_ax,'xticklabel',names);
      title(Cbest_leg,'Iteration'); 
      
   case 'iter'
       % --------------------------- Update plot only if best value changes 
       if optimValues.stalliterations == 0
           
       OutputFun = true; %--------Return individual contributions to reward
       fun_C     = @(x_opt) costFun (x_opt, mdl, init_case, simtime, ...
                                     MetabolicMeasure, var_opt, OutputFun);
                                        
       x_opt     = optimValues.bestx;%--------Best individual of population

       Contr     = fun_C(x_opt); 
       best_C    = [best_C Contr];
        
       close(Cbest_fig);
        
       Cbest_fig = figure;
       Cbest_ax  = axes;
      names     = {'CoT';'CoT_{ACT}';'GRF_{jerk}';'acc_{HAT}';'M_{Stop, Knee}';...
                                             'M_{Stop, Ankle}';'t_{Fall}'};
                    
       leg_names = [leg_names, {num2str(optimValues.iteration)}];
       %
       h = bar(Cbest_ax, best_C);
       set(h, {'DisplayName'}, leg_names')
       Cbest_leg = legend('Location','eastoutside');
       set(Cbest_ax,'xticklabel',names);
       title(Cbest_leg,'Iteration');
       end
        
   case 'done' %--------------------------Save best populations and barplot
   fpath_opt = pwd;

        dt = datestr(datetime('now'),30); % -- 'yyyymmddTHHMMSS' (ISO 8601)
        filename = ['BestC_' dt];
        figname  = ['BestC_fig' dt];
        
        save(fullfile(fpath_opt, filename), 'best_C');
        savefig(Cbest_fig, fullfile(fpath_opt, figname));
end

