# EcoWalker - Neuromuscular Model 2nd Generation

This repository contains a neuromuscular model *(NMS)* of human gait based on *Geyer 2010* [1]. The repository is associated with the following publication:

**Buchmann, Alexandra**; Wenzler, Simon; Welte, Lauren; Renjewski, Daniel (2024): The effect of including a mobile arch, toe joint, and joint coupling on predictive neuromuscular simulations of human walking. In: Sci Rep 14 (1), S. 14879. DOI: 10.1038/s41598-024-65258-z.

We have *adapted/extended* the **EcoWalker** model to include:
- A metabolic energy calculation, according to *Umberger 2003* [3]
- Global gait optimization methods
- Data postprocessing and simulation analysis 
- Customized library different foot models *Buchmann 2022* [5]

Copyright (C) 2023 Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, Technical University of Munich.


## Running the Model
To run the NMS, open ```Run_nms_model2G_EcoWalk.m``` in *Model/* and press play. The SimMechanics Editor will automatically open in Matlab to show a video of the walking model. The script will save the simulation results in the folder *Simulation_Results/*.  
**Please understand the workflow for running the model in ```Run_nms_model2G_EcoWalk.m``` before diving into other scripts or functions!** 


## Folder Structure 
**Note: Each first-level folder contains a separate README.md to explain its features and functions. All functions/scripts have a description when calling ```help function_name``` in the Matlab command window to explain the function syntax, input, output, and, if available, options.**


**Model/**  
The folder contains the NMS ```nms_model2G_EcoWalk.slx```, all initialization files, and a custom library for features and add-ons. The NMS works with the [Simscape Multibody™ Toolbox][4] in Matlab R2019a and higher. 


**Optimization/**  
The global optimization framework is described in *Buchmann 2022* [2]. See ```main_OPT.m``` for a detailed description of the algorithm and references used, and see README.


**Visualization_and_Analysis/**  
Contains all scripts and functions for *data postprocessing* and *simulation analysis*:
- Energy and power analysis for all joints and the entire model with automatic LaTeX table generation 
- Cross-correlation analysis to compare simulation and human reference data
- All figures and tables used in our manuscript (reference will follow once the document is published)
- Human reference data
- Standard figures for all joints (ankle, knee, and hip) and muscles (GAS, SOL, TA, VAS, HAM, GLU, HFL): 
	1. Joint Kinematics
	2. Joint Torque
	3. Joint Power and Work 
	4. Muscle activation patterns
	5. Ground Reaction Forces (GRFs) and Center of Pressure(CoP)
	6. Foot Kinematics of Toe Joint and Midtarsal Joint (if applicable)
	7. Foot Arch Power and Work (if applicable)
	8. Energy Dissipation of the Foot Models (Bar plot)
- Table to LaTeX features


 **!! Caution: Git should never be used to store images, PDF files, Matlab plots, or large output files. ```Simulation_Resuls``` thus is a LOCAL folder that git ignores (see ```.gitignore```). Save/backup your results somewhere else (e.g., on your NAS drive) to avoid data loss!!**


## License 
This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/). Copyright (C) 2023 Alexandra Buchmann and Simon Wenzler.


## References: 
[1]: H. Geyer and H. Herr, "A Muscle-Reflex Model That Encodes Principles of Legged Mechanics Produces Human Walking Dynamics and Muscle Activities," in IEEE Transactions on Neural Systems and Rehabilitation Engineering, vol. 18, no. 3, pp. 263-273, June 2010, doi: 10.1109/TNSRE.2010.2047592    .

[2]: Buchmann, Alexandra; Kiss, Bernadett; Badri-Spröwitz, Alexander Thomas; Renjewski, Daniel: Power to the Springs: Passive Elements are Sufficient to Drive Push-Off in Human Walking. In: Robotics in Natural Settings, S. 21–32, DOI: 10.1007/978-3-031-15226-9_5.

[3]: Umberger, Brian R.; Gerritsen, Karin G. M.; Martin, Philip E. (2003): A model of human muscle energy expenditure. In: Computer Methods in Biomechanics and Biomedical Engineering 6 (2), S. 99–111. DOI: 10.1080/1025584031000091678    .

[4]: <https://de.mathworks.com/products/simscape-multibody.html> "Simscape Multibody™ Toolbox"

[5]: Buchmann, Alexandra; Wenzler, Simon; Welte, Lauren; Renjewski, Daniel (2024): The effect of including a mobile arch, toe joint, and joint coupling on predictive neuromuscular simulations of human walking. In: Sci Rep 14 (1), S. 14879. DOI: 10.1038/s41598-024-65258-z.
