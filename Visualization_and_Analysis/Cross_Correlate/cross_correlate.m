function [out_corr] = cross_correlate(out_sim, out_human)
arguments
   out_sim      struct
   out_human    struct
end
% -------------------------------------------------------------------------
% [out_corr] = cross_correlate(out_sim, out_human)
%
% calculates the maximum normalized cross correlation and signal delay 
% seperately for swing and stance phase (see Geyer 2010 - Results) between 
% simulation data and human data 
%
% Input: 
% - out_sim:    Simulink simulation output preprocessed with 
%                         data_preprocess.m followed by resample_sim_data.m
% - out_human:  human data preprocessed with resample_human_data.m
%
% Output: 
% - out_corr:   struct with cross correlations R and signal delays Del 
%               seperated in swing and stance for all muscle activations 
%               (GAS, SOL, TA, VAS, HAM, GLU, HFL) and joint angles phi 
%               (ankle, knee, hip)
% 
% Functions used:
%  r = xcorr(x,y,'scaleopt') returns the cross-correlation of two 
%  discrete-time sequences. Cross-correlation measures the similarity 
%  between a vector x and shifted (lagged) copies of a vector y as a 
%  function of the lag. 
%
%  D = finddelay(X,Y), where X and Y are row or column vectors of length LX 
%  and LY, respectively, returns an estimate of the delay D between X and Y, 
%  where X serves as the reference vector. If Y is delayed with respect to 
%  X, D is positive. If Y is advanced with respect to X, D is negative. 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-12-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Calculate Percent Stride (signal delay)
PCS = 1/length(out_human.ACT.HFLal.Time);

%% Norm to BW
options.norm_to_bw = 1;
norm = norm_to_bw(options);

out_sim.GRFs.Data        = out_sim.GRFs.Data/norm.GRFs_hum;
out_human.GRFs.prog.Data = out_human.GRFs.prog.Data/norm.GRFs;
out_sim.P_T.Data         = out_sim.P_T.Data*norm.m_bw_de_hum;

out_human.Power.Ankle.Data  = out_human.Power.Ankle.Data/norm.m_bw;
out_human.Power.Knee.Data   = out_human.Power.Knee.Data/norm.m_bw;
out_human.Power.Hip.Data    = out_human.Power.Hip.Data/norm.m_bw;
out_human.Torque.Ankle.Data = out_human.Torque.Ankle.Data/norm.m_bw;
out_human.Torque.Knee.Data  = out_human.Torque.Knee.Data/norm.m_bw;
out_human.Torque.Hip.Data   = out_human.Torque.Hip.Data/norm.m_bw;

%% Max. Normalized Correlation for Stance Phase ------- ACT, RoM ----------

muscle    = {'HFLal', 'GLUmax', 'HAMsb', 'VAS', 'GAS', 'SOL', 'TA'};                                              
joint     = {'Ankle', 'Knee', 'Hip'};
phi_index = [ 1     ,  3    ,  5   ];
phi_conv  = [ 90    , 180   , 180  ];
T_index   = [ 4     ,  5    ,  6   ];
P_index   = [ 1     ,  2    ,  3   ];


% ----------------------------------------------------------------- Muscles
for i = 1:length(muscle)
    
% ------------------------------------------------------ Stance correlation
eval(['out_corr.stance.R_' char(muscle(i)) ' = max(xcorr('...
        'out_human.ACT.'         char(muscle(i)) '.Data'...
        '(1:find(out_human.ACT.' char(muscle(i)) '.Time == 0.6)),'...
        'out_sim.ACT.Data(1:find(out_sim.ACT.Time == 0.6), i),'...
                                                       '"normalized"));']);
                                                   
% ------------------------------------------------------- Swing correlation
eval(['out_corr.swing.R_' char(muscle(i)) ' = max(xcorr('...
        'out_human.ACT.'       char(muscle(i)) '.Data'...
        '(find(out_human.ACT.' char(muscle(i)) '.Time == 0.6) : end),'...
        'out_sim.ACT.Data(find(out_sim.ACT.Time == 0.6) : end, i),'...
                                                       '"normalized"));']);                                                   
    
% ------------------------------------------------------------ Stance delay   
eval(['out_corr.stance.Del_' char(muscle(i)) '= finddelay('...
         'out_human.ACT.'         char(muscle(i)) '.Data'...
         '(1:find(out_human.ACT.' char(muscle(i)) '.Time == 0.6)),'...
         'out_sim.ACT.Data((1:find(out_sim.ACT.Time == 0.6)), i))* PCS;']);
 
% --------- Swing delay (not for GAS and SOl as they are inactive in swing)   
if ~ (strcmp(char(muscle(i)), 'GAS') || strcmp(char(muscle(i)), 'SOL'))
eval(['out_corr.swing.Del_' char(muscle(i)) '= finddelay('...
         'out_human.ACT.'       char(muscle(i)) '.Data'...
         '(find(out_human.ACT.' char(muscle(i)) '.Time == 0.6) : end),'...
         'out_sim.ACT.Data((find(out_sim.ACT.Time == 0.6):end),i))* PCS;']);
end
end


% ------------------------------------------------ Joint angles and torques
for j = 1:length(joint)
eval(['out_corr.stance.R_phi_' char(joint(j))...
            '= max(xcorr(out_human.RoM.' char(joint(j)) '.Data('...
              '1:find(out_human.RoM.' char(joint(j)) '.Time == 0.6)),'...
              'rad2deg(out_sim.Dyn.Data((1:find(out_sim.Dyn.Time == 0.6)),'...
              num2str(phi_index(j)) '))*(-1)+' num2str(phi_conv(j))...
                                                      ',"normalized"));']);

eval(['out_corr.stance.R_T_' char(joint(j)) '= max(xcorr(out_human.Torque.'...
                                                char(joint(j)) '.Data,'...
           'out_sim.P_T.Data(:,' num2str(T_index(j)) '),"normalized"));']);

eval(['out_corr.stance.Del_T_' char(joint(j)) '= finddelay(out_human.Torque.'...
                                                 char(joint(j)) '.Data,'...
              'out_sim.P_T.Data(:,' num2str(T_index(j)) '))* 0.6 * PCS;']);
          
eval(['out_corr.stance.R_P_' char(joint(j)) '= max(xcorr(out_human.Power.'...
                                                char(joint(j)) '.Data,'...
           'out_sim.P_T.Data(:,' num2str(P_index(j)) '),"normalized"));']);

eval(['out_corr.stance.Del_P_' char(joint(j)) '= finddelay(out_human.Power.'...
                                                 char(joint(j)) '.Data,'...
              'out_sim.P_T.Data(:,' num2str(P_index(j)) '))* 0.6 * PCS;']);

eval(['out_corr.swing.R_phi_' char(joint(j)) '= max(xcorr(out_human.RoM.'...
                char(joint(j)) '.Data(find(out_human.RoM.' char(joint(j))...
                                                    '.Time == 0.6):end),'...
            'rad2deg(out_sim.Dyn.Data((find(out_sim.Dyn.Time == 0.6):end),'...
num2str(phi_index(j)) '))*(-1)+' num2str(phi_conv(j)) ',"normalized"));']);    

end

end

