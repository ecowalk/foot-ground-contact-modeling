function [R_ankle, R_knee, R_hip] = get_R_ROM(out_corr)
%% get_R_ROM
% -------------------------------------------------------------------------
% [R_ankle, R_knee, R_hip] = get_R_ROM(out_corr) extracts cross correlation
% values for range of motion in swing and stance from output data created
% by cross_correlate.m
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       10-11-2023, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

R_ankle = zeros(2,numel(out_corr));
R_knee  = zeros(2,numel(out_corr));
R_hip   = zeros(2,numel(out_corr));

% Extract ankle, knee and hip R value for swing and stance
for i = 1 : numel(out_corr)
    R_ankle(:,i)= [out_corr{i}.stance.R_phi_Ankle, out_corr{i}.swing.R_phi_Ankle];
    R_knee(:,i) = [out_corr{i}.stance.R_phi_Knee, out_corr{i}.swing.R_phi_Knee];
    R_hip(:,i)  = [out_corr{i}.stance.R_phi_Hip, out_corr{i}.swing.R_phi_Hip];
end

