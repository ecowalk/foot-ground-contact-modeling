%% main_cross_correlate.m
% -------------------------------------------------------------------------
% main_cross_correlate.m - cross correlation analysis for simulation output 
% in comparison to human data
%
% Script supports:
%       1) Data PreProcessing: cropping to one step, normalizing to stride 
%                              time, filter GRF 
%                              (see "help data_preprocess")
%
%       2) Resample human and simulation data to uniform frequency
%          (see "help resample_human_data" or "help resample_sim_data")
%
%       3) Cross correlation analysis (see "help cross_correlate")
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------
%% References
%{ 
% #########################################################################
%
% Ankle Power -------------------------------------------------------------
% [1] Cronin, Neil J.; Prilutsky, Boris I.; Lichtwark, Glen A.; Maas, Huub 
%    (2013): Does ankle joint power reflect type of muscle action of soleus
%     and gastrocnemius during walking in cats and humans? In: Journal of 
%     Biomechanics 46 (7), S. 1383–1386. DOI: 10.1016/j.jbiomech.2013.02.023.
% 
% Ankle Power & Torque; Energy and Impulse --------------------------------
% [2] Lipfert, Susanne W.; Günther, Michael; Renjewski, Daniel; Seyfarth, 
%     André (2014): Impulsive ankle push-off powers leg swing in human 
%     walking. In: Journal of Experimental Biology 217 (8), S. 1218–1228. 
%     DOI: 10.1242/jeb.097345.
% 
% Joint Torques & Angles (RoM), GRFs, Muscle Activations ------------------
% [3] Perry, Jacquelin; Burnfield, Judith M.; Cabico, L. M. (2010): 
%     Gait Analysis. Normal and Pathological Function: SLACK.
% 
% Muscle Length-Time Relations for GAS & SOL ------------------------------
% [4] Ishikawa, Masaki; Komi, Paavo V.; Grey, Michael J.; Lepola, Vesa; 
%     Bruggemann, Gert-Peter (2005): Muscle-tendon interaction and elastic 
%     energy usage in human walking. In: Journal of Applied Physiology 
%     (Bethesda, Md. : 1985) 99 (2), S. 603–608. 
%     DOI: 10.1152/japplphysiol.00189.2005.
% 
% [5] Lichtwark, Glen A.; Wilson, Alan M. (2006): Interactions between the 
%     human gastrocnemius muscle and the Achilles tendon during incline, 
%     level and decline locomotion. In: The Journal of Experimental Biology 
%     209 (Pt 21), S. 4379–4388. DOI: 10.1242/jeb.02434.
%
% [6] Matlab Help: Resampling nonuniformly sampled signals
%     https://de.mathworks.com/help/signal/ug/resampling-nonuniformly-sampled-signals.html
%
% [7] https://de.wikipedia.org/wiki/Korrelation_(Signalverarbeitung)
% 
% [8] https://de.mathworks.com/help/matlab/ref/xcorr.html#mw_5302e782-55af-4f39-a990-a27d094ad3ea
% 
% Energetics of Swing and Stance ------------------------------------------
% [9] Umberger, Brian R. (2010): Stance and swing phase costs in human 
%     walking. In: J. R. Soc. Interface. 7 (50), S. 1329–1340. 
%     DOI: 10.1098/rsif.2010.0084.
% #########################################################################
%}

%% Init -------------------------------------------------------------------
close all; clear; clc

stk = dbstack; filepath = which(stk(1).file);
local_path= filepath(1:strfind(filepath,'Visualization_and_Analysis')-1);

addpath ([local_path 'Visualization_and_Analysis']); 
addpath ([local_path 'Visualization_and_Analysis\Cross_Correlate']); 
addpath ([local_path 'Visualization_and_Analysis\Human_Data']); 
addpath ([local_path 'Visualization_and_Analysis\Standard_Figures']); 

%% Set Path to Working Directory (local)-----------------------------------
% Results for optimized parameters
fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_Opt_1000');
fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_Opt_1000');
fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_Opt_1000');
fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_Opt_1000');
fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_Opt_1000');
fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_Opt_1000');
res_var = 'Result_Opt_1000';

% Results for non optimized parameters
% fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_No_Opt_1000');
% fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_No_Opt_1000');
% fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_No_Opt_1000');
% fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_No_Opt_1000');
% fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_No_Opt_1000');
% fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_No_Opt_1000');
% res_var = 'Result_No_Opt_1000';


%% Initialize Variables --------------------------------------------------- 

% Step Number to be evaluted (-1 for last full step)
step_no = -1;

% Data PreProcess
filter.flag   = 1; 
filter.Fpass  = 50;     % Fpass = passband frequency of lowpass filter [Hz]

% Data Resampling 
res.fs = 1000;          % Sample frequency
res.p = 3; 
res.q = 1;              % Resampling factors

%% Process Simulation Output ---------------------------------------------- 
[out1] = data_preprocess(fpath_ref1l, res_var, step_no, filter);
[out2] = data_preprocess(fpath_ref1h, res_var, step_no, filter);
[out4] = data_preprocess(fpath_ref2m, res_var, step_no, filter);
[out3] = data_preprocess(fpath_ref2t, res_var, step_no, filter);
[out5] = data_preprocess(fpath_ref3n, res_var, step_no, filter);
[out6] = data_preprocess(fpath_ref3w, res_var, step_no, filter);

%% Cross Correlation Analysis --------------------------------------------- 
out_human = resample_human_data(res.fs,res.p,res.q);
out_cases = [out1 out2 out3 out4 out5 out6];
out_sim   = cell(6,1);
out_corr  = cell(6,1);

for i = 1:numel(out_cases)
    out_sim{i} = resample_sim_data(out_cases(i),res.fs,res.p,res.q);
    out_corr{i}= cross_correlate(out_sim{i}, out_human);
end

% Get cross-correlation for hip, knee and ankle kinematics
[R_ankle, R_knee, R_hip] = get_R_ROM(out_corr);
