function [y,t] = resample_NZE(x,tx,fs,p,q)
% -------------------------------------------------------------------------
% [y,t] = resample_NZE(x,tx,fs,p,q) 
%
% eliminates edge effects (oscialltions) created by resample(x,tx,fs,p,q) 
% if signals have non-zero values at the edges (NZE) - Details see [1]
%
%     [y,t] = resample(x,tx,fs,p,q) - interpolates the input signal to an 
%     intermediate uniform grid with a sample spacing of (p/q)/fs. Function 
%     then filters the result to upsample it by p and downsample it by q, 
%     resulting in a final sample rate of fs. 
%
%     For best results, ensure that fs × q/p is at least twice as large as
%     highest frequency component of x.
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       30-11-2021, Munich
% Last modified:    12-12-2022, Munich 
% 
% -------------------------------------------------------------------------

%% Detect non-zero edges

if (x(1) || x(end) ~= 0)
    
% Compute slope and offset (y = a1 x + a2)
a(1)         = (x(end)-x(1))/(tx(end)-(tx(1)));
a(2)         = x(1);

% Detrend signal
xdetrend     = x - polyval(a,tx);

[xdetrend,t] = resample (xdetrend,tx,fs,p,q);
y            = xdetrend + polyval(a,t);

else 
[y,t]        = resample (x,tx,fs,p,q);
end

end

