function [out] = resample_human_data(fs,p,q)
arguments
    fs      double
    p       double
    q       double
end
% -------------------------------------------------------------------------
% [out] = resample_human_data(fs,p,q)
%
% resamples human data digitzed from references (see below in references)
%
% Functions used:
%
%  [y,t] = resample_NZE(x,tx,fs,p,q) - resample_NZE(x,tx,fs,p,q) eliminates 
%    edge effects (oscialltions created by resample(x,tx,fs,p,q) if signals 
%    have non-zero values at the edges.
%
%  For best results, ensure that fs × q/p is at least twice as large as the 
%  highest frequency component of x.
% 
% [out]  -   struct with fields:    time: 0-1 (percent of stride or stance)
%              ACT: [1×1 struct] - [% MMT]                         - stride
%             GRFs: [1×1 struct] - [% bw]                          - stance
%              RoM: [1×1 struct] - [deg]                           - stride
%           Torque: [1×1 struct] - [% bw per leg length]           - stance
%            Power: [1×1 struct] - [W/kg]                          - stride
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      30-11-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

human_data = load_human_data(1:7);
var_names = fieldnames(human_data);

% Resample human data
for i = 1:numel(var_names)
    if size(human_data.(var_names{i}),1)>1 && size(human_data.(var_names{i}),2)==2
        
        time = human_data.(var_names{i})(:,1)/max(human_data.(var_names{i})(:,1));
        data = human_data.(var_names{i})(:,2);
        
        [y,t] = resample_NZE(data, time, fs,p,q);
        eval([var_names{i} ' = y;']);
        eval(['t' var_names{i} ' = t;']);
    end
end

%% Create Output File~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% --------------------------------------------------------------------- ACT
out.ACT.GAS             =  timeseries(ACTGAS,      tACTGAS');
out.ACT.HFLal           =  timeseries(ACTHFLal,    tACTHFLal');
out.ACT.SOL             =  timeseries(ACTSOL,      tACTSOL');
out.ACT.GLUmax          =  timeseries(ACTGLUmax,   tACTGLUmax');
out.ACT.HAMsb           =  timeseries(ACTHAMsb,    tACTHAMsb');
out.ACT.TA              =  timeseries(ACTTA,       tACTTA');
out.ACT.VAS             =  timeseries(ACTVASlat,   tACTVASlat');

% -------------------------------------------------------------------- GRFs
out.GRFs.prog           = timeseries(GRFprog,           tGRFprog');
out.GRFs.vertical       = timeseries(GRFvertical,       tGRFvertical');

% --------------------------------------------------------------------- RoM
out.RoM.Hip             = timeseries(HipRoM,            tHipRoM');
out.RoM.Knee            = timeseries(KneeRoM,           tKneeRoM');
out.RoM.Ankle           = timeseries(AnkleRoM,          tAnkleRoM');

% ----------------------------------------------------------------- Torques
out.Torque.Hip               =  timeseries(HipTorque,        tHipTorque');
out.Torque.Knee              =  timeseries(KneeTorque,       tKneeTorque');
out.Torque.Ankle             =  timeseries(AnkleTorque,      tAnkleTorque');


% ------------------------------------------------------------- Ankle Power
out.Power.Hip               =  timeseries(HipPower,         tHipPower');
out.Power.Knee              =  timeseries(KneePower,        tKneePower');
out.Power.Ankle             =  timeseries(AnklePower,       tAnklePower');


end
