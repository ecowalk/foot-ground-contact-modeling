function [out] = resample_sim_data(in,fs,p,q)
arguments
    in      Simulink.SimulationOutput
    fs      double
    p       double
    q       double
end
% -------------------------------------------------------------------------
% [out] = resample_sim_data(in,fs,p,q) 
% 
% resamples simulation data in with sampling frequency fs 
% 
% Functions used:
%
%  [y,t] = resample_NZE(x,tx,fs,p,q) - resample_NZE(x,tx,fs,p,q) eliminates 
%    edge effects (oscialltions created by resample(x,tx,fs,p,q) if signals 
%    have non-zero values at the edges.
%
%    For best results, ensure that fs × q/p is at least twice as large as the 
%    highest frequency component of x.
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       30-11-2021, Munich
% Last modified:    12-12-2022, Munich 
% 
% -------------------------------------------------------------------------

%% Resample Simulation Data
% Hand over static variables
out.end_time       = in.end_time;
out.start_time     = in.start_time;
out.stride_time    = in.stride_time;
out.take_off       = in.take_off;
out.take_off_norm  = in.take_off_norm;
 
% Limit power-torque data to Lstance phase only and resample (for comp.
% to human data - Perry et al. 2010 only reports Lstance torques) 
in.P_T.Time = in.P_T.Time/in.take_off_norm;
in.P_T      = delsample (in.P_T, 'Index', ...
                   (find(in.P_T.Time > 1,1,'first'): length(in.P_T.Data)));
               
vars = {'Dyn', 'GRFs', 'ACT', 'F_MTU', 'L_MTU', 'P_T'};

for i = 1:length(vars) 
    for j = 1:eval(['min(size(in.' char(vars(i)) '.Data))']) 
     eval(['[out.' char(vars(i)) '.Data(:,j),out.' char(vars(i)) '.Time] =' ...
                             'resample_NZE(in.' char(vars(i)) '.Data(:,j),'...
                                    'in.' char(vars(i)) '.Time,fs,p,q);']);
    end
end

end
