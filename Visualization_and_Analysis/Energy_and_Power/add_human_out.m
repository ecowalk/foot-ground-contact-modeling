function [simout, trial_names, stride_range] = add_human_out(simout, ...
                             trial_names, stride_range, stride_range_human)
arguments
    simout          (1,:)   Simulink.SimulationOutput
    trial_names     (1,:)   cell 
    
    stride_range       (:,2) double  = []   
    stride_range_human (1,2) double  = [0 100] 
end
% -------------------------------------------------------------------------
% [simout, trial_names, stride_range] = ...
%                          add_human_out(simout, trial_names, stride_range)
%
% adds human dataset to simout vector with similar structure than simulink
% simulation output data to facilitate human data integration into the
% exisiting table creation framework
%
% - extends simout by humanout
% - adds {'Human'} to trial names 
% - adds stride_range_human to stride range vector 
% 
% For details on data structure of humanout call help creat_humanout.m
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       26-01-2023, Munich
% Last modified:    26-01-2023, Munich 
% 
% -------------------------------------------------------------------------

% -------------------------------- Expand simout with human comparison data
    [humanout]   = create_humanout('vanderZee2022');
    simout       = [humanout, simout];
    trial_names  = [{'Human'}, trial_names]; 
    stride_range = [stride_range_human; stride_range];
end

