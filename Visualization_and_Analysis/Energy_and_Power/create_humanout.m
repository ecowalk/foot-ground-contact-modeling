function [humanout] = create_humanout (datasource)
arguments 
    datasource (1,:) char 
end
% -------------------------------------------------------------------------
% [humanout] = create_humanout ()
% 
% creates a humanout data structure matching the dataformat of simout.P_T
% to be used as reference data for energy table analysis from van der Zee
% 2022 dataset in Human_Data folder
%
% Output: humanout.
%            take_off_norm: 0.6400
%                      P_T: [1×1 timeseries]
%                      Dyn: [1×1 timeseries]
%                SMC_Ankle: [1×1 timeseries]
%                 SMC_Knee: [1×1 timeseries]
%                  SMC_Hip: [1×1 timeseries]
%                   VelHAT: [1×1 timeseries]
%                       DF: 0.6400
%     MetabEnergy_Umberger: NaN
%                   PosHAT: [1×1 timeseries]
%              stride_time: NaN
%                tag_human: true  - tag to distinguish from simulation data
%
% For details on selected human data call help select_samples_vdZee
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-01-2023, Munich
% Last modified:    26-01-2023, Munich 
% 
% -------------------------------------------------------------------------
%% References
% #########################################################################
% [1] van der Zee, Tim J.; Mundinger, Emily M.; Kuo, Arthur D. (2022): A 
%     biomechanics dataset of healthy human walking at various speeds, step 
%     lengths and step widths. In: Sci Data 9 (1), S. 704. 
%     DOI: 10.1038/s41597-022-01817-1.
% #########################################################################

%% --------------------------------------------  Load human comparison data
if strcmp(datasource,'vanderZee2022')
load('vdZee_Torque.mat','vdZeeAnkleTorque','vdZeeHipTorque','vdZeeKneeTorque')
load('vdZee_Power.mat','vdZeeAnklePower','vdZeeKneePower','vdZeeHipPower')
load('vdZee_GRFs.mat','vdZeeGRFvertical')
load('vdZee_StrideTime.mat','vdZeeStrideTime');
else
    error("create_humanout: Human data source has to be 'vanderZee2022'!");
end

%% -------------------------------------------------- Init Known Quantities
take_off = max(find(vdZeeGRFvertical(:,2) <= 20, 2)); % detection theshold of 20N
humanout.take_off_norm = vdZeeGRFvertical(take_off,1)/100;
vector_size = length(vdZeeGRFvertical);

% ------ simout structure '[LP12 LP23 LP34 LM12 LM23 LM34]' with [W t[0-1]]
humanout.P_T = timeseries([vdZeeAnklePower(:,2)*80, ...
                            vdZeeKneePower(:,2)*80,...
                            vdZeeHipPower(:,2)*80,...
                            vdZeeAnkleTorque(:,2)*80, ...
                            vdZeeKneeTorque(:,2)*80,...
                            vdZeeHipTorque(:,2)*80],vdZeeAnklePower(:,1)/100,...
                            'Name','[LP12 LP23 LP34 LM12 LM23 LM34]'); 
                    
                        
humanout.VelHAT = timeseries(1.25 , [], 'Name','[dx]');  
humanout.stride_time = vdZeeStrideTime;
humanout.DF = humanout.take_off_norm; % ----------------------- duty factor 

% ------------------- Average leg length (from paper [1] for Froude Number)
leg_length = [0.89; 0.94; 1.04; 0.88; 0.86; 0.94; 0.99; 0.99; 0.94; 0.86];
humanout.leg_length = mean(leg_length);

% --- Tag to skip error messages and warning only applicable for simualtion
humanout.tag_human = true;

%% ----------------------------------------------------------- Empty Arrays
humanout.MetabEnergy_Umberger = NaN;
humanout.PosHAT = timeseries(NaN, []);
humanout.MetabPower = timeseries(NaN(vector_size,1), zeros(vector_size,1),...
                                                                'Name',''); 
                                                            
% ------------- simout structure '[phi12 dphi12 phi23 dphi23 phi34 dphi34]'
humanout.Dyn = timeseries([NaN(vector_size,1),...
                           NaN(vector_size,1),...
                           NaN(vector_size,1),...
                           NaN(vector_size,1),...
                           NaN(vector_size,1),...
                           NaN(vector_size,1)], zeros(vector_size,1),...
                        'Name','[phi12 dphi12 phi23 dphi23 phi34 dphi34]'); 
                        
% --------------------------- SMC_Ankle '[LAnkle Stop LM_GAS LM_SOL LM_TA]'
humanout.SMC_Ankle = timeseries([NaN(vector_size,1),...
                                 NaN(vector_size,1),...
                                 NaN(vector_size,1),...
                                 NaN(vector_size,1),...
                                 NaN(vector_size,1)], zeros(vector_size,1),...
                               'Name','[LAnkle Stop LM_GAS LM_SOL LM_TA]'); 

% ---------------------------- SMC_Knee '[LKnee Stop LM_GAS LM_HAM LM_VAS]'
humanout.SMC_Knee = timeseries([NaN(vector_size,1),...
                                NaN(vector_size,1),...
                                NaN(vector_size,1),...
                                NaN(vector_size,1),...
                                NaN(vector_size,1)], zeros(vector_size,1),...
                               'Name','[LKnee Stop LM_GAS LM_HAM LM_VAS]'); 

% ------------------------------ SMC_Hip '[LHip Stop LM_GLU LM_HAM LM_HFL]'
humanout.SMC_Hip = timeseries([NaN(vector_size,1),...
                               NaN(vector_size,1),...
                               NaN(vector_size,1),...
                               NaN(vector_size,1),...
                               NaN(vector_size,1)], zeros(vector_size,1),...
                                'Name','[LHip Stop LM_GLU LM_HAM LM_HFL]');
end
