function [E] = energy_analysis (simout)
arguments 
    simout  (1,:)  Simulink.SimulationOutput               
end
% -------------------------------------------------------------------------
% [E] = energy_analysis (simout)
%
% calculates metabolic energy for swing and stance phase as well as the 
% mechanical power joint energy of [Ankle12 Knee23 Hip34 Sum] for stance 
% and swing
%
% Inputs:   - simout:   simulation output (use DataPreProcess before!)
%
% Output:   E. ------- 
%            Stance_Pos: [1x4 double]
%            Stance_Neg: [1x4 double]
%             Swing_Pos: [1x4 double]
%             Swing_Neg: [1x4 double]
%                   Net: [1x4 double]
%            Net_Stance: [1x4 double]
%             Net_Swing: [1x4 double]
%              Net_Cont: [1x4 double]
%       Net_Stance_Cont: [1x4 double]
%        Net_Swing_Cont: [1x4 double]
%           Rate_Stance: [1x4 double]
%            Rate_Swing: [1x4 double]
%          -------------------------- only for simulation data
%          Metab_Stance: double 
%           Metab_Swing: double 
%             Metab_Sum: double 
%     Metab_Stance_Cont: double 
%      Metab_Swing_Cont: double 
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       08-02-2022, Munich
% Last modified:    25-01-2023, Munich 
% 
% -------------------------------------------------------------------------

%% --------------------------- Split energy calculation in stance and swing
take_off = find(simout.P_T.Time <= simout.take_off_norm, 1, 'last');
d_t      = diff(simout.P_T.Time); 

% ------------------------ Joint energy - stance [Ankle12 Knee23 Hip34 Sum]
E.Stance_Pos(1:3) = sum(d_t(1:take_off).*...
                                   max(simout.P_T.Data(1:take_off,1:3),0));
                                  
E.Stance_Neg(1:3) = sum(d_t(1:take_off).*...
                                   min(simout.P_T.Data(1:take_off,1:3),0));
                                  
% ------------------------- Joint energy - swing [Ankle12 Knee23 Hip34 Sum]
E.Swing_Pos(1:3) = sum(d_t(take_off:end).*...
                             max(simout.P_T.Data(take_off:(end-1),1:3),0));
                            
E.Swing_Neg(1:3) = sum(d_t(take_off:end).*...
                             min(simout.P_T.Data(take_off:(end-1),1:3),0));
                                                 
E.Stance_Pos(4) = sum(E.Stance_Pos(1:3));
E.Stance_Neg(4) = sum(E.Stance_Neg(1:3));                           
E.Swing_Pos(4)  = sum(E.Swing_Pos(1:3));                 
E.Swing_Neg(4)  = sum(E.Swing_Neg(1:3));

% ------------------------------------------------------ Total joint energy
E.Net           = E.Stance_Pos + E.Stance_Neg + E.Swing_Pos + E.Swing_Neg;

% ------------------------------------------- Total swing and stance energy
E.Net_Stance    = E.Stance_Pos + E.Stance_Neg;                   
E.Net_Swing     = E.Swing_Pos  + E.Swing_Neg;

% -------------------------------------- Energetic contributions in percent
E.Net_Cont        = (E.Net        / E.Net(4)        )*100;
E.Net_Stance_Cont = (E.Net_Stance / E.Net_Stance(4) )*100;
E.Net_Swing_Cont  = (E.Net_Swing  / E.Net_Swing(4)  )*100;

% ------------------------------------------------------ Joint engery rates
E.Rate_Stance     = abs(E.Stance_Pos./E.Stance_Neg);                                
E.Rate_Swing      = abs(E.Swing_Pos./E.Swing_Neg);

try
% ----------------------------------------------- Metabolic energy per step 
take_off = find(simout.MetabPower.Time <= simout.take_off_norm, 1, 'last');
d_t      = diff(simout.MetabPower.Time); 

E.Metab_Stance = sum(d_t(1:take_off).*simout.MetabPower.Data(1:take_off));                   
E.Metab_Swing  = sum(d_t(take_off:end).*...
                                 simout.MetabPower.Data(take_off:(end-1)));

    catch
    disp (['energy_analysis: No evaluation of metabolic energy'...
                               ' contributions for human data possible!']);
    E.Metab_Stance = NaN;                   
    E.Metab_Swing  = NaN;
                                
end

E.Metab_Sum          =  E.Metab_Stance + E.Metab_Swing;
E.Metab_Stance_Cont  = (E.Metab_Stance / E.Metab_Sum   )*100;                                  
E.Metab_Swing_Cont   = (E.Metab_Swing  / E.Metab_Sum   )*100;

% ----------------------------------------------------- Round to two digits
    function out = round_struct(S)
        out = round(S, 2);
    end
    E = structfun(@round_struct, E,'UniformOutput',false);
    
end
