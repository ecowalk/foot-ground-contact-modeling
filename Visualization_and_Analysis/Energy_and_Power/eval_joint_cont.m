function [smc] = eval_joint_cont (simout, j_index, stride_range)
arguments 
    simout        (1,:)    Simulink.SimulationOutput  
    j_index       (1,1)    double {mustBeMember(j_index,[12,23,34])}
    stride_range  (1,2)    double                       = zeros(1,2)
end
% -------------------------------------------------------------------------
% [eval_out] = eval_joint_cont (simout, j_index, region)
% 
% calculates energy and power contributions of the muscles during stance to
% the joint given in the index; checks joint stop moments and warns, if
% stop moments exist
% 
% Inputs:   - simout:   simulation output (use DataPreProcess before!)
%           - j_index:  joint to be avaluated
%               * Ankle '12'
%               * Knee  '23'
%               * Hip   '34'
%           -  region:  %stride time the data is analysed; 
%               * default - stance
%               * example - [20 70] to evaluate from 20-70\% stride
%
% Output:   smc. ----- T: [:×3 double] (single muscle joint torque)
%                      P: [:×3 double] (single muscle joint power)
%       -------------------- joint and single muscles ----------------
%                  P_Pos: [:×4 double] 
%                  P_Neg: [:×4 double] 
%                  E_Pos: [1×4 double]
%                  E_Neg: [1×4 double]
%                  E_Sum: [1×4 double]
%                 E_Cont: [1×4 double]
%             E_Cont_Pos: [1×4 double]
%             E_Cont_Neg: [1×4 double]
%                 P_Cont: [:×4 double]
%                 T_Cont: [:×4 double]
%                  P_max: [1×4 double]
%                  P_min: [1×4 double]
%     Amplification_Rate: [1×4 double]
%             P_max_Cont: [1×4 double]
%             P_min_Cont: [1×4 double]
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       07-02-2022, Munich
% Last modified:    13-12-2022, Munich 
% 
% -------------------------------------------------------------------------
       
%% Select Joint

switch j_index
    % out.Dyn '[phi12 dphi12 phi23 dphi23 phi34 dphi34]'
    % out.P_T '[LP12  LP23   LP34  LM12   LM23  LM34]'
    
    case 12 % ------------------- Ankle '[LAnkle Stop LM_GAS LM_SOL LM_TA]'
        SMC      = simout.SMC_Ankle;
        Joint    = 'Ankle';
        Dyn_Data = simout.Dyn.Data(:,2);
        T_joint  = simout.P_T.Data(:,4);
        P_joint  = simout.P_T.Data(:,1);
        
    case 23 % -------------------- Knee '[LKnee Stop LM_GAS LM_HAM LM_VAS]'
        SMC      = simout.SMC_Knee;
        Joint    = 'Knee';
        Dyn_Data = simout.Dyn.Data(:,4);
        T_joint  = simout.P_T.Data(:,5);
        P_joint  = simout.P_T.Data(:,2);
        
    case 34 % ---------------------- Hip '[LHip Stop LM_GLU LM_HAM LM_HFL]'
        SMC      = simout.SMC_Hip;
        Joint    = 'Hip';
        Dyn_Data = simout.Dyn.Data(:,6);
        T_joint  = simout.P_T.Data(:,6);
        P_joint  = simout.P_T.Data(:,3);
        
otherwise
         error('Specify correct j_index - Must be member of [12;23;34]');
end

%% Evaluate Power Contributions 
smc.T = [SMC.Data(:,2),...
         SMC.Data(:,3),...
         SMC.Data(:,4)*(-1)]; % -------------------------------- Antagonist

smc.P = smc.T.*Dyn_Data;

if ~any(strcmp(who(simout), 'tag_human')) % ------ Only for simulation data
% ---------------------------------------------- Check Stop Moment & Torque
    T_Stop   = SMC.Data(:,1);  
    P_Stop   = T_Stop.*Dyn_Data;  

    temp     = find(T_Stop ~= 0,1);
    temp2    = find(P_Stop ~= 0,1);

if isempty(temp) && isempty(temp2)
    disp([Joint ': Stop Moment Check OK!'])
else 
  warning([Joint ': Stop Moment Check in Eval_Joint Failed - Check Data!'])
end

% ------------------------------------------------ Check Power & Torque Sum
T_Sum   = T_Stop + smc.T(:,1) + smc.T(:,2) + smc.T(:,3);
P_Sum   = P_Stop + smc.P(:,1) + smc.P(:,2) + smc.P(:,3); 

% ----------------------------------- P_T '[LP12 LP23 LP34 LM12 LM23 LM34]'
diff_T  = round((T_joint - T_Sum),2);
diff_P  = round((P_joint - P_Sum),2);

temp    = find(diff_T ~= 0,1);
temp2   = find(diff_P ~= 0,1);

if isempty(temp) && isempty(temp2)
    disp([Joint ': Power and Torque Sum Check OK!'])
else 
 warning([Joint ': Power Sum Check in Eval_Joint Failed - Check Data!'])
end

    else
        disp(['eval_joint_cont: Processing Human Data -'...
                                           ' No Power and Torque Check.']);
end
  
%% Power contributions for stride_range
if ~any(stride_range) % --- default: stance phase from 10\% until take off
    
    take_off = find(simout.P_T.Time <= simout.take_off_norm, 1, 'last');
    start_index = find(simout.P_T.Time <= 0.1, 1, 'last'); 
    
    index       = (1:take_off);
    index_PA    = (start_index:take_off);

    else
        start_index = find(simout.P_T.Time <= stride_range(1)/100, 1, 'last');
        end_index   = find(simout.P_T.Time >= stride_range(2)/100, 1, 'last');
    
        index       = (start_index:end_index);
        index_PA    = (start_index:end_index);
end

smc.P_Pos(:,1:3) = max(smc.P(index,:),0);
smc.P_Neg(:,1:3) = min(smc.P(index,:),0);

smc.P_Pos(:,4)   = max(P_joint(index),0);
smc.P_Neg(:,4)   = min(P_joint(index),0);

% Muscle cont. to laoding and push off - Power Integral (Right Riemann Sum)
delta_t               = diff(simout.P_T.Time(index)); 

smc.E_Pos        = sum(delta_t.*smc.P_Pos(2:end,:));
smc.E_Neg        = sum(delta_t.*smc.P_Neg(2:end,:));

smc.E_Sum        = smc.E_Pos + smc.E_Neg;
smc.E_Cont       = (smc.E_Sum./smc.E_Sum(4))*100;

smc.E_Cont_Pos   = (smc.E_Pos./smc.E_Pos(4))*100;
smc.E_Cont_Neg   = (smc.E_Neg./smc.E_Neg(4))*100;
    
% ------------- Single muscle contributions to power, energy, joint torques
smc.P_Cont       = ([smc.P P_joint]./P_joint)*100;
smc.T_Cont       = ([smc.T T_joint]./T_joint)*100;

% ------------------------------- Power Amplification Rate (for Ankle only)
smc.P_max(1:3)   = max(smc.P(index_PA, 1:3));
smc.P_min(1:3)   = min(smc.P(index_PA, 1:3));

smc.P_max(4)     = max(P_joint(index_PA));
smc.P_min(4)     = min(P_joint(index_PA));

    smc.Amplification_Rate = abs(smc.P_max./smc.P_min); 
    smc.P_max_Cont       = (smc.P_max./smc.P_max(4))*100;
    smc.P_min_Cont       = (smc.P_min./smc.P_min(4))*100;
    
% ----------------------------------------------------- Round to two digits
    function out = round_struct(S)
        out = round(S, 2);
    end
    smc = structfun(@round_struct,smc,'UniformOutput',false);
end
