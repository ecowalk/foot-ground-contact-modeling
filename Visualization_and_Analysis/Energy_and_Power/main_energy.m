%% main_energy
% -------------------------------------------------------------------------
% main_energy: data evaluation of output of nms_model2G_EcoWalk.slx
%
% Script supports:
%     1) Data PreProcessing: cropping to one step, normalizing to stride 
%                            time, filter GRF (see "help data_preprocess")
%     2) Gait Measure Table
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Init -------------------------------------------------------------------
close all; clear; clc

% Set Path to Working Directory (local)
stk = dbstack; filepath = which(stk(1).file);
local_path= filepath(1:strfind(filepath,'Visualization_and_Analysis')-1);

% Geyer 2010 Reference Model
fpath_ref  = [local_path 'Visualization_and_Analysis\Figures_Paper\Case_1_og_geyer\Result_1'];
naming_1 = 'Result_1';

% Results for optimized parameters
fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_Opt_1000');
fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_Opt_1000');
fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_Opt_1000');
fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_Opt_1000');
fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_Opt_1000');
fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_Opt_1000');
res_var = 'Result_Opt_1000';

% Results for non optimized parameters
% fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_No_Opt_1000');
% fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_No_Opt_1000');
% fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_No_Opt_1000');
% fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_No_Opt_1000');
% fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_No_Opt_1000');
% fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_No_Opt_1000');
% res_var = 'Result_No_Opt_1000';

% Figure path and name
fpath_fig = [local_path 'Visualization_and_Analysis'];
save_name_fig = 'FGCM';


%% Initialize Variables --------------------------------------------------- 
% Step Number to be evaluted
step_no = -1; %12;

% Data PreProcess
filter.flag   = 0; 
filter.Fs_out = 250;    % Sampling frequency [Hz] for GRFs, PosHAT, F_MTU
filter.Fpass  = 30;     % Fpass = passband frequency of lowpass filter [Hz];

% Data Resampling 
res.fs = 1000;          % Sample frequency
res.p = 3; 
res.q = 1;              % Resampling factors

%% Process Simulation Output ---------------------------------------------- 
%[out1] = data_preprocess(fpath_ref, naming_1, step_no, filter);

[out2] = data_preprocess(fpath_ref1l, res_var, step_no, filter);
[out3] = data_preprocess(fpath_ref1h, res_var, step_no, filter);
[out4] = data_preprocess(fpath_ref2t, res_var, step_no, filter);
[out5] = data_preprocess(fpath_ref2m, res_var, step_no, filter);
[out6] = data_preprocess(fpath_ref3n, res_var, step_no, filter);
[out7] = data_preprocess(fpath_ref3w, res_var, step_no, filter);

%% Tables (Ankle and Global Measures) ------------------------------------- 
trial_names  = {'lA','hA','TJ', 'MTJ','nW','W'};
simout = [out2, out3, out4, out5, out6, out7];

% ------------------------------- Global Gait Measures and Metabolic Energy
[Tab_GM] = tab_gait_measures(trial_names, simout);

disp(Tab_GM);
table2latex(Tab_GM,  fullfile(fpath_fig , 'Tab_GM'));


