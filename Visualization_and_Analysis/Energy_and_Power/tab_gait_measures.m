function [Tab_GM] = tab_gait_measures(trial_names, simout)
arguments 
    trial_names (1,:)   cell 
    simout      (1,:)   Simulink.SimulationOutput
end
% -------------------------------------------------------------------------
% [Tab_GM] = tab_gait_measures(trial_names, simout)
% 
% creates comparison table for global gait measures for all simout trials:
% 
%       1) average forward velocity vel_HAT 
%       2) stride time              t_s
%       3) duty factor              DF
%       4) frounde number           Fr
%       5) Cost of transport        COT
%       6) metabolic energy         E_Metab -> total; for swing; for stance
%
% Inputs:
%   * trial_names:  name for each trial displayed as a header for the
%                   respective column
%   * simout:       Simulink.SimulationOutput
%
% NOTE: CoT and VelHAT are calculated based on entire simetime. The data 
%       are NOT limited to one stride! All other quantities are limited.
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       08-12-2022, Munich
% Last modified:    12-12-2022, Munich 
% 
% -------------------------------------------------------------------------

%% -------------------------------------------------------------- Constants
m_bw                = 80;
g                   = 9.81;

Prop = {'$v_{HAT}$','$t_s$','DF','$F_{r}$','CoT',...
            '$\sum E_{metab}$',...
            '\hspace{0.5cm} \rotatebox[origin=c]{180}{$\Lsh$} $_{Stance}$',...
            '\hspace{0.5cm} \rotatebox[origin=c]{180}{$\Lsh$} $_{Swing}$'}';
        
Unit    = {'$[m/s]$','[s]','[-]','[-]','[$J/kgm$]',...
            '[J]',...
            '[J]',...
            '[J]'}';
       
% ----------------------------------------- Spacer for sideways multicolumn
Spacer(1:length(Prop),:)   = {' '};
Spacer(5,:) = {'\multirow{-5}{*}{\begin{sideways} Gait Measures \end{sideways}}'};
Spacer(8,:) = {'\multirow{-3}{*}{\begin{sideways} Energy        \end{sideways}}'};

% ----------------------------------------- Conversion functions for struct
tab_conv = @(simin) {num2str(round(simin, 3))};

% -------------------------------------------------------------- Init table
Tab_GM  = table(Spacer, Prop, Unit); 

% -------------------------------- Expand simout with human comparison data
%[simout, trial_names, ~] = add_human_out(simout, trial_names);

% -------------------------------------------------------------- Fill table 
    for i = 1:length(simout)
    % ----------------------------------------------------- Evaluate Energy
    E = energy_analysis(simout(i));
    E = structfun(@num2str, E,'UniformOutput',false);

    % ----------------------------------------------------- Gait Parameters
    Trial(1,:) = tab_conv( mean(simout(i).VelHAT.Data(:,1)));                  
    Trial(2,:) = tab_conv(      simout(i).stride_time);                       
    Trial(3,:) = tab_conv(      simout(i).DF);        
    
    if ~any(strcmp(who(simout(i)), 'tag_human')) % -- Only for simulation data
    Trial(4,:) = tab_conv((mean(simout(i).VelHAT.Data(:,1))^2)/g*1);    
    
    else 
        Trial(4,:) = tab_conv((mean(simout(i).VelHAT.Data(:,1))^2)/...
                                                   g*simout(i).leg_length);  
    end
    
    Trial(5,:) = tab_conv(simout(i).MetabEnergy_Umberger/...
                                    (m_bw*simout(i).stride_length));   

    % ---------------------------------------------------- Metabolic Energy
    Trial(6,:) = {     E.Metab_Sum};
    Trial(7,:) = {['$' E.Metab_Stance '^{(' E.Metab_Stance_Cont '\%)}$']}; 
    Trial(8,:) = {['$' E.Metab_Swing  '^{(' E.Metab_Swing_Cont '\%)}$']};

    % ----------------------------------------------------- Assign to Table
    eval(['Tab_GM.' char(trial_names(i)) '= Trial;']);
    end
end

