function [ankle_amp] = calc_ankle_amp(out)
arguments
    out (1,1) Simulink.SimulationOutput
end
%% calc_ankle_amp
% -------------------------------------------------------------------------
% [ankle_amp] = calc_ankle_amp(out) Calculates ankle power amplification as 
% maximum positive ankle power divided by minimum negative ankle power 
% during stance
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% -------------------------------------------------------------------------

% ---------------------------- Find maximum ankle power during stance phase
pos_ind = 1:find(out.P_T.Time <= out.take_off_norm,1,'last');
maximum = max(out.P_T.Data(find(out.P_T.Data(pos_ind,1)>0),1));

% --------------------------------- Find minimum ankle power before maximum
neg_ind   = 1:find(out.P_T.Data(pos_ind,1) == maximum);
minimum   = min(out.P_T.Data(find(out.P_T.Data(neg_ind,1)<0),1));
ankle_amp = [maximum; minimum; abs(maximum/minimum)];
end

