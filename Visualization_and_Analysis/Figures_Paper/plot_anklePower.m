function [fig] = plot_anklePower(simout)
arguments 
    simout (:,1) Simulink.SimulationOutput
end
%% plot_anklePower
% -------------------------------------------------------------------------
% [fig] = plot_anklePower(simout) creates ankle power plots 
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% -------------------------------------------------------------------------

% --------------------------------------------------------- Load human data 
human_data = load_human_data([5 4 3]);

[simout] = simout_rad2deg(simout);
options.norm_to_bw = 1;
norm = norm_to_bw(options);

% ------------------------------------------------------ Define plot colors 
cols  =    [100, 100, 100;...
            64, 83, 211;...
            221, 179, 16;...
            181, 29, 20;...
            0, 190, 255;...
            251, 73, 176;...
            0, 178, 93].*(1/255);    

% ------------------------------------------------------------ Start figure     
fig = figure;
set(fig,'DefaultLineLineWidth',1)
fig.Position = [1 1 15.4 15.4*2/3];
fig.Color    = 'w';

% ---------------------------------------------------- Assign plotting data
for i = 1:length(simout)
    time1 = simout(i).P_T.Time; 
    stance_ind_sim=1:find(simout(i).Dyn.Time<=0.7,1,'last');
    
    plot_sim{i}  = [{time1, simout(i).P_T.Data(:,1)/norm.m_bw};...
                    {simout(i).Dyn.Data(stance_ind_sim,1), simout(i).Dyn.Data(stance_ind_sim,3)}];
end

p = 3;                      % Axes per figure without human data
n = ceil(length(simout)/p); % Number of columns of subfigures
m = size(plot_sim{1},1);    % Number of rows of subfigures
tile = tiledlayout(m,n);    % Format
tile.TileSpacing  = 'compact';
tile.Padding      = 'tight';

% ----------------------------------------------------- Plot Double Support 
for i = 1:n
    nexttile(i);
    plot_double_support_human([-100 100], human_data.DS);
end

% ---------------------------------------------------- Plot simulation data 
for i = 1:length(simout)
    nexttile(ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{1,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{1,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{1,1}==simout(i).take_off_norm);
    
    plot(plot_sim{i}{1,1}, plot_sim{i}{1,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);  
    
    nexttile(n+ceil(i/p))
    hold on
    plot(plot_sim{i}{2,1}, plot_sim{i}{2,2},'-^','MarkerIndices',1);
    plot(plot_sim{i}{2,1}(tf_ind), plot_sim{i}{2,2}(tf_ind),'o');
    
end

% --------------------------------------------------------- Plot human data 
for i = 1:m*n
    nexttile(i)
    hold on
    if i <= n
        h_hum = plot(human_data.AnklePower(:,1)*(1/100), human_data.AnklePower(:,2)*norm.m_bw_de_hum);
        
    elseif i<=2*n
        stance_ind_hum=1:find(human_data.AnkleRoM(:,1)<=70,1,'last');
        h_hum = plot(human_data.AnkleRoM(stance_ind_hum,2), human_data.KneeRoM(stance_ind_hum,2),'-^','MarkerIndices',1);
        mkr_ind = find(human_data.AnkleRoM(:,1)<=human_data.DS(3)*100,1,'last');
        h_hum_mkr = plot(human_data.AnkleRoM(mkr_ind,2), human_data.KneeRoM(mkr_ind,2),'o');
    end
    
    h_hum.Color = cols(1,:);
    h_hum_mkr.Color = cols(1,:);
    
end

% ------------------------------------------------------ Layout adjustments
% -------------------------------------------------------------- Line style 
counter = 2;
for i = 1:n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    xlines = ax.findobj('Type','constantline');
    if i == n+1 || i == 2*n+1
        counter = 2;
    end
    for j = length(lines):-1:2
        lines(j).Color = cols(counter,:);
        xlines(3*(j-1)).Color = cols(counter,:);
        xlines(3*(j-1)-1).Color = cols(counter,:);
        xlines(3*(j-1)-2).Color = cols(counter,:);
        counter = counter + 1;
    end
end

% ------------------------------------------------------------- Line colors 
counter = 2;
for i = n+1:m*n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    if i == 2*n+1 || i == 3*n+1
        counter = 2;
    end
    for j = length(lines):-2:3
        lines(j).Color = cols(counter,:);
        lines(j-1).Color = cols(counter,:);
        counter = counter + 1;
    end
end

% --------------------- Plot "invisible" colors for correct legend later on
for i = 1:length(simout)
    ax = nexttile(2);
    plots_lgd(i) = plot(ax,NaN,NaN, '-', 'Color', cols(i+1,:));
end

% --------------------------------------------------------- Set axis limits
for i = 1:m*n
    ax = nexttile(i);
    ax.Box = 'on';
    ax.Layer = 'top';  
    ax.TickLength = [0.025 0.01];
    
    if i<=n
        ax.YLim = [-1 3.7]*norm.m_bw_de;
        ax.XTick = 0:0.2:1;
        ax.XTickLabel = num2str(str2double(ax.XTickLabel)*100);
        ax.XLim =[0 0.7];
        ax.XLabel.String='\% gait cycle';
    elseif i<=2*n
        ax.YLim = [ -5 85];
        ax.XLim =[-42 20];
        ax.XLabel.String='$\varphi_{\textrm{ank}}$ (deg)';
    end
        
    if mod(i,n) ~= 1
        ax.YTickLabel={};
    end
end

% ---------------------------------------------- Set labels and annotations
tile.XLabel.Interpreter = 'latex';
tile.XLabel.FontSize = 10;
ax = nexttile(1);
ax.YLabel.String = '$P_{\textrm{ank}}$ (W/kg)';
ax = nexttile(n+1);
ax.YLabel.String = '$\varphi_{\textrm{kne}}$ (deg)';

% -------------------------------------------------------------- Set legend
lgd_label={'1s-lA' '1s-hA' '2s-TJ' '2s-MTJ' '3s-nW' '3s-W' };
lgd = legend(plots_lgd,lgd_label);
lgd.Layout.Tile = 'North';
lgd.Orientation = 'vertical';
lgd.NumColumns = 6;
lgd.ItemTokenSize(1) = 15;
% ----- Fix for graphic error, box was invisible with standard value of 0.5
lgd.LineWidth = 0.7; 