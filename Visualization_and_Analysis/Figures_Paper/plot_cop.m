function [fig] = plot_cop(simout)
arguments 
    simout              (:,1) Simulink.SimulationOutput
end
%% plot_cop
% -------------------------------------------------------------------------
% [fig] = plot_cop(simout) creates center of pressure (CoP) plots 
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% -------------------------------------------------------------------------

% --------------------------------------------------------- Load human data 
human = load_human_data(5);

[simout]     = simout_rad2deg(simout);
options.norm_to_bw = 1;
norm = norm_to_bw(options);

% ------------------------------------------------------ Define plot colors 
cols  =    [64, 83, 211;...
            221, 179, 16;...
            181, 29, 20;...
            0, 190, 255;...
            251, 73, 176;...
            0, 178, 93].*(1/255); 
        
% ------------------------------------------------------------ Start figure             
fig = figure;
fig.Position = [1 1 15.4 5];
fig.Color    = 'w';
set(fig,'DefaultLineLineWidth',1)

% ---------------------------------------------------- Assign plotting data
for i = 1:length(simout)
    plot_sim{i}  = [{simout(i).COP.Time, simout(i).COP.Data(:,2)}
                    ];
end

p = 3;                      % Axes per figure without human data
n = ceil(length(simout)/p); % Number of columns of subfigures
m = size(plot_sim{1},1);    % Number of rows of subfigures
tile = tiledlayout(m,n);    % Format
tile.TileSpacing = 'compact';
tile.Padding     = 'tight';

% ----------------------------------------------------- Plot Double Support 
for i = 1:m*n
    nexttile(i);
    plot_double_support_human([-100 100], human.DS);
end

% ---------------------------------------------------- Plot simulation data 
% lines = cell(length(simout),1);
for i = 1:length(simout)
    nexttile(ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{1,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{1,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{1,1}==simout(i).take_off_norm);
    
    plot(plot_sim{i}{1,1}, plot_sim{i}{1,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);
    
end

% ------------------------------------------------------ Layout adjustments
% -------------------------------------------------------------- Line style 
counter = 1;
for i = 1:m*n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    xlines = ax.findobj('Type','constantline');
    if i == n+1 || i == 2*n+1
        counter = 1;
    end
    for j = length(lines):-1:1
        lines(j).Color = cols(counter,:);
        xlines(3*(j)).Color = cols(counter,:);
        xlines(3*(j)-1).Color = cols(counter,:);
        xlines(3*(j)-2).Color = cols(counter,:);
        counter = counter + 1;
    end
end

% --------------------- Plot "invisible" colors for correct legend later on
for i = 1:length(simout)
    ax = nexttile(1);
    plots_lgd(i) = plot(ax,NaN,NaN, '-', 'Color', cols(i,:));
end
 
% --------------------------------------------------------- Set axis limits
for i = 1:m*n
    ax = nexttile(i);
    ax.Box = 'on';
    ax.Layer = 'top';
    ax.XTick = 0:0.2:1;
    ax.XTickLabel = num2str(str2double(ax.XTickLabel)*100);
    ax.XLim =[0 0.7];
    ax.TickLength = [0.025 0.01];
    ax.YLim = [0 0.28];
    yl1 = yline(0.17 ,'--');
    yl2 = yline(0.225,'--');
    
    if i==1
        yl1.Interpreter='latex';
        yl2.Interpreter='latex';
        yl1.Label= '$C_\textrm{B}$';
        yl2.Label= '$C_\textrm{T}$';
        yl1.LabelHorizontalAlignment = 'left';
        yl2.LabelHorizontalAlignment = 'left';
        yl1.LabelVerticalAlignment = 'middle';
        yl2.LabelVerticalAlignment = 'middle';
    end
end

% ---------------------------------------------- Set labels and annotations
tile.XLabel.Interpreter = 'latex';
tile.XLabel.FontSize = 10;
tile.XLabel.String = '\% gait cycle';
ax = nexttile(1);
ax.YLabel.String = '$COP$ (m)';

% -------------------------------------------------------------- Set legend
lgd_label={'1s-lA' '1s-hA' '2s-TJ' '2s-MTJ' '3s-nW' '3s-W' };
lgd = legend(plots_lgd,lgd_label);
lgd.Layout.Tile = 'North';
lgd.Orientation = 'vertical';
lgd.NumColumns = 6;
lgd.ItemTokenSize(1) = 15;
% ----- Fix for graphic error, box was invisible with standard value of 0.5
lgd.LineWidth = 0.7;