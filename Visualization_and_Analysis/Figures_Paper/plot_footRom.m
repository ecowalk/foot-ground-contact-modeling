function [fig] = plot_footRom(simout)
arguments 
    simout (:,1) Simulink.SimulationOutput
end
%% plot_footRom
% -------------------------------------------------------------------------
% [fig] = plot_footRom(simout) creates the foot kinematics plots 
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% ------------------------------------------------------------------------- 
%% References (human data of internal foot movement was manually sampled):
%{
%[1] Caravaggi, P., Pataky, T., Günther, M., Savage, R., and Crompton, R. “Dynamics of
%   longitudinal arch support in relation to walking speed: contribution of the plantar
%   aponeurosis”. In: Journal of Anatomy 217.3 (Aug. 2010), pp. 254–261. DOI: 10.1111/
%   j.1469-7580.2010.01261.x.

%[2] Bruening DA, Pohl MB, Takahashi KZ, Barrios JA. Midtarsal locking, the windlass mechanism,
%   and running strike pattern: A kinematic and kinetic assessment. J Biomech. 2018 May 17;73:185-191.
%   doi: 10.1016/j.jbiomech.2018.04.010.

%[3] Nester, C. J., Jarvis, H. L., Jones, R. K., Bowden, P. D., and Liu, A. “Movement of the
%   human foot in 100 pain free individuals aged 18–45: implications for understanding
%   normal foot function”. In: Journal of Foot and Ankle Research 7.1 (Nov. 2014). DOI:
%   10.1186/s13047-014-0051-8.
%}

% --------------------------------------------------------- Load human data 
human_temp = load_human_data(5);

[simout]     = simout_rad2deg(simout);
options.norm_to_bw = 1;
norm = norm_to_bw(options);

% ------------------------------------------------------ Define plot colors 
cols  =    [100, 100, 100;...
            181, 29, 20;...
            0, 190, 255;...
            251, 73, 176;...
            0, 178, 93].*(1/255);
        
% ------------------------------------------------------------ Start figure                     
fig = figure;
fig.Position     = [1 1 15.4/2 8];
fig.Color        = 'w';
set(fig,'DefaultLineLineWidth',1)

% ---------------------------------------------------- Assign plotting data
DS = human_temp.DS;
file_mtj={'caravaggi_MTJ_norm', 'bruening_MTJ_norm', 'nester_MTJ_norm'};
file_tj={'caravaggi_TJ_norm', 'bruening_TJ_norm', 'nester_TJ_norm'};

for i =1:3
    % MTJ
    data = load(file_mtj{i});
    data = struct2array(data);
    human_data.mtj{i} = data;
    
    % TJ
    data = load(file_tj{i});
    data = struct2array(data);
    human_data.tj{i} = data;
end

for i = 1:length(simout)
    plot_sim{i}  = [{simout(i).Dyn.Time, simout(i).Foot_Kinematics_L.MTJ_phi.Data};...
                    {simout(i).Dyn.Time, simout(i).Foot_Kinematics_L.MTPJ_phi.Data}
                    ];
end

p = 4;                      % Axes per figure without human data
n = ceil(length(simout)/p); % Number of columns of subfigures
m = size(plot_sim{1},1);    % Number of rows of subfigures
tile = tiledlayout(m,n);    % Format
tile.TileSpacing  = 'compact';
tile.Padding      = 'tight';

% ----------------------------------------------------- Plot Double Support 
for i = 1:m*n
    nexttile(i);
    plot_double_support_human([-100 100], DS);
end

% ---------------------------------------------------- Plot simulation data 
% lines = cell(length(simout),1);
for i = 1:length(simout)
    nexttile(ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{1,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{1,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{1,1}==simout(i).take_off_norm);
    if any(plot_sim{i}{1,2})
        plot(plot_sim{i}{1,1}, plot_sim{i}{1,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
        xline(simout(i).heel_off_norm);
        xline(simout(i).heel_opp_norm);
        xline(simout(i).take_off_norm);
    else
        plot(NaN,NaN);
        xline(0);
        xline(0);
        xline(0);
    end
    
    nexttile(n+ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{2,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{2,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{2,1}==simout(i).take_off_norm);
    if any(plot_sim{i}{2,2})
        plot(plot_sim{i}{2,1}, plot_sim{i}{2,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
        xline(simout(i).heel_off_norm);
        xline(simout(i).heel_opp_norm);
        xline(simout(i).take_off_norm);
    else
        plot(NaN,NaN);
        xline(0);
        xline(0);
        xline(0);
    end
end

% --------------------------------------------------------- Plot human data 
author = 3; % 1:caravaggi; 2:bruening; 3:nester
for i = 1:m*n
    nexttile(i)
    hold on
    if i <= n
        h_hum = plot(human_data.mtj{author}(:,1)*(1/100), human_data.mtj{author}(:,2));
    elseif i <= 2*n
        h_hum = plot(human_data.tj{author}(:,1)*(1/100), human_data.tj{author}(:,2));
    else
        h_hum = plot(NaN, NaN);
    end
    h_hum.Color = cols(1,:);
end

% ------------------------------------------------------ Layout adjustments
% ---------------------------------------------------- Line style and color
counter = 2;
for i = 1:m*n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    xlines = ax.findobj('Type','constantline');
    if i == n+1 || i == 2*n+1
        counter = 2;
    end
    for j = length(lines):-1:2
        lines(j).Color = cols(counter,:);
        xlines(3*(j-1)).Color = cols(counter,:);
        xlines(3*(j-1)-1).Color = cols(counter,:);
        xlines(3*(j-1)-2).Color = cols(counter,:);
        counter = counter + 1;
    end
end

% --------------------- Plot "invisible" colors for correct legend later on
h = findobj(gcf,'Type','constantline','Value',0);
delete(h);
for i = 1:length(simout)
    ax = nexttile(1);
    plots_lgd(i) = plot(ax,NaN,NaN, '-', 'Color', cols(i+1,:));
end
 
% --------------------------------------------------------- Set axis limits
for i = 1:m*n
    ax = nexttile(i);
    ax.Box = 'on';
    ax.Layer = 'top';
    ax.XTick = 0:0.2:1;
    ax.XTickLabel = num2str(str2double(ax.XTickLabel)*100);
    ax.XLim =[0 1];
    ax.TickLength = [0.025 0.01];
    if i <= n
        ax.YLim = [-15 5];
    elseif i <= 2*n
       ax.YLim = [-5 75];
    end
end

% ---------------------------------------------- Set labels and annotations
tile.XLabel.Interpreter = 'latex';
tile.XLabel.FontSize = 10;
tile.XLabel.String = '\% gait cycle';

ax = nexttile(1);
ax.YLabel.String = '$\varphi_{\mathrm{MTJ}}$ (deg)';
y_txt_up = ax.YLim(2)-0.1*(ax.YLim(2)-ax.YLim(1));
y_txt_down = ax.YLim(1)+0.07*(ax.YLim(2)-ax.YLim(1));
x_txt = ax.XLim(1)+0.03*(ax.XLim(2)-ax.XLim(1));
text(x_txt,y_txt_up,'DF','FontSize',9)
text(x_txt,y_txt_down,'PF','FontSize',9)

ax = nexttile(n+1);
ax.YLabel.String = '$\varphi_{\mathrm{TJ}}$ (deg)';
y_txt_up = ax.YLim(2)-0.1*(ax.YLim(2)-ax.YLim(1));
y_txt_down = ax.YLim(1)+0.07*(ax.YLim(2)-ax.YLim(1));
x_txt = ax.XLim(1)+0.03*(ax.XLim(2)-ax.XLim(1));
text(x_txt,y_txt_up,'DF','FontSize',9)
text(x_txt,y_txt_down,'PF','FontSize',9)

% -------------------------------------------------------------- Set legend
lgd_label={  '2s-TJ' '2s-MTJ'  '3s-nW' '3s-W' };
lgd = legend(plots_lgd,lgd_label);
lgd.Layout.Tile = 'North';
lgd.Orientation = 'vertical';
lgd.NumColumns = 2;
lgd.ItemTokenSize(1)=15;
% ----- Fix for graphic error, box was invisible with standard value of 0.5
lgd.LineWidth = 0.7;