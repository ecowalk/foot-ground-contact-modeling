function [fig] = plot_gas_sol(simout)
arguments 
    simout (:,1) Simulink.SimulationOutput
end
%% plot_gas_sol
% -------------------------------------------------------------------------
% [fig] = plot_gas_sol(simout) generates plots for GAS and SOL behavior
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% -------------------------------------------------------------------------

% --------------------------------------------------------- Load human data 
human_data = load_human_data([5 4 3]);

[simout]     = simout_rad2deg(simout);
options.norm_to_bw = 1;
norm = norm_to_bw(options);

% ------------------------------------------------------ Define plot colors 
cols  =    [100, 100, 100;...
            64, 83, 211;...
            221, 179, 16;...
            181, 29, 20;...
            0, 190, 255;...
            251, 73, 176;...
            0, 178, 93].*(1/255); 

% ------------------------------------------------------------ Start figure             
fig = figure;
set(fig,'DefaultLineLineWidth',1)
fig.Position = [1 1 15.4 15.4];
fig.Color    = 'w';

% ---------------------------------------------------- Assign plotting data
for i = 1:length(simout) 
    time = simout(i).ACT.Time;    
    
    plot_sim{i} = [{time, simout(i).ACT.Data(:,5)*100};...
                    {time, simout(i).ACT.Data(:,6)*100};...
                    {simout(i).L_MTU.Data(:,5), simout(i).F_MTU.Data(:,5)};...
                    {simout(i).L_MTU.Data(:,6), simout(i).F_MTU.Data(:,6)}];
end


p = 3;                      % Axes per figure without human data
n = ceil(length(simout)/p); % Number of columns of subfigures
m = size(plot_sim{1},1);    % Number of rows of subfigures
tile = tiledlayout(m,n);    % Format
tile.TileSpacing = 'compact';
tile.Padding     = 'tight';

% ----------------------------------------------------- Plot Double Support 
for i = 1:2*n
    nexttile(i);
    plot_double_support_human([-100 100], human_data.DS);
end

% ---------------------------------------------------- Plot simulation data 
for i = 1:length(simout)
    nexttile(ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{1,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{1,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{1,1}==simout(i).take_off_norm);
    plot(plot_sim{i}{1,1}, plot_sim{i}{1,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);
    
    nexttile(n+ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{2,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{2,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{2,1}==simout(i).take_off_norm);
    plot(plot_sim{i}{2,1}, plot_sim{i}{2,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);
    
    nexttile(2*n+ceil(i/p))
    hold on
    plot(plot_sim{i}{3,1}, plot_sim{i}{3,2});

    nexttile(3*n+ceil(i/p))
    hold on
    plot(plot_sim{i}{4,1}, plot_sim{i}{4,2});
end

% --------------------------------------------------------- Plot human data 
for i = 1:m*n
    nexttile(i)
    hold on
    if i <= n
        h_hum = plot(human_data.ACTGAS(:,1)*(1/100), human_data.ACTGAS(:,2));
    elseif i<=2*n
        h_hum = plot(human_data.ACTSOL(:,1)*(1/100), human_data.ACTSOL(:,2));
    end
    h_hum.Color = cols(1,:);
end


% ------------------------------------------------------ Layout adjustments
% ---------------------------------------------------- Line style and color
counter = 2;
for i = 1:2*n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    xlines = ax.findobj('Type','constantline');
    if i == n+1 || i == 2*n+1
        counter = 2;
    end
    for j = length(lines):-1:2
        lines(j).Color = cols(counter,:);
        xlines(3*(j-1)).Color = cols(counter,:);
        xlines(3*(j-1)-1).Color = cols(counter,:);
        xlines(3*(j-1)-2).Color = cols(counter,:);
        counter = counter + 1;
    end
end

counter = 2;
for i = 2*n+1:m*n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    if i == 2*n+1 || i == 3*n+1
        counter = 2;
    end
    for j = length(lines):-1:1
        lines(j).Color = cols(counter,:);
        counter = counter + 1;
    end
    
end

% --------------------- Plot "invisible" colors for correct legend later on
for i = 1:length(simout)
    ax = nexttile(2);
    plots_lgd(i) = plot(ax,NaN,NaN, '-', 'Color', cols(i+1,:));
end

% --------------------------------------------------------- Set axis limits
for i = 1:m*n
    ax = nexttile(i);
    ax.Box = 'on';
    ax.Layer = 'top';

    ax.TickLength=[0.025 0.01];
    if i <= n
        ax.YLim = [ 0 100];
        ax.XTick = 0:0.2:1;
        ax.XTickLabel = num2str(str2double(ax.XTickLabel)*100);
        ax.XLim =[0 1];
    elseif i<=2*n
        ax.YLim = [ 0 100];
        ax.XTick = 0:0.2:1;
        ax.XTickLabel = num2str(str2double(ax.XTickLabel)*100);
        ax.XLim =[0 1];
        ax.XLabel.String='\% gait cycle';
    elseif i<=3*n
        ax.YLim = [0 1000];
        ax.XLim =[0.4 0.46];
        
    else
        ax.YLim = [0 2000];
        ax.XLim =[0.28 0.305];
        ax.XLabel.String='$l_{\textrm{MTU}}$ (m)';
    end
    if mod(i,n) ~= 1
        ax.YTickLabel={};
    end
end

% ---------------------------------------------- Set labels and annotations
tile.XLabel.Interpreter = 'latex';
tile.XLabel.FontSize = 10;
ax = nexttile(1);
ax.YLabel.String = '$ACT_{\textrm{GAS}}$';
ax = nexttile(n+1);
ax.YLabel.String = '$ACT_{\textrm{SOL}}$';
ax = nexttile(2*n+1);
ax.YLabel.String = '$F_{\textrm{GAS}}$ (N)';
ax = nexttile(3*n+1);
ax.YLabel.String = '$F_{\textrm{SOL}}$ (N)';

% -------------------------------------------------------------- Set legend
lgd_label={'1s-lA' '1s-hA' '2s-TJ' '2s-MTJ' '3s-nW' '3s-W' };
lgd = legend(plots_lgd,lgd_label);
lgd.Layout.Tile = 'North';
lgd.Orientation = 'vertical';
lgd.NumColumns = 6;
lgd.ItemTokenSize(1) = 15;
% ----- Fix for graphic error, box was invisible with standard value of 0.5
lgd.LineWidth = 0.7; 