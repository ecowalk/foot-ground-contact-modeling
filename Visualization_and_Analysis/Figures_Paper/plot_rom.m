function [fig] = plot_rom(simout)
arguments 
    simout              (:,1) Simulink.SimulationOutput
end
%% plot_rom
% -------------------------------------------------------------------------
% [fig] = plot_rom(simout) generates kinematics plots for ankle, knee and
% hip
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% -------------------------------------------------------------------------

% --------------------------------------------------------- Load human data 
human_data = load_human_data(5);

[simout]     = simout_rad2deg(simout);
options.norm_to_bw = 1;
norm = norm_to_bw(options);

% ------------------------------------------------------ Define plot colors 
cols  =    [100, 100, 100;...
            64, 83, 211;...
            221, 179, 16;...
            181, 29, 20;...
            0, 190, 255;...
            251, 73, 176;...
            0, 178, 93].*(1/255);

% ------------------------------------------------------------ Start figure                     
fig = figure;
set(fig,'DefaultLineLineWidth',1)
fig.Position     = [1 1 15.4 15.4*2/3];
fig.Color        = 'w';

% ---------------------------------------------------- Assign plotting data    
for i = 1:length(simout)
    plot_sim{i}  = [{simout(i).Dyn.Time, simout(i).Dyn.Data(:,1)};...
                    {simout(i).Dyn.Time, simout(i).Dyn.Data(:,3)};...
                    {simout(i).Dyn.Time, simout(i).Dyn.Data(:,5)}];
end

p = 3;                      % Axes per figure without human data
n = ceil(length(simout)/p); % Number of columns of subfigures
m = size(plot_sim{1},1);    % Number of rows of subfigures
tile = tiledlayout(m,n);    % Format
tile.TileSpacing  = 'compact';
tile.Padding      = 'tight';

% ----------------------------------------------------- Plot Double Support 
for i = 1:m*n
    nexttile(i);
    plot_double_support_human([-100 100], human_data.DS);
end

% ---------------------------------------------------- Plot simulation data 
for i = 1:length(simout)
    nexttile(ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{1,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{1,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{1,1}==simout(i).take_off_norm);
    plot(plot_sim{i}{1,1}, plot_sim{i}{1,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);
    
    nexttile(n+ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{2,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{2,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{2,1}==simout(i).take_off_norm);
    plot(plot_sim{i}{2,1}, plot_sim{i}{2,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);
    
    nexttile(2*n+ceil(i/p))
    hold on
    hf_ind = find(plot_sim{i}{3,1}==simout(i).heel_off_norm);
    ho_ind = find(plot_sim{i}{3,1}==simout(i).heel_opp_norm);
    tf_ind = find(plot_sim{i}{3,1}==simout(i).take_off_norm);
    plot(plot_sim{i}{3,1}, plot_sim{i}{3,2},'-*','MarkerIndices',[hf_ind ho_ind tf_ind]);
    xline(simout(i).heel_off_norm);
    xline(simout(i).heel_opp_norm);
    xline(simout(i).take_off_norm);
end

% --------------------------------------------------------- Plot human data 
for i = 1:m*n
    nexttile(i)
    hold on
    if i <= n
        h_hum = plot(human_data.AnkleRoM(:,1)*(1/100), human_data.AnkleRoM(:,2));
    elseif i<=2*n
        h_hum = plot(human_data.KneeRoM(:,1)*(1/100), human_data.KneeRoM(:,2));
    else
        h_hum = plot(human_data.HipRoM(:,1)*(1/100), human_data.HipRoM(:,2));
    end
    h_hum.Color = cols(1,:);
    
end

% ------------------------------------------------------ Layout adjustments
% --------------------------------------------------- Line style and colors
counter = 2;
for i = 1:m*n
    ax = nexttile(i);
    lines = ax.findobj('Type','line');
    xlines = ax.findobj('Type','constantline');
    if i == n+1 || i == 2*n+1
        counter = 2;
    end
    for j = length(lines):-1:2
        lines(j).Color = cols(counter,:);
        xlines(3*(j-1)).Color = cols(counter,:);
        xlines(3*(j-1)-1).Color = cols(counter,:);
        xlines(3*(j-1)-2).Color = cols(counter,:);
        counter = counter + 1;
    end
end

% --------------------- Plot "invisible" colors for correct legend later on
for i = 1:length(simout)
    ax = nexttile(1);
    plots_lgd(i) = plot(ax,NaN,NaN, '-', 'Color', cols(i+1,:));
end

% --------------------------------------------------------- Set axis limits
for i = 1:m*n
    ax = nexttile(i);
    ax.Box = 'on';
    ax.Layer = 'top';
    ax.XTick = 0:0.2:1;
    ax.XTickLabel = num2str(str2double(ax.XTickLabel)*100);
    ax.XLim =[0 1];
    ax.TickLength = [0.025 0.01];
    if i <= n
        ax.YLim = [-42 20];
        ax.XTickLabel = {};
    elseif i<=2*n
        ax.YLim = [ -5 85];
        ax.XTickLabel = {};
    else
        ax.YLim = [-20 50];
    end
    if mod(i,n) ~= 1
        ax.YTickLabel = {};
    end
end

% ---------------------------------------------- Set labels and annotations
tile.XLabel.Interpreter = 'latex';
tile.XLabel.FontSize = 10;
tile.XLabel.String = '\% gait cycle';

ax = nexttile(1);
ax.YLabel.String = '$\varphi_{\textrm{ank}}$ (deg)';
y_txt_up = ax.YLim(2)-0.1*(ax.YLim(2)-ax.YLim(1));
y_txt_down = ax.YLim(1)+0.07*(ax.YLim(2)-ax.YLim(1));
x_txt = ax.XLim(1)+0.03*(ax.XLim(2)-ax.XLim(1));
text(x_txt,y_txt_up,'DF','FontSize',9)
text(x_txt,y_txt_down,'PF','FontSize',9)

ax = nexttile(n+1);
ax.YLabel.String = '$\varphi_{\textrm{kne}}$ (deg)';
y_txt_up = ax.YLim(2)-0.1*(ax.YLim(2)-ax.YLim(1));
y_txt_down = ax.YLim(1)+0.07*(ax.YLim(2)-ax.YLim(1));
x_txt = ax.XLim(1)+0.03*(ax.XLim(2)-ax.XLim(1));
text(x_txt,y_txt_up,'Flex.','FontSize',9)
text(x_txt,y_txt_down,'Ext.','FontSize',9)

ax = nexttile(2*n+1);
ax.YLabel.String = '$\varphi_{\textrm{hip}}$ (deg)';
y_txt_up = ax.YLim(2)-0.1*(ax.YLim(2)-ax.YLim(1));
y_txt_down = ax.YLim(1)+0.07*(ax.YLim(2)-ax.YLim(1));
x_txt = ax.XLim(1)+0.03*(ax.XLim(2)-ax.XLim(1));
text(x_txt,y_txt_up,'Flex.','FontSize',9)
text(x_txt,y_txt_down,'Ext.','FontSize',9)

% -------------------------------------------------------------- Set legend
lgd_label={'1s-lA' '1s-hA' '2s-TJ' '2s-MTJ' '3s-nW' '3s-W' };
lgd = legend(plots_lgd,lgd_label);
lgd.Layout.Tile = 'North';
lgd.Orientation = 'vertical';
lgd.NumColumns = 6;
lgd.ItemTokenSize(1) = 15;
% ----- Fix for graphic error, box was invisible with standard value of 0.5
lgd.LineWidth = 0.7; 