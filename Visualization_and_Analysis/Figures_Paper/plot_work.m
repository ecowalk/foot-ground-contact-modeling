function [fig] = plot_work(simout)
arguments 
    simout (:,1) Simulink.SimulationOutput
end
%% plot_work
% -------------------------------------------------------------------------
% [fig] = plot_work(simout) generates FGCM work bar plots
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      11-10-2023, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann 
% 
% -------------------------------------------------------------------------

% ------------------------------------------------------------ Start figure             
fig = figure;
fig.Position = [1 1 15.4 7];
fig.Color    = 'w';

options.norm_to_bw = 1;
norm = norm_to_bw(options);

% ---------------------------------------------------- Plot simulation data 
for i = 1:length(simout)
    simout(i).Foot_arch_L.MTPJ_energy = simout(i).Foot_arch_L.MTPJ_energy - simout(i).Foot_arch_L.MTPJ_energy.Data(1);
    simout(i).Foot_arch_L.PA_energy = simout(i).Foot_arch_L.PA_energy - simout(i).Foot_arch_L.PA_energy.Data(1);
    simout(i).Foot_arch_L.PL_energy = simout(i).Foot_arch_L.PL_energy - simout(i).Foot_arch_L.PL_energy.Data(1);
    simout(i).Foot_CE_L.CE_heel_energy = simout(i).Foot_CE_L.CE_heel_energy - simout(i).Foot_CE_L.CE_heel_energy.Data(1);
    simout(i).Foot_CE_L.CE_ball_energy = simout(i).Foot_CE_L.CE_ball_energy - simout(i).Foot_CE_L.CE_ball_energy.Data(1);
    simout(i).Foot_CE_L.CE_toe_energy = simout(i).Foot_CE_L.CE_toe_energy - simout(i).Foot_CE_L.CE_toe_energy.Data(1);
    
    plot_sim{i}  = [{ sum(simout(i).Foot_arch_L.MTPJ_energy.Data(end)+...
                          simout(i).Foot_arch_L.PA_energy.Data(end)+...
                          simout(i).Foot_arch_L.PL_energy.Data(end)+...
                          simout(i).Foot_CE_L.CE_heel_energy.Data(end)+...
                          simout(i).Foot_CE_L.CE_ball_energy.Data(end)+...
                          simout(i).Foot_CE_L.CE_toe_energy.Data(end))./norm.m_bw,...
                     sum(simout(i).Foot_CE_L.CE_heel_energy.Data(end))./norm.m_bw,...
                     sum(simout(i).Foot_CE_L.CE_ball_energy.Data(end))./norm.m_bw,...
                     sum(simout(i).Foot_CE_L.CE_toe_energy.Data(end))./norm.m_bw,...
                     sum(simout(i).Foot_arch_L.MTPJ_energy.Data(end)+...
                         simout(i).Foot_arch_L.PA_energy.Data(end)+...
                         simout(i).Foot_arch_L.PL_energy.Data(end))./norm.m_bw}];
end

% ---------------------------------------------- Set labels and annotations
hold on
X = categorical({'Total','CE Heel','CE Ball','CE Toe', 'Internal'});
X = reordercats(X,{'Total','CE Heel','CE Ball','CE Toe', 'Internal'});
Y = zeros(length(X),length(plot_sim));

hold on
for i = 1:length(plot_sim)
    for j = 1:length(X)
        Y(j,i) = plot_sim{i}{j};
    end
end

% --------------------------------------------------------- Set axis limits
    l_sim = bar(X,Y);
    ax = gca;
    ax.Box = 'on';
    ax.Layer = 'top';
    ax.YLim = [-0.2 0.0]*norm.m_bw_de;

    ax.XAxisLocation = 'top';
    ax.XLabel.Interpreter = 'Latex';
    ax.XAxis.TickLength = [0 0];
    ax.YLabel.String = '$W_{\textrm{FGCM}}$ (J/kg)';
    
    % ---------------------------------------------------------- Set legend
    lgd_label={'1s-lA' '1s-hA' '2s-TJ' '2s-MTJ' '3s-nW' '3s-W' };
    lgd = legend(lgd_label);
    set(lgd, 'FontName', 'math','Interpreter','latex')
    lgd.Location = 'southeast';
    lgd.ItemTokenSize(1)=15;
    lgd.Orientation = 'vertical';
    lgd.NumColumns = 2;
end