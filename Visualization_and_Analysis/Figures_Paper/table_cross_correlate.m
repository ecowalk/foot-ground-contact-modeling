%% table_cross_correlate.m
% -------------------------------------------------------------------------
% table_cross_correlate.m - Creates the cross-correlation table in the 
% paper appendix
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann
% 
% -------------------------------------------------------------------------

% ------------------------------------------ Run cross correlation analysis
main_cross_correlate
R = [R_ankle;R_knee;R_hip];

% ------------------------------------------ Round all values to two digits 
R_str = cell(6,1);
for i = 1:6
    R_str{i} = sprintf('%.2f & ' , R(i,:));
    R_str{i} = R_str{i}(1:end-2);
end

% ----------------------------------------------- Write results to tex file
text_table={
'\begin{table}[h]';
'\centering';
'\caption{Cross correlation to human data from \cite{vanderZee.2022}.}';
'\begin{tabular*}{\columnwidth}{@{\extracolsep{\fill}\quad} p{1.3cm} l l l l l l }';
'Arch & \multicolumn{3}{c }{\cellcolor{gray!50}rigid} & \multicolumn{3}{c}{mobile} \\';
'\hline';
'Toe & \multicolumn{2}{c}{none} & \cellcolor{gray!50}decpl. & none & \cellcolor{gray!50}decpl. & cpl. \\';
'\multicolumn{7}{c}{} \\[-5pt] % small skip';
%
'& \textbf{1s-lA} & \textbf{1s-hA} & \textbf{2s-\gls{TJ}} & \textbf{2s-\gls{MTJ}} & \textbf{3s-\gls{nW}} & \textbf{3s-\gls{W}}\\';
'\hline';
['$R_\text{ank, stance}$  & ' R_str{1} '\\'];
['$R_\text{ank, swing}$   & ' R_str{2} '\\'];
'\hline';
['$R_\text{kne, stance}$  & ' R_str{3} '\\'];
['$R_\text{kne, swing}$   & ' R_str{4} '\\'];
'\hline';
['$R_\text{hip, stance}$  & ' R_str{5} '\\'];
['$R_\text{hip, swing}$   & ' R_str{6} '\\'];
'\hline';
'\end{tabular*}';
'\label{tab:cross_corr}';
'\end{table}';
};

% -------------------------------- Table path and name for saving .tex file
fpath_tex = fullfile(local_path, 'Visualization_and_Analysis');
file = fopen([fpath_tex '\cross_corr_power_amp.tex'],'w');
fprintf(file,'%s\n',text_table{:});
fclose(file);