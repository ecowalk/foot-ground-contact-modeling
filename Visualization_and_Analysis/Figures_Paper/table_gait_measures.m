%% table_gait_measures.m
% -------------------------------------------------------------------------
% table_gait_measures.m - Creates gait measure table in the paper
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann
% 
% -------------------------------------------------------------------------

close all; clear; clc

stk = dbstack; filepath = which(stk(1).file);
local_path = filepath(1:strfind(filepath,'Visualization_and_Analysis')-1);

addpath ([local_path 'Visualization_and_Analysis']); 
addpath ([local_path 'Visualization_and_Analysis\Figures_Paper']); 
addpath ([local_path 'Visualization_and_Analysis\Human_Data']); 
addpath ([local_path 'Visualization_and_Analysis\Standard_Figures']); 

%% Set Path to Working Directory (local)-----------------------------------
% results for optimized parameters
fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_Opt_1000');
fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_Opt_1000');
fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_Opt_1000');
fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_Opt_1000');
fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_Opt_1000');
fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_Opt_1000');
res_var = 'Result_Opt_1000';

% results for non optimized parameters
% fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_No_Opt_1000');
% fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_No_Opt_1000');
% fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_No_Opt_1000');
% fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_No_Opt_1000');
% fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_No_Opt_1000');
% fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_No_Opt_1000');
% res_var = 'Result_No_Opt_1000';



%% Initialize Variables --------------------------------------------------- 
% Step Number to be evaluted (-1 for last full step)
step_no = -1;

% Data PreProcess
filter.flag   = 1; 
filter.Fpass  = 50;       % Fpass = passband frequency of lowpass filter [Hz];

%% Process Simulation Output ---------------------------------------------- 
[out1] = data_preprocess(fpath_ref1l, res_var, step_no, filter);
[out2] = data_preprocess(fpath_ref1h, res_var, step_no, filter);
[out4] = data_preprocess(fpath_ref2m, res_var, step_no, filter);
[out3] = data_preprocess(fpath_ref2t, res_var, step_no, filter);
[out5] = data_preprocess(fpath_ref3n, res_var, step_no, filter);
[out6] = data_preprocess(fpath_ref3w, res_var, step_no, filter);

simout = [out1 out2 out3 out4 out5 out6];

% ------------------------------------------ Round all values to two digits
str_out = cell(6,6);
for i = 1:6
    str_out{1,i}=sprintf('%0.2f',simout(i).stride_time);
    str_out{2,i}=sprintf('%0.2f',simout(i).stride_length);
    str_out{3,i}=sprintf('%0.2f',simout(i).VelHAT_mean(1));
    str_out{4,i}=sprintf('%0.2f',simout(i).DF);
    str_out{5,i}=sprintf('%0.2f',simout(i).MetabEnergy_Umberger/80/simout(i).stride_length);
    ankle_amp=calc_ankle_amp(simout(i));
    str_out{6,i}=sprintf('%0.2f',ankle_amp(3));
end

% ----------------------------------------------- Write results to tex file
text_table={
'\renewcommand{\arraystretch}{1.15}';
%
'\newcommand\tsmin{1.04}';
'\newcommand\lsmin{1.31}';
'\newcommand\vmin{1.06}';
'\newcommand\DFmin{0.59}';
'\newcommand\CoTmin{3.94}';
'\newcommand\Pmin{2.45}';
%
'\begin{tabular}{ l l l l l l l l }';
'& Arch & \multicolumn{3}{c}{\cellcolor{gray!50}rigid} & \multicolumn{3}{c}{mobile} \\';
'\cline{2-8}';
'& Toe  & \multicolumn{2}{c}{none} & \cellcolor{gray!50}decpl. & none & \cellcolor{gray!50}decpl. & cpl. \\'; 
'\multicolumn{8}{c}{} \\[-5pt] % small skip';
'& \textbf{Unit} & \textbf{1s-lA} & \textbf{1s-hA} & \textbf{2s-\gls{TJ}} & \textbf{2s-\gls{MTJ}} & \textbf{3s-\gls{nW}}& \textbf{3s-\gls{W}}\\';
'\hline';  

['$t_{s}$              & \si{s}      & \colorCell{\tsmin}{' str_out{1,1} '}  & \colorCell{\tsmin}{' str_out{1,2} '}  & \colorCell{\tsmin}{' str_out{1,3} '}  & \colorCell{\tsmin}{' str_out{1,4} '}  & \colorCell{\tsmin}{' str_out{1,5} '}  & \colorCell{\tsmin}{' str_out{1,6} '}  \\'];   
['$l_{s}$              & \si{m}      & \colorCell{\lsmin}{' str_out{2,1} '}  & \colorCell{\lsmin}{' str_out{2,2} '}  & \colorCell{\lsmin}{' str_out{2,3} '}  & \colorCell{\lsmin}{' str_out{2,4} '}  & \colorCell{\lsmin}{' str_out{2,5} '}  & \colorCell{\lsmin}{' str_out{2,6} '} \\'];     
['$\Bar{v}_\text{HAT}$ & \si{m/s}    & \colorCell{\vmin}{' str_out{3,1} '}   & \colorCell{\vmin}{' str_out{3,2} '}   & \colorCell{\vmin}{' str_out{3,3} '}   & \colorCell{\vmin}{' str_out{3,4} '}   & \colorCell{\vmin}{' str_out{3,5} '}   & \colorCell{\vmin}{' str_out{3,6} '} \\'];   
['DF                   & \si{-}      & \colorCell{\DFmin}{' str_out{4,1} '}  & \colorCell{\DFmin}{' str_out{4,2} '}  & \colorCell{\DFmin}{' str_out{4,3} '}  & \colorCell{\DFmin}{' str_out{4,4} '}  & \colorCell{\DFmin}{' str_out{4,5} '}  & \colorCell{\DFmin}{' str_out{4,6} '} \\'];
['CoT                  & \si{J/kg m} & \colorCell{\CoTmin}{' str_out{5,1} '} & \colorCell{\CoTmin}{' str_out{5,2} '} & \colorCell{\CoTmin}{' str_out{5,3} '} & \colorCell{\CoTmin}{' str_out{5,4} '} & \colorCell{\CoTmin}{' str_out{5,5} '} & \colorCell{\CoTmin}{' str_out{5,6} '}\\'];
['$P_\text{amp}$       &  \si{-}     & \colorCell{\Pmin}{' str_out{6,1} '}   & \colorCell{\Pmin}{' str_out{6,2} '}   & \colorCell{\Pmin}{' str_out{6,3} '}   & \colorCell{\Pmin}{' str_out{6,4} '}   & \colorCell{\Pmin}{' str_out{6,5} '}   & \colorCell{\Pmin}{' str_out{6,6} '}\\'];
'\hline';
'\end{tabular}';
};

% -------------------------------- Table path and name for saving .tex file
fpath_tex = fullfile(local_path, 'Visualization_and_Analysis');
file = fopen([fpath_tex '\results_gaitmeasures.tex'],'w');
fprintf(file,'%s\n',text_table{:});
fclose(file);