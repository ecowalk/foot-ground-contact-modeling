%% table_testbench.m
% -------------------------------------------------------------------------
% table_testbench.m - Creates testbench results table in the paper
% appendix
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann
% 
% -------------------------------------------------------------------------

close all; clear; clc

stk = dbstack; filepath = which(stk(1).file);
local_path= filepath(1:strfind(filepath,'Visualization_and_Analysis')-1);

addpath ([local_path 'Visualization_and_Analysis']); 
addpath ([local_path 'Visualization_and_Analysis\Figures_Paper']); 
addpath ([local_path 'Visualization_and_Analysis\Human_Data']); 
addpath ([local_path 'Visualization_and_Analysis\Standard_Figures']); 

% ----------------------------------------------------- Load testbench data
load('out_plantarflexed.mat');
[testbench.plantar] = testbench_measures(out_testbench, 2);
load('out_dorsiflexed.mat');
[testbench.dorsi] = testbench_measures(out_testbench, 2);

% ----------------------------------------------- Write results to tex file
text_table =  {
    '\begin{tabular*}{\columnwidth}{@{\extracolsep{\fill}\quad} p{3cm} l | l l | l l }';
    '& &\multicolumn{2}{c|}{\textbf{plantarflexed}} & \multicolumn{2}{c}{\textbf{dorsiflexed}} \\';
    '& \textbf{Unit} &\textbf{Simulation} & \textbf{Human \cite{Welte.2018}} & \textbf{Simulation} & \textbf{Human \cite{Welte.2018}} \\ \hline';
    %
    ['arch elongation                   & \si{-}     & ' num2str(round(testbench.plantar.arch_elongation,1)) ' & 5.6  $\pm$ 2.3  & ' num2str(round(testbench.dorsi.arch_elongation,1)) ' & 8.7  $\pm$ 2.9\\'];
    ['$\Delta \varphi_\text{\gls{MTJ}}$ & \si{deg}   & ' num2str(round(testbench.plantar.delta_MLA,1)) ' & 2.9  $\pm$ 0.8  & ' num2str(round(testbench.dorsi.delta_MLA,1)) ' & 2.6  $\pm$ 1.0\\'];
    '\hline';
    ['energy absorbed                   & \si{mJ/kg} & ' num2str(round(testbench.plantar.energy_absorbed,1)) ' & 22.5 $\pm$ 5.8  & ' num2str(round(testbench.dorsi.energy_absorbed,1)) ' & 25.9 $\pm$ 9.9\\'];
    ['energy returned                   & \si{mJ/kg} & ' num2str(round(testbench.plantar.energy_returned,1)) ' & 16.7 $\pm$ 5.5  & ' num2str(round(testbench.dorsi.energy_returned,1)) ' & 17.9 $\pm$ 5.9\\'];
    ['energy dissipated                 & \si{mJ/kg} & ' num2str(round(testbench.plantar.energy_dissipated,1)) ' & 5.9  $\pm$ 4.0  & ' num2str(round(testbench.dorsi.energy_dissipated,1)) ' & 8.0  $\pm$ 5.9\\'];
    ['energy ratio                      & \si{-}     & ' num2str(round(testbench.plantar.energy_ratio,2)) ' & 0.74 $\pm$ 0.15 & ' num2str(round(testbench.dorsi.energy_ratio,2)) ' & 0.71 $\pm$ 0.12\\'];
    '\hline';
'\end{tabular*}'
};

% -------------------------------- Table path and name for saving .tex file
fpath_tex = fullfile(local_path, 'Visualization_and_Analysis');
file = fopen([fpath_tex '\testbench.tex'],'w');
fprintf(file,'%s\n',text_table{:});
fclose(file);
