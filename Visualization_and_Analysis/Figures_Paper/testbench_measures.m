function [table] = testbench_measures(out, t_start)
arguments
    out      (1,1) Simulink.SimulationOutput
    t_start  (1,1) double % time in seconds when load case starts (2s)
end
% -------------------------------------------------------------------------
% This function evaluates the results of fgcm_testbench.slx. The simulation
% tries to emulate the fast loadcase in [1].
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-12-2021, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann
% 
% -------------------------------------------------------------------------
%% References:  
% [1]   Welte, L., Kelly, L. A., Lichtwark, G. A., and Rainbow, M. J. 
%       “Influence of the windlass mechanism on arch spring mechanics 
%       during dynamic foot arch deformation”. In: J. R. Soc. Interface 
%       15.145 (2018). DOI: 10.1098/rsif.2018.0270
% -------------------------------------------------------------------------


% Arch elongation in Welte et.al. 2018: (l_max-l_min)/l_max. 
% Our definition of arch length is considerably shorter so we will 
% normalize with the mean of the maximum arch lengths
arch_max_paper = 0.2601;
ind_start = find(out.Foot_Kinematics_L.PA_l.Time == t_start);
table.arch_elongation = 1000*(max(out.Foot_Kinematics_L.PA_l.Data(ind_start:end))...
              - out.Foot_Kinematics_L.PA_l.Data(ind_start))/arch_max_paper;

% Arch compression: absolute values in mm are given
ind_start = find(out.Foot_Kinematics_L.MTJ_phi.Time == t_start);
table.delta_MLA = rad2deg(max(out.Foot_Kinematics_L.MTJ_phi.Data(ind_start:end)) ...
                          - out.Foot_Kinematics_L.MTJ_phi.Data(ind_start));

ind_start = find(out.arch_height.ey.Time == t_start);
e_start = 1000*out.arch_height.ey.Data(ind_start)/80;
e_max = 1000*max(out.arch_height.ey.Data(ind_start:end))/80;
e_end = 1000*out.arch_height.ey.Data(end)/80;

table.energy_absorbed = e_max-e_start;
table.energy_returned = e_max-e_end;
table.energy_dissipated = e_end-e_start;
table.energy_ratio = table.energy_returned/table.energy_absorbed;
end

