# Human Reference Data
**Note:** The van der Zee dataset [1] is the default dataset for plotting. Data not available in digital form have been extracted from PDFs/scans using the [Web Plot Digitizer][7]. For corresponding scans and images, please contact alexandra.buchmann@tum.de or check the sources listed in [# References](#References)

General explanation of variable names:
- var_name = mean value
- var_name*stdlow* = lower limit of standard deviation
- var_name*stdup*  = upper limit of standard deviation


### From *van der Zee 2022* [1]
Up-to-date and extensive dataset with 33 different experiments, 9 subjects and various experimental conditions [1]. Settings for data stored in ```vdZee_XXX.mat```: *left leg*, *saggital plane* averages over all nine subjects for five stride averages per subject walking at *1.25 m/s preferred walking speed*.  You can change the settings of the ```.mat`` file using ```create_mat_vdZee.m`` (see notes below for details).

**Joint angles (RoM), joint torques, joint power, muscle activations (ACT), and ground reaction forces (GRFs) for different walking speeds and experimental conditions for nine subjects.**
1. ```vdZee_GRFs.mat```  
**Data format**: ``` (t [% stance] - F [N])```   
**var_names**: vdZeeGRFs...
	- lateralshear (forces)
	- prog (progression forces)
	- vertical (forces)	
			
2. ```vdZee_RoM.mat```  
**Data format**: ```(t [% gait stirde] - angle [deg])```  
**var_names**: vdZee...
	- AnkleRoM...
	- KneeRoM...
	- HipRoM...
			
3. ```vdZee_Torques.mat```  
**Data format**: ```(t [% of stance] - torque [Nm/kg])```  
**var_names**: vdZee...
	- AnkleTorque
	- KneeTorque
	- HipTorque

3. ```vdZee_Power.mat```  
**Data format**: ```(t [% of stance] - power [W/kg])```  
**var_names**: vdZee...
	- AnklePower
	- KneePower
	- HipPower

**Note**: use ```create_mat_vdZee.m``` to change the settings for planes *(sagittal, frontal, transverse plane)*, experimental setting *(preferred walking, constant step length, constant step frequency, constant speed)*, walking speed *(0.7, 0.9, 1.1, 1.25, 1.40, 1.6, 1.8, 2.0)* and the foot to look at *(left, right)*. Calling this function will overwrite the existing files. For details on the available options, see ```help select_samples_vdZee.m``.  

---
### From *Perry 2010* [2]
**Joint angles (RoM), joint torques, muscle activations (ACT) and ground reaction forces (GRFs).**
Data digitized from scans of the corresponding book pages [1].  

**Note**: GRFs and torques only for stance phase (60% of stride time), ACT and RoM for whole stride - check data format!  

1. ```Perry_ACT.mat```  
**Data format**: ```(t [% stirde] - ACT [%MMT(Max.ManualMuscletestvalue)])```  
**var_names**: Perry...
	- GAS (gastrocnemius)
	- GLUmax (gluteus maximus)
	- HAMsb (semimembranosis)  
	- HFLal (adductor longus)
	- SOL (soleus)
	- TA (tibialis anterior)  
	- VASlat (vastus lateralis)  

2. ```Perry_GRFs.mat```  
**Data format**: ``` (t [% stance] - F [%bw])```  
**Additional info**: 20lbs = 10,44 %bw    
**var_names**: PerryGRFs...
	- lateralshear (forces)
	- prog (progression forces)
	- vertical (forces)	
			
3. ```Perry_RoM.mat```  
**Data format**: ```(t [% gait stirde] - angle [deg])```  
**var_names**: Perry...
	- AnkleRoM...
		* stdlow 
		* stdup  
	- KneeRoM...
		* stdlow 
		* stdup  
	- ThighRoM...
		* stdlow 
		* stdup  
			
4. ```Perry_Torques.mat```  
**Data format**: ```(t [% of stance] - torque [%bw per leg length])```  
**var_names**: Perry...
	- AnkleTorque
	- KneeTorque
	- HipTorque

---
### From *Lichtwark 2006* [3]
```Lichtwark_DeltaMTU_GAS_level_walking.mat``` digitized from paper [2]  

**Data format**: ```(time [s] - MTU length changes [mm])```  
**Additional info**: 0.604s stance time (60% of stride time) - MTU length changes relative to length at toe off (0.604s)    

**var_names**: DeltaMTUGAS... stdlow; stdup

---
### From *Cronin 2013* [4]
```Cronin_Ankle_Power.mat``` digitized from paper [3]
 
**Data format**: ```(time [% gait cycle/stirde] - ankle power [W/kg])```  
**Additional info**: take off at t = 62,405s  

**var_names**: CroninAnklePower... stdlow; stdup

---
### From *Ishikawa 2005* [5]
```Ishikawa_GAS_SOL_MTU.mat``` digitized from paper [4] 
 
**Data format**: ```(t [% of stance] - change in length [% from heel strike] )```  
**Additional info**: data only for stance phase (60% of stride time)  

**var_names**: Ishikawa... GAS; SOL

---
### From *Lipfert 2014* [6]
```Lipfert_Ankle_Data.mat``` from authors (Daniel Renjewski, daniel.renjewski@tum.de)  

**Additional information**: body mass in the experiments: m_bw =70.9±11.7 kg  

**var_names and data format**:
1. Single subject data:
 	- ```Lip_t [s]```: Time vector 
	- ```Lip_TAnk_single [Nm]```: Ankle torque
	- ```Lip_pow_single [W]```: Ankle power  

2. Mean data for all subjects:  
**Note:** mean data range from 0-100% GC in 0.5% increments
	- ```t = linspace (0, 100, 201)*(1/100) [s]```
	- ```Lip_TAnk_mean [Nm]```: Ankle torque
	- ```Lip_pow_mean [W]```: Ankle power

---
## References
[1]: van der Zee, Tim J.; Mundinger, Emily M.; Kuo, Arthur D. (2022): A biomechanics dataset of healthy human walking at various speeds, step lengths and step widths. In: Sci Data 9 (1), S. 704. DOI: 10.1038/s41597-022-01817-1.

[2]: Perry, Jacquelin; Burnfield, Judith M.; Cabico, L. M. (2010): Gait Analysis. Normal and Pathological Function: SLACK.

[3]: Lichtwark, G. A.; Wilson, A. M. (2006): Interactions between the human gastrocnemius muscle and the Achilles tendon during incline, level and decline locomotion. In: The Journal of Experimental Biology 209 (Pt 21), S. 4379–4388. DOI: 10.1242/jeb.02434.

[4]: Cronin, Neil J.; Prilutsky, Boris I.; Lichtwark, Glen A.; Maas, Huub (2013): Does ankle joint power reflect type of muscle action of soleus and gastrocnemius during walking in cats and humans? In: Journal of Biomechanics 46 (7), S. 1383–1386. DOI: 10.1016/j.jbiomech.2013.02.023.

[5]: Ishikawa, Masaki; Komi, Paavo V.; Grey, Michael J.; Lepola, Vesa; Bruggemann, Gert-Peter (2005): Muscle-tendon interaction and elastic energy usage in human walking. In: Journal of applied physiology (Bethesda, Md. : 1985) 99 (2), S. 603–608. DOI: 10.1152/japplphysiol.00189.2005.

[6]: Lipfert, Susanne W.; Günther, Michael; Renjewski, Daniel; Seyfarth, André (2014): Impulsive ankle push-off powers leg swing in human walking. In: Journal of Experimental Biology 217 (8), S. 1218–1228. DOI: 10.1242/jeb.097345.

[7]: <https://automeris.io/WebPlotDigitizer/> "Web Plot Digitizer"

