function [human_data] = load_human_data(plots)
arguments
    plots (1,:) double 
end
% -------------------------------------------------------------------------
% [human_data] = load_human_data(plots)
% 
% loads human data from data sets specified in select_human_dataset.m. See
% help select_human_dataset.m for avaliable options and details. 
%
% Input: plots (1,:) double - data you want to compare your outcomes to
%           * 1 - Joint Kinematics
%           * 2 - Joint Torque
%           * 3 - Joint Power 
%           * 4 - Muscle ACT and Joint Angle
%           * 5 - Ground Reaction Forces (GRFs) and Center of Pressure(CoP)
%           * 6 - Foot ROM
%           * 7 - Arch Power and Work
%
% Output: Human_Data (t[% stance] data) 
%                   .ACT: [1×1 struct]
%                  .GRFs: [1×1 struct]
%                   .RoM: [1×1 struct]
%                .Torque: [1×1 struct]
%                 .Power: [1×1 struct]
%
% with Muscle = {GAS, SOL, VASlat, TA, HAMsb, GLUmax, HFLal}
%                                                    e.g. Human_Data.ACTGAS
% with Joint  = {Ankle, Knee, Hip} e.g. Human_Data.AnkleRoM
% 
% Notes: - Leg length neuromuscular simualtion = 1m
%        - Body weight neuromuscular simualtion = 80kg
%        - Perry book: 20lbs = 10,44418851024895 %bw
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-12-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

[RoM_data, Power_data, GRF_data] = select_human_dataset();
                                                
%% Range of Motion and Joint Torques 
if any(plots == 1) || any(plots == 2) || any(plots == 5) || any(plots == 7)
    
switch RoM_data % RoM (t[% stride] angle[deg]); T (t[% stance] torque [Nm])
    case 1 % ------------------------------ van der Zee 2022 sagittal plane
      disp(['<strong>RoM data</strong>: van der Zee 2022 - A biomechanics '...
                'dataset of healthy human walking at various speeds, step '...
                                              ' lengths and step widths.']); 

      load('vdZee_RoM.mat', 'vdZeeAnkleRoM','vdZeeHipRoM','vdZeeKneeRoM');
        human_data.AnkleRoM = vdZeeAnkleRoM;
        human_data.KneeRoM  = vdZeeKneeRoM;
        human_data.HipRoM   = vdZeeHipRoM;
        
      load('vdZee_Torque.mat', 'vdZeeAnkleTorque','vdZeeHipTorque',...
                                                        'vdZeeKneeTorque');
        human_data.AnkleTorque = vdZeeAnkleTorque;
        human_data.KneeTorque  = vdZeeKneeTorque;
        human_data.HipTorque   = vdZeeHipTorque;
        
       load('vdZee_DS.mat', 'vdZeeDS');
       human_data.DS   = vdZeeDS;
        
    case 2 % --------------------------------------------------- Perry 2010
      disp(['<strong>RoM data</strong>: Perry 2010 - Gait Analysis. '...
                                    ' Normal and Pathological Function.']);
                                 
      load('Perry_RoM.mat', 'PerryAnkleRoM', 'PerryHipRoM', 'PerryKneeRoM');
        human_data.AnkleRoM = PerryAnkleRoM;
        human_data.KneeRoM  = PerryKneeRoM;
        human_data.HipRoM   = PerryHipRoM;

      % --------------------------------------- Torques: stance data only    
      load('Perry_Torques.mat','PerryAnkleTorque','PerryHipTorque',...
                                                        'PerryKneeTorque');
        human_data.AnkleTorque = [PerryAnkleTorque(:,1)*(0.6) ...
                                               PerryAnkleTorque(:,2)/9.81];
        human_data.KneeTorque  = [PerryKneeTorque(:,1)*(0.6)  ...
                                                PerryKneeTorque(:,2)/9.81];
        human_data.HipTorque   = [PerryHipTorque(:,1)*(0.6)   ...
                                                 PerryHipTorque(:,2)/9.81];
                                             
   otherwise 
   error('Load_Human_Data: invalid data set selection for RoM and joint torques!');   
end

end

%% Joint Power
if any(plots == 3)
    
switch Power_data % ----------------------- Power (t[% stride] power[W/kg])
    case 1 % ------------------------------ van der Zee 2022 sagittal plane
        disp(['<strong>All joints power data</strong>: van der Zee 2022 - '...
               'A biomechanics dataset of healthy human walking at various'...
                                ' speeds, step lengths and step widths.']); 
                            
        load('vdZee_Power.mat', 'vdZeeAnklePower','vdZeeHipPower',...
                                                         'vdZeeKneePower');
         human_data.AnklePower = vdZeeAnklePower;
         human_data.KneePower  = vdZeeKneePower;
         human_data.HipPower   = vdZeeHipPower;
         
         load('vdZee_DS.mat', 'vdZeeDS');
       human_data.DS   = vdZeeDS;
        
    case 2 % -------------------------------- Lipfert 2014 ankle power only 
 
        disp(['<strong>Ankle power data</strong>: Lipfert 2014 - Impulsive'...
                    ' ankle push-off powers leg swing in human walking.']); 
                                                  
        load('Lipfert_Ankle_Data.mat','Lip_pow_mean');

        time = linspace (0, 100, 201)'; % [0-100% stance in 0.5% steps]
        % --- Normalize to body weight with m_bw = 70.9±11.7 kg (see paper)
        human_data.AnklePower = [time Lip_pow_mean/(70.9)];
        
        human_data.KneePower  = [0 0];  % Missing human data
        human_data.HipPower   = [0 0];  % Missing human data
         

    case 3 % -------------------------------------- Cronin 2013 ankle power
        disp(['<strong>Ankle power data</strong>: Cronin 2013 - '...
              'Differences in contractile behaviour between the soleus '...
                 'and medial gastrocnemius muscles during human walking']);    
                                       
        load('Human_Data\Cronin_Ankle_Power.mat','CroninAnklePower');
        human_data.AnklePower = CroninAnklePower;
        human_data.KneePower  = [0 0]; % Missing human data
        human_data.HipPower   = [0 0]; % Missing human data
        
    otherwise 
     error('Load_Human_Data: invalid data set selection for joint power!');
end

end

%% Ground Reaction Forces 
if any(plots == 5)
switch GRF_data % -------------------------------- GRFs (t [%stance] F [N])
    case 1 % ------------------------------ van der Zee 2022 sagittal plane
       disp(['<strong>GRF data</strong>: van der Zee 2022 - A biomechanics'...
               ' dataset of healthy human walking at various speeds, step'...
                                             ' lengths and step widths.']); 
       load('vdZee_GRFs.mat','vdZeeGRFvertical','vdZeeGRFprog','vdZeeCOP');
       
         human_data.GRFvertical = vdZeeGRFvertical;
         human_data.GRFprog     = vdZeeGRFprog;
         human_data.COP         = vdZeeCOP;
         
       load('vdZee_DS.mat', 'vdZeeDS');
         human_data.DS   = vdZeeDS;
         
        
    case 2 % --------------------------------------------------- Perry 2010
        disp(['<strong>GRF data</strong>: Perry 2010 - Gait Analysis.'...
                                   ' Normal and Pathological Function.']);
                               
        load('Perry_GRFs.mat','PerryGRFvertical','PerryGRFprog');
        % Perry: initially normalized with [t [% stance] F [%bw]] is now 
        %                            denormalized with the models mass 80kg
         
        human_data.GRFvertical = [PerryGRFvertical(:,1)*0.6 ...
                                        PerryGRFvertical(:,2)*80*9.81/100];
        human_data.GRFprog     = [PerryGRFprog(:,1)*0.6     ...
                                            PerryGRFprog(:,2)*80*9.81/100]; 
        
   otherwise 
        error('Load_Human_Data: invalid data set selection for GRFs!');   
end
end

%% Muscle activations
if any(plots == 4)
% -------------- ACT (t[%stance] act[% MMT(max. manual muscle test value)])
disp(['<strong>Muscle activations</strong>: Perry 2010 - Gait Analysis.'...
                                    ' Normal and Pathological Function.']);

 % ------------------------------------------------------------- Perry 2010                    
load('Perry_ACT.mat','PerryGAS', 'PerryGLUmax', 'PerryHAMsb', ...
                        'PerryHFLal','PerrySOL', 'PerryTA', 'PerryVASlat');
                    
    human_data.ACTGAS     = PerryGAS;
    human_data.ACTGLUmax  = PerryGLUmax;
    human_data.ACTHAMsb   = PerryHAMsb;
    human_data.ACTHFLal   = PerryHFLal;
    human_data.ACTSOL     = PerrySOL;
    human_data.ACTTA      = PerryTA;
    human_data.ACTVASlat  = PerryVASlat;
end
check = exist('human_data','var');
if check ~= 1
    human_data = [];
end
end

