function [RoM_data_source, Power_data_source, GRF_data_source] = ...
                                                     select_human_dataset()  
arguments
end
% -------------------------------------------------------------------------
% [RoM_data_source, Power_data_source, GRF_data_source] = ...
%                                                    select_human_dataset()  
% 
% requests user input to ask for desired human data set used for plotting
% and informs user about necessary requirements for plotting with different
% data sets. Avaliable data sets see reference section of this help.
%
% 
% Outputs: data set indizes ([1,2,3]) for each dataset to pass to 
%          Load_Human_Data.m 
% 
% Default output: van der Zee 2022 [4] data set for RoMs, Torques,
%                 Power, GRFs and Perry 2010 [3] for muscle activations
% 
% Note: human data files from van der Zee 2022 are to large to store them
%       in git - more infos avaliable when calling this function
%
% -------------------------------------------------------------------------
%
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       04-01-2023, Munich
% Last modified:    04-01-2023, Munich 
% 
% -------------------------------------------------------------------------
%% References
%{ 
% #########################################################################
% Ankle Power -------------------------------------------------------------
% [1] Cronin, Neil J.; Prilutsky, Boris I.; Lichtwark, Glen A.; Maas, Huub 
%    (2013): Does ankle joint power reflect type of muscle action of soleus
%     and gastrocnemius during walking in cats and humans? In: Journal of 
%     Biomechanics 46 (7), S. 1383–1386. DOI: 10.1016/j.jbiomech.2013.02.023.
% 
% Ankle Power & Torque; Energy and Impulse --------------------------------
% [2] Lipfert, Susanne W.; Günther, Michael; Renjewski, Daniel; Seyfarth, 
%     André (2014): Impulsive ankle push-off powers leg swing in human 
%     walking. In: Journal of Experimental Biology 217 (8), S. 1218–1228. 
%     DOI: 10.1242/jeb.097345.
% 
% Joint Torques & Angles (RoM), GRFs, Muscle Activations ------------------
% [3] Perry, Jacquelin; Burnfield, Judith M.; Cabico, L. M. (2010): 
%     Gait Analysis. Normal and Pathological Function: SLACK.
%
% Human Gait Data (RoMs, Torques, Power, GRFs) ----------------------------
% [4] van der Zee, Tim J.; Mundinger, Emily M.; Kuo, Arthur D. (2022): A 
%     biomechanics dataset of healthy human walking at various speeds, step 
%     lengths and step widths. In: Sci Data 9 (1), S. 704. 
%     DOI: 10.1038/s41597-022-01817-1.
%
% #########################################################################
%}
%

%% User Input
prompt_RoM   = ['Which human dataset do you want to plot for joint angle and torque (1/2)? ' ...
               '(1 = van der Zee 2022; '... 
                '2 = RoM Perry 2010)'];
            
prompt_Power = ['Which human dataset do you want to plot for joint power (1/2/3)? '...
                '(1 = van der Zee 2022; '...
                 '2 = Lipfert 2014 (ankle only); ' ...
                 '3 = Cronin 2013 (ankle only))'];

prompt_GRFs  = ['Which human dataset do you want to plot for GRFs (1/2)? '...
                '(1 = van der Zee 2022; '...
                 '2 = Perry 2010)'];
             
% --------------------------- ask for user input regarding desired data set             
RoM_data_source   = input(prompt_RoM);    
Power_data_source = input(prompt_Power);
GRF_data_source   = input(prompt_GRFs);

%% Check Input and Set Default 
% --------------------- Check for empty or wrong inputs and set default RoM
if isempty(RoM_data_source) || (RoM_data_source ~= 1 &&...
                                RoM_data_source ~= 2)  
                            
    warning(['Selected default human data set for joint angle and torque '...
                                                   '(van der Zee 2022).']);
    RoM_data_source = 1; 
    
end

% ------------------- Check for empty or wrong inputs and set default Power
if isempty(Power_data_source) || (Power_data_source ~= 1 && ...
                                  Power_data_source ~= 2 && ...
                                  Power_data_source ~= 3)
                              
    warning(['Selected default human data set for joint power '...
                                                   '(van der Zee 2022).']);
    Power_data_source = 1; 
    
end

% -------------------- Check for empty or wrong inputs and set default GRFs
if isempty(GRF_data_source) || (GRF_data_source ~= 1 &&...
                                GRF_data_source ~= 2)
                            
    warning(['Selected default human data set for ground reaction forces '...
                                                   '(van der Zee 2022).']);
    GRF_data_source = 1; 
    
end
end

