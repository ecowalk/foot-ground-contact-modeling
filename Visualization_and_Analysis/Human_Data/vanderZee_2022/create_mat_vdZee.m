function [] = create_mat_vdZee()
arguments
end
% -------------------------------------------------------------------------
% [] = create_mat_vdZee()
%
% creates .mat files for van der Zee 2022 data for RoM, Torque, Power and
% ground reaction forces and double stance
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       04-01-2023, Munich
% Last modified:    23-06-2023, Munich 
% 
% -------------------------------------------------------------------------

%% --------------------------- Refer to data and paper for van der Zee 2022
warning(['To use data from van der Zee 2022 you have to download all data '...
         'and run the corresponding code from Github to create the '...
         'Level 3 - MATLAB files folder for plotting.']);

prompt = ['Do you already have a Level 3 - MATLAB files folder '...
            'INSIDE the current folder vanderZee_2022? (1=yes / 0=no)']; 
check  = input(prompt);

if check 
    addpath([pwd '\Level 3 - MATLAB files']);
    
else
    web https://github.com/timvanderzee/human-walking-biomechanics
    web https://www.nature.com/articles/s41597-022-01817-1
    
    error(['You need to download the corresponding data first. The'...
             ' Github page and the paper will open in your browser now.']);
end

%% --------------------------------------- Range of motion van der Zee 2022
[vdZeeAnkleRoM,~,~] = select_samples_vdZee('angle', 'ankle', '1.25');                             
[vdZeeKneeRoM,~,~]  = select_samples_vdZee('angle', 'knee', '1.25');   
[vdZeeHipRoM ,~,~]  = select_samples_vdZee('angle', 'hip', '1.25');

% ------------------- flip signs for knee angle to match EcoWalk convention
vdZeeKneeRoM = flip_sign(vdZeeKneeRoM); 

save('vdZee_RoM_wenzler.mat','vdZeeAnkleRoM','vdZeeKneeRoM','vdZeeHipRoM'); 

%% ----------------------------------------- Joint torques van der Zee 2022
[vdZeeAnkleTorque,~,~] = select_samples_vdZee('moment', 'ankle', '1.25');
[vdZeeKneeTorque,~,~]  = select_samples_vdZee('moment', 'knee', '1.25');
[vdZeeHipTorque ,~,~]  = select_samples_vdZee('moment', 'hip', '1.25');

% --------- flip signs for ankle and hip torque to match EcoWalk convention
vdZeeAnkleTorque = flip_sign(vdZeeAnkleTorque);
vdZeeHipTorque   = flip_sign(vdZeeHipTorque);

save('vdZee_Torque_wenzler.mat',...
                    'vdZeeAnkleTorque','vdZeeKneeTorque','vdZeeHipTorque'); 

%% ------------------------------------------- Joint power van der Zee 2022
[vdZeeAnklePower,~,~] = select_samples_vdZee('power', 'ankle', '1.25');
[vdZeeKneePower,~,~]  = select_samples_vdZee('power', 'knee', '1.25');
[vdZeeHipPower ,~,~]  = select_samples_vdZee('power', 'hip', '1.25');

save('vdZee_Power_wenzler.mat','vdZeeAnklePower','vdZeeKneePower','vdZeeHipPower');

%% --------------------------------- Ground reaction forces van der Zee 2022
[GRFs,~,~] = select_samples_vdZee('GRFs', 'ankle', '1.25');
vdZeeGRFvertical = [GRFs(:,1) GRFs(:,2)];
vdZeeGRFprog     = [GRFs(:,1) GRFs(:,3)]; 
vdZeeCOP     = [GRFs(:,1) GRFs(:,4)]; 

save('vdZee_GRFs_wenzler.mat','vdZeeGRFvertical','vdZeeGRFprog','vdZeeCOP');

%% ----------------------------- Average Stride Time (for comparison table)
% ------------------------ !! Only possible for MoCap vars, NOT for GRFs !!
[~,vdZeeStrideTime,~] = select_samples_vdZee('angle', 'ankle', '1.25'); 
save('vdZee_StrideTime_wenzler.mat','vdZeeStrideTime');

%% ----------------------------- double stance normalized to 1; row 1 opposite toe off; row 2 opposite heel strike, row 3 toe off
% ------------------------ !! Only possible for MoCap vars, NOT for GRFs !!
[~,~,vdZeeDS] = select_samples_vdZee('angle', 'ankle', '1.25'); 
save('vdZee_DS_wenzler.mat','vdZeeDS');
end

