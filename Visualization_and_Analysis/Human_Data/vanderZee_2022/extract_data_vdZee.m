function [time, subjmean, stridetime_mean, DS_mean] = extract_data_vdZee(typename, ...
                                               variablename, trials, plane)
arguments
    typename        (1,:) char
    variablename    (1,:) char
    trials          (1,:) double 
    plane           (1,1) double {mustBeMember(plane,[1 2 3])}
end
% -------------------------------------------------------------------------
% [time, subjmean, stridetime_mean, DS_mean] = extract_data_vdZee(typename, ...
%                                              variablename, trials, plane)
%
% loads data as specified by variablename and typename and calculates 
% 	(1) first the mean of five strides for each subject and trial and
%   (2) second the mean over all subjects for each trial (subjmean) with
%   the corresponding time vector (time)
% 
% INPUTS: 
%   - typename: string of the name of the field that contains the variable.
%                                               Example: 'Link_Model_Based'
%
%   - varibalename: A string that is equal to the variable name. 
%                                                    Example: 'r_kne_power' 
%               Note: the variable has to be written the same way that it
%                 is in the data file so that this function can call on it.
%
%   - trials: any combination of trials from 1-33 you are interested in 
%             plotting selected by the choosen experiment. TRIAL LIST:
%                * constant step length: [1, 4, 7, 33, 12, 15]   
%                * constant step freq:   [3, 6, 9, 10, 13, 31]
%                * constant speed:       [17:25]
%                * perferred speed:      [2, 5, 8, 11, 14, 16, 20:22, 32]
%
%   - plane: plane to be visualized 1 = Sagittal plane
%                                   2 = Frontal plane
%                                   3 = Transverse plane
%
% -------------------------------------------------------------------------
% 
% © van der Zee, Tim J. University of Calgary, Biomedical Engineering 
%   Graduate Program, Calgary, Canada. ✉e-mail: tim.vanderzee@ucalgary.ca
%
%  Adaptations by Alexandra Buchmann, Chair of Applied Mechanics, TUM:
%                           1) function description
%                           2) arguments check
%                           3) indentation
%                           4) error messages
%                           5) ground reaction force subject mean and
%                           output of horizontal and vertical GRFs
%                           6) variable initialization
%                           7) code cleanup
%                           8) include stride time calculation
% 
%  Adaptions by Simon Wenzler: added DS_mean
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-01-2023, Munich
% Last modified:    23-06-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Initialize Variables
no_subjects      = 9;   % nine subjects with 5 strides data files
no_interp_points = 201;
time             = linspace(0,100,no_interp_points)';

index = zeros(2, length(trials)); 
var   = zeros(no_interp_points, length(trials), no_subjects);
var_h = zeros(no_interp_points, length(trials), no_subjects);
var_v = zeros(no_interp_points, length(trials), no_subjects);
var_cop = zeros(no_interp_points, length(trials), no_subjects);
stridetime = zeros(length(trials), no_subjects);
stridetime_mean = zeros(length(trials),1);
DS = zeros(3,length(trials), no_subjects);
DS_mean = zeros(3,length(trials));
    if contains(variablename,'GRFs')
        % -- subjmean with third dimension for horizontal and vertical GRFs
        % and COP
        subjmean   = zeros(length(var),length(trials),3);

    else
            % ---- subjmean with two dimension for all other variable types
            subjmean   = zeros(length(var),length(trials));
    end

%% Extract Data
% ------------------------------ Put trials in order from low to high speed
trialsvel = [1:9, 17:33, 10:16]; 

for t = 1:length(trials)
    
    index(1,t) = find(trialsvel==trials(t));
    index(2,t) = trials(t);
    
end
trialsort = sortrows(index');
trials    = trialsort(:,2);

% - Load data and calculate average of 5 strides for each subject and trial 
for subject = 1:no_subjects 
    % --------- Load data and define column of interest within the variable
    disp(['Retrieving data for subject: ', num2str(subject)])
    load ((strcat('p', num2str(subject), '_5StridesData.mat')),'data')
    
% -------------------------------- Use grfs for all trials to detect stride
    for trial = 1:length(trials)
        
        if isempty(data(trials(trial)).Force) == 1 
            continue
            
        else 
            GRFL = [data(trials(trial)).Force.force1(:,1), ...
                        data(trials(trial)).Force.force1(:,2), ...
                            data(trials(trial)).Force.force1(:,3)];
                
            GRFR = [data(trials(trial)).Force.force2(:,1),...
                        data(trials(trial)).Force.force2(:,2),...
                            data(trials(trial)).Force.force2(:,3)];
        end   
        
        % --------------- Get heelstrikes for the left and right (hsl, hsr)
        [hsl,tol, hsr, tor] = invDynGrid_getHS_TO_vdZee(GRFL, GRFR, 20);
        % ------------- Last heelstrike often not found with edge detection
        hsr(6) = length(GRFL); 
        
        % ------------------------------------------- Select leg to look at
        if contains(variablename, 'l_')  
            hs = hsl;
           ohs = hsr; 
            to1 = tor;
            to2 = tol;
        elseif contains(variablename, 'r_') 
            hs = hsr; 
           ohs = hsl;
           to1 = tol;
           to2 = tor;
        end
        
            if ~contains(variablename,'GRFs')
            % Divide by ten: mocap frequ. 10x less than force plates (grfs)
                hs = ceil(hs/10);
               ohs = ceil(ohs/10);
                to1 = ceil(to1/10);
                to2 = ceil(to2/10);

            end
            hs             = unique(hs);
           ohs            = unique(ohs);
            to1            = unique(to1);
            to2            = unique(to2);
            hs(diff(hs)<5) = [];
           ohs(diff(ohs)<5) = [];
            to1(diff(to1)<5) = [];
            to2(diff(to2)<5) = [];
           if ohs(1)<hs(1)
               ohs = ohs(2:end);
           end
           if to1(1)<hs(1)
               to1 = to1(2:end);
           end
           if to2(1)<hs(1)
               to2 = to2(2:end);
           end
               
        
        if contains(variablename,'GRFs')
        % -- Interpolate data from 0-100% stride time with no_interp_points
        interp_v = interpolate_to_percgaitcycle_vdZee(...
                                data(trials(trial)).Force.force1(:,3), ...
                                                     hs, no_interp_points); 
                                              
        interp_h = interpolate_to_percgaitcycle_vdZee(...
                                data(trials(trial)).Force.force1(:,2), ...
                                                     hs, no_interp_points); 
                                                
        interp_cop = interpolate_to_percgaitcycle_vdZee(...
                                data(trials(trial)).Force.cop1(:,2), ...
                                                     hs, no_interp_points); 
                                              
        % ----------------- Mean of 5 strides for each subject and variable                                      
        var_v(:, trial, subject) = mean(interp_v , 2, 'omitnan'); 
        var_h(:, trial, subject) = mean(interp_h , 2, 'omitnan');
        var_cop(:, trial, subject) = mean(interp_cop , 2, 'omitnan');
        else
           % Interpolate data from 0-100% stride time with no_interp_points
           interp = interpolate_to_percgaitcycle_vdZee(...
                    data(trials(trial)).(typename).(variablename)(:,plane), ...
                                                     hs ,no_interp_points);
           % -------------- Mean of 5 strides for each subject and variable
           var(:, trial, subject) = mean(interp , 2, 'omitnan'); 
           
        % -- Calculate stride time (only possible for normal mocap vars,
        % not for GRFs
        % mod wenzler:stridetime mean
        real_time = data(trials(trial)).Time.TIME(hs(1):hs(end));
        stridetime (trial, subject) = (real_time(end)-real_time(1))/(length(hs)-1);
        ohs=ohs-hs(1)+1;
        to1=to1-hs(1)+1;
        to2=to2-hs(1)+1;
        hs=hs-hs(1)+1;
        
        % get double stance DS(1) opp toe off, DS(2) opp heel strike, 
        % DS(3) toe off
        for i = 1:length(hs)-1
            DS(1,trial, subject) = DS(1,trial, subject) + real_time(to1(i)) - real_time(hs(i));
            DS(2,trial, subject) = DS(2,trial, subject) + real_time(ohs(i)) - real_time(hs(i));
            DS(3,trial, subject) = DS(3,trial, subject) + real_time(to2(i)) - real_time(hs(i));
        end
        DS(:,trial, subject) = DS(:,trial, subject)/(length(hs)-1)/stridetime(trial, subject);
    end
    
end

% ---------------------------------------- Take trial average over subjects 
for trial = 1:length(trials)
    if contains(variablename,'GRFs')
    % -------------------------- Mean data over all subjects for each trial
        subjmean(:,trial,1) = mean(var_v(:,trial,:), 3);
        subjmean(:,trial,2) = mean(var_h(:,trial,:), 3);   
        subjmean(:,trial,3) = mean(var_cop(:,trial,:), 3);
        
        % Calculate stride time only possible for mocap vars, not for GRFs
        stridetime_mean (trial) = NaN; 
        
    else
    % -------------------------- Mean data over all subjects for each trial
        subjmean(:,trial) = mean(var(:,trial,:), 3);
        stridetime_mean (trial) = mean(stridetime(trial,:), 2);
        DS_mean(1,trial) = mean(DS(1,trial,:),3);
        DS_mean(2,trial) = mean(DS(2,trial,:),3);
        DS_mean(3,trial) = mean(DS(3,trial,:),3);
    end
end


end

