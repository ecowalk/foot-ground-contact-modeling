function [human_data] = flip_sign(human_data)
arguments
    human_data double
end
% -------------------------------------------------------------------------
% [human_data] = flip_sign(human_data)
%
% flips the sign of all datapoints in human_data
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       04-01-2023, Munich
% Last modified:    04-01-2023, Munich 
% 
% -------------------------------------------------------------------------

human_data = [human_data(:,1) human_data(:,2)*(-1)];

end

