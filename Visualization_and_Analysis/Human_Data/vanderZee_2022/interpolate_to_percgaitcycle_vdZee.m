function[xi] = interpolate_to_percgaitcycle_vdZee(x,segm,npoints)
% -------------------------------------------------------------------------
% [xi] = interpolate_to_percgaitcycle_vdZee(x,segm,npoints)
%
% interpolates data given in x with npoints between two susequent heel
% strikes defined in segm
%
% -------------------------------------------------------------------------
% 
% © van der Zee, Tim J. University of Calgary, Biomedical Engineering 
%   Graduate Program, Calgary, Canada. ✉e-mail: tim.vanderzee@ucalgary.ca
%
%  Adaptations by Alexandra Buchmann, Chair of Applied Mechanics, TUM:
%                           1) function description
%                           2) comments and code cleanup
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-01-2023, Munich
% Last modified:    04-01-2023, Munich 
% 
% -------------------------------------------------------------------------

% ----------------------------------------------------------- Init variable 
xi = nan(npoints,length(segm)-1);

for j = 1:(length(segm)-1)
    % ------------------------------- heelstrike to heelstrike, finite only
    xseg = x(segm(j):segm(j+1));
    xfin = xseg(isfinite(xseg)); xfin = xfin(:);
    
    % ------------------------------------------ make artifical time vector
    tart = linspace(0, 100, length(xfin)); tart = tart(:);
    
    % --------------------------------------------------------- interpolate
    xi(:,j) = interp1(tart, xfin, linspace(0, 100, npoints));
end

end