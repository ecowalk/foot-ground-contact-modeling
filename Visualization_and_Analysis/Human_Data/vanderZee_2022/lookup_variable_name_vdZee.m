function[variablename, typename, trials] = lookup_variable_name_vdZee...
                                                (variable, leg, joint, exp)
arguments
    variable (1,:) char {mustBeMember(variable,{'angle','moment','power','GRFs'})}
    leg      (1,:) char {mustBeMember(leg,{'left', 'right'})}
    joint    (1,:) char {mustBeMember(joint,{'ankle','knee','hip'})}
    exp      (1,:) char {mustBeMember(exp,{'preferred walking','constant speed'...
                          'constant step length','constant step frequency'})}
end
% -------------------------------------------------------------------------
% [variablename, typename, trials] = lookup_variable_name_vdZee...
%                                               (variable, leg, joint, exp)
% 
% returns the variablename to be loaded from data files, the typename 
% 'Link_Model_Based' and the trialnumber matching the defined experiment 
% variables
% 
% Inputs: 
%       - variable: 'angle', 'moment', 'power', 'GRFs'
%       - leg:      'left' or 'right'
%       - joint:    'ankle', 'knee', 'hip'
%       - exp:      'preferred walking', 'constant step length', 
%                     'constant step frequency', 'constant speed'
% 
% Outputs: 
%       - variablename: name of variable in data files e.g. 'l_ank_angle'
%
%       - typename: name of the field where the variable can be found - 
%                                                        'Link_Model_Based'
%
%       - trials: trial numbers for trials matching the experiment defined 
%                 in exp. The trials are returned from lowest to highest 
%                 walking speed
% 
% -------------------------------------------------------------------------
% 
% © van der Zee, Tim J. University of Calgary, Biomedical Engineering 
%   Graduate Program, Calgary, Canada. ✉e-mail: tim.vanderzee@ucalgary.ca
%
%  Adaptations by Alexandra Buchmann, Chair of Applied Mechanics, TUM:
%                           1) function description
%                           2) arguments check
%                           3) indentation
%                           4) error messages
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-01-2023, Munich
% Last modified:    04-01-2023, Munich 
% 
% -------------------------------------------------------------------------

typename = 'Link_Model_Based';

switch joint
    case 'ankle' 
        jointn  = 'ank';
        
    case 'knee'
        jointn  = 'kne';
        
    case 'hip'
        jointn  = 'hip';
        
    otherwise
       error('lookup_variable_name_vdZee: unable to identify joint name!');
end


variablename = strcat(leg(1), '_', jointn, '_', variable);

switch exp
    case 'constant step length'
        trials= [1, 4, 7, 33, 12, 15];
        
    case 'constant step frequency'
        trials= [3, 6, 9, 31, 10, 13];
        
    case 'constant speed'
        trials = [17:19, 23:25]; 
        
    case 'preferred walking'
        trials= [2, 5, 8, 20, 32, 11, 14, 16];
        
    otherwise 
        error('lookup_variable_name_vdZee: unable to identify experiment!');
end
end

        
