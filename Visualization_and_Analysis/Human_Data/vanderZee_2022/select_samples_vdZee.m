function [human_data, st_out, DS_out] = select_samples_vdZee(variable, joint,...
                                                            speed, options)
arguments
   variable (1,:) char {mustBeMember(variable,{'angle','moment','power','GRFs'})}
   joint    (1,:) char {mustBeMember(joint,{'ankle','knee','hip'})}
   speed    (1,:) char {mustBeMember(speed,{'0.7' ,'0.9','1.1','1.25',...
                                            '1.40','1.6','1.8','2.0'})}
   
   options.plane (1,1) double {mustBeMember(options.plane,[1 2 3])} = 1
    
   options.leg (1,:) char {mustBeMember(options.leg,{'left', 'right'})} ...
                                                                   = 'left'
                                                               
   options.exp (1,:) char {mustBeMember(options.exp,{'preferred walking',...
                          'constant step length','constant step frequency',...
                                  'constant speed'})} = 'preferred walking'
                                                   
end
% -------------------------------------------------------------------------
% [human_data, st_out] = select_samples_vdZee(variable, joint,...
%                                                           speed, options)
%
% function extracts desired data from entire dataset of van der Zee 2022.
% 
% Inputs: 
%       - variable: {'angle', 'moment', 'power', 'GRFs'}
%       - joint:    {'ankle', 'knee', 'hip'}
%       - speed:    {'0.7' ,'0.9','1.1','1.25','1.40','1.6','1.8','2.0'}
%                   desired walking speed - should be similar to the
%                   average forward velocity of the simulation
%       
%       - options.leg   (1,:) char   = 'left' (or 'right')
%
%       - options.exp   (1,:) char   = 'preferred walking' 
%                                            (or 'constant step length', 
%                                                'constant step frequency', 
%                                                'constant speed')
%
%       - options.plane (1,1) double = 1 with 1 = Sagittal plane
%                                             2 = Frontal plane
%                                             3 = Transverse plane
%
% for details on all the variables see code from van der Zee and
% corresponding function helps 
%
% Output: 1) human data (t[% stride time] data)
%         2) st_out - average stride time 
% 
%       --> data are the mean value of five stride for each subject over 
%           all 9 subjects for the selected trial
%       --> select suitable trial to have approx. the same forward velocity
%           than the model
% 
% Trials: 1) '0.7  m/s constant step length'
%         2) '0.7  m/s preferred'
%         3) '0.7  m/s constant step frequency'
%         4) '0.9  m/s constant step length'
%         5) '0.9  m/s preferred'
%         6) '0.9  m/s constant step frequency'
%         7) '1.1  m/s constant step length'
%         8) '1.1  m/s preferred'
%         9) '1.1  m/s constant step frequency'
%        10) '1.6  m/s constant step frequency'
%        11) '1.6  m/s preferred'
%        12) '1.6  m/s constant step length'
%        13) '1.8  m/s constant step frequency'
%        14) '1.8  m/s preferred'
%        15) '1.8  m/s constant step length'
%        16) '2.0  m/s preferred'
%        17) '1.25 m/s lowest step frequency'
%        18) '1.25 m/s lower step frequency'
%        19) '1.25 m/s low step frequency'
%        20) '1.25 m/s preferred'
%        21) '1.25 m/s preferred'
%        22) '1.25 m/s preferred'
%        23) '1.25 m/s high step frequency'
%        24) '1.25 m/s higher step frequency'
%        25) '1.25 m/s highest step frequency'
%        26) '1.25 m/s zero step width'
%        27) '1.25 m/s 10 cm step width'
%        28) '1.25 m/s 20 cm step width'
%        29) '1.25 m/s 30 cm step width'
%        30) '1.25 m/s 40 cm step width'
%        31) '1.40 m/s constant step length'
%        32) '1.40 m/s preferred'
%        33) '1.40 m/s constant step frequency'
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-01-2023, Munich
% Last modified:    23-01-2023, Munich 
% 
% -------------------------------------------------------------------------
%% References
% #########################################################################
% [1] van der Zee, Tim J.; Mundinger, Emily M.; Kuo, Arthur D. (2022): A 
%     biomechanics dataset of healthy human walking at various speeds, step 
%     lengths and step widths. In: Sci Data 9 (1), S. 704. 
%     DOI: 10.1038/s41597-022-01817-1.
% #########################################################################


% --------------------------------- Look up applicable trials and variables
[variablename, typename, trials] = lookup_variable_name_vdZee(variable, ...
                                          options.leg, joint, options.exp);
                                      
% ---------------- Extract step of selected data set and return time vector
[time, subjmean, stridetime, DS] = extract_data_vdZee(typename, variablename,...
                                                    trials, options.plane); 

% --------------------------------------------- Select trial to be analysed
trialnames = lookup_trial_names_vdZee(trials);
trialindex = contains(trialnames,speed); 

% ----------------------------------- Create human output data for plotting
if contains(variablename,'GRFs')
    human_data = [time, subjmean(:,trialindex,1), subjmean(:,trialindex,2), subjmean(:,trialindex,3)];
else
    human_data = [time, subjmean(:,trialindex)];
end

% ----------------------------------------------- Create stride time output
st_out = stridetime(trialindex);
DS_out = DS(:,trialindex);
end

