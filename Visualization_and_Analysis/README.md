# Visualization and Analysis 
This folder provides tools for analyzing and visualizing the NMS. You can explore one simulation at a time or compare multiple simulation outputs within a single plot.

## Model Output
By simulating the *EcoWalker* model with ```simout = sim('nms_model2G_EcoWalk')``` we create an ```simout```output struct
- The model output data are defined in **Visualization & Analysis** in ```nms_model2G_EcoWalk.slx``` using [Matlab Simulink To Workspace][6] blocks
- You can check out the contents of the ```simout``` struct by simply calling ```simout``` in the Matlab command window
- Please do the same for each struct entry to check its contents, e.g., ```simout.ACT```
- All data are recorded with a fixed sampling frequency ```Fs_out``` defined in ```nms_model_MechInit_AddOns.m```
- Kinematics and dynamics data for all joints and muscle properties are from the left foot 

**Recommendation: Before creating new signal output tags, check the existing outputs for the necessary information!**


## Basic Visualization with 'Standard Plots'
1. The first step is to create the output files you want to visualize. Follow the instructions for *Running the Model* in the README of the parent directory. After running the model, you should see a ```\Simulation_Results\Case1``` folder in your local directory containing ```Result_1.mat```. 
2. Open ```main_plot.m``` and press the run button. 
3. The output are several plots showing joint kinematics, joint torques, joint powers, muscle activation patterns, ground reaction forces, center of pressure, foot kinematics, and work 


## Visualization with 'Figures_Paper'
1. Open 'main_plot_paper.m' and press play to create the plots used in the paper. Before every plot, a prompt will ask which human data you want to compare to. Always press ```1``` to get the same result as in the paper. 
2. You will find three more scripts to create tables S1, S2, and S4. Press play to create and save the tables as '.tex' files.


## File and Folder Structure 
```data_preprocess```: limit data to one stride and filter GRFs if filter = true  
```del_sample```: remove samples from the time series object  
```save_plot```: save plots in PDF and .fig format  
```write_video```: write video from animated plots   


**Energy_and_Power/**  
Energy and power analysis for all joints and global model measures with automatic LaTeX table generation.
Workflow in ```main_energy.m```: 
1. ```data_preprocess```: simout must be preprocessed before the data analysis
2. ```tab_gait_measures```: takes one or multiple simulation inputs and returns a table with global gait measures such as forward velocity, CoT, and more - for details, see ```help tab_gait_measures```
3. ```table2latex```: exports the tables to LaTeX format

The ```create_humanout.m``` and ```add_humanout.m``` functions create a struct similar to the Simulink simulation output for human data and integrate the humanout dataset into the simout set for bundled data processing in the table functions.


**Human_Data/**   
Human reference data used for plotting - see folders README for details.


**Standard_Figures/**  
Standard biomechanical figures are used to analyze the neuromuscular model. To create your first plot, follow the instructions in [# Basic Visualization with 'Standard Plots'](#Basic-Visualization-with-'Standard Plots'). Muscle activation or range of motion plots are inspired by *Geyer 2010* [1].

The standard plots include:
1. Joint Kinematics *(ankle, knee, and hip)*
2. Joint Torques *(ankle, knee, and hip)*
3. Joint Power and Work *(ankle, knee, and hip)*
4. Muscle activation patterns *(GAS, SOL, TA, VAS, HAM, GLU, HFL)*
5. Ground Reaction Forces (GRFs) and Center of Pressure (CoP)
6. Foot Kinematics of Toe Joint and Midtarsal Joint (if applicable)
7. Foot Arch Power and Work (if applicable)
8. Energy Dissipation of the Foot Models (bar plot)

The functions support a variety of options, such as plotting in TUM colors, changing font size and line width, normalizing some plots to body weight and leg length, or focusing on specific regions in a smaller subplot inside the axis. See ```help def_figures``` for details or contact alexandra.buchmann@tum.de. 


**Table_to_LaTeX/**  
The table2latex(T, filename) function formats a MATLAB table (T) into a .tex file (specified by the filename path). Please avoid using cells or structs inside the table. - Víctor Martínez-Cagigal (2022). MATLAB Table to LaTeX Converter (https://www.mathworks.com/matlabcentral/fileexchange/69063-matlab-table-to-latex-conversor), MATLAB Central File Exchange. Retrieved December 15, 2022. 


## References: 
[1]: H. Geyer and H. Herr, "A Muscle-Reflex Model That Encodes Principles of Legged Mechanics Produces Human Walking Dynamics and Muscle Activities," in IEEE Transactions on Neural Systems and Rehabilitation Engineering, vol. 18, no. 3, pp. 263-273, June 2010, doi: 10.1109/TNSRE.2010.2047592 

[2]: van der Zee, Tim J.; Mundinger, Emily M.; Kuo, Arthur D. (2022): A biomechanics dataset of healthy human walking at various speeds, step lengths and step widths. In: Sci Data 9 (1), S. 704. DOI: 10.1038/s41597-022-01817-1.

[6]: <https://de.mathworks.com/help/simulink/slref/toworkspace.html> "Matlab Simulink To Workspace"

[7]: <https://sharelatex.tum.de/read/tmycszhprccw > "Passivity Report"

[8]: <https://de.mathworks.com/help/optim/ug/fmincon.html> "Matlab fmincon"



