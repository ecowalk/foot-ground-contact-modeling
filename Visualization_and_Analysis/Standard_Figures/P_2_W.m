function [W] = P_2_W(P)
% -------------------------------------------------------------------------
% [W] = P_2_W(P) power to work calculates power from work using the trapez 
% rule for integration
% 
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    12-12-2023, Munich by Alexandra Buchmann
% 
% -------------------------------------------------------------------------

W = zeros(length(P),1);

for i = 1:length(P)
    W(i) = trapz(P(1:i));
end

end

