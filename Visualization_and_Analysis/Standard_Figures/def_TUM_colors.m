function [TUMColors,TUMColormap] = def_TUM_colors()
% -------------------------------------------------------------------------
% [TUMColors,TUMColormap] = def_TUM_colors() 
%
% defines the a colororder and colorbar in TUM cooperate design colors 
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

TUMColors    = [127 127 127             % TUMGray2
                0 101 189               % TUMBlue
                227 114 34              % TUMOrange
                162 173 0               % TUMGreen  
                0 51 89                 % TUMBlue1
                218 215 203             % TUMIvory 
                51 51 51                % TUMGray1
                0 82 147                % TUMBlue2 
                204 204 204             % TUMGray3    
                100 160 200             % TUMBlue3    
                152 198 234].*(1/255);  % TUMBlue4    

TUMColormap  = [227 114 34              % TUMOrange
                162 173 0               % TUMGreen  
                0 101 189               % TUMBlue
                127 127 127             % TUMGray2
                0   0   0].*(1/255);    % Black
end

