function [fig_prop] = def_fig_prop(fig_name, ...
                                   var_names, ...
                                   fig_save_name,... 
                                   ylabels,... 
                                   ylim_ax,...
                                   norm_vars,...                           
                                   annots,...
                                   legend_type,...
                                   fig_size,...
                                   line_type,...
                                   axes_intercept,...
                                   lines_per_ax,...
                                   line_names_ax,...
                                   plot_DS,...
                                   plot_xlines,...
                                   line_style,...
                                   xlabels,... 
                                   xlim_ax)
arguments
        fig_name        (1,:) char
        var_names       (1,:) cell
        fig_save_name   (1,:) char
        ylabels         (1,:) cell
        ylim_ax         (:,2) double
        
        norm_vars       (1,1) struct  = struct()
        annots          (1,:) cell    = {'none'}
        legend_type     (1,:) char    = 'standard'
        fig_size        (1,4) double  = [0 0 21.0 29.7] %A4
        line_type       (1,:) cell    = {'plot'}; 
        axes_intercept  (1,:) char    = 'stride'
        lines_per_ax    (1,:) double  = 1; 
        line_names_ax         cell    = {};
        plot_DS         (1,:) logical = [true];
        plot_xlines     (1,:) logical = [true];
        line_style      (1,:) cell    = {'-'};  
        xlabels         (1,:) cell    = {};
        xlim_ax         (:,2) double  = [0 1];
end
% -------------------------------------------------------------------------
% [fig_prop] = def_fig_prop(fig_name, ...     (1,:)             char
%                           var_names, ...    (1, no_axes)      cell
%                           fig_save_name,... (1,:)             char
%                           ylabels,...       (1, no_axes)      cell
%                           ylim_ax,...       (no_axes,2)       double
%
%                           norm_vars,...     (1,1)             struct = struct()
%                           annots,...        (1, no_axes)      cell   = {'none'}   
%                           legend_type,...   (1,:)             char   = 'standard'
%                           fig_size,...      (1,4)             double = A4
%                           line_type,...     (1, no_axes)      cell   = {'plot'}
%                           axes_intercept,...(1,:)             char   ='stride'
%                           lines_per_ax,...  (1, no_axes)      double =  1
%                           line_names);      (1, lines_per_ax) cell   = {}
% 
% defines struct with all figure properties needed for plotting.
%
% Options and details: 
%              - norm_vars:      see help normalize_bw
%              - annots:         see help show_annots
%              - legend_type:    see help show_legend
%              - fig_size:       [left bottom width height]       
%              - line_type:      see help plot_axis
%              - axes_intercept: see help link_x_axes
%              - lines_per_ax:   see help plot_axis
%              - line_names:     see help name_lines
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------
   
%% Normalize to Body Weight
fig_prop.name           = fig_name; 
fig_prop.ax_title       = var_names;
fig_prop.save_name      = fig_save_name;
fig_prop.ylabel         = ylabels;
fig_prop.xlabel         = xlabels;
fig_prop.annots         = annots; 
fig_prop.ylim_ax        = ylim_ax; 
fig_prop.xlim_ax        = xlim_ax; 
fig_prop.norm_vars      = norm_vars;

fig_prop.legend_type    = legend_type;
fig_prop.format         = fig_size;
fig_prop.line_type      = line_type;
fig_prop.axes_intercept = axes_intercept;
fig_prop.lines_per_ax   = lines_per_ax; 
fig_prop.line_names_ax  = line_names_ax;    
fig_prop.plot_DS        = plot_DS;
fig_prop.plot_xlines    = plot_xlines;
fig_prop.line_style     = line_style;


end

