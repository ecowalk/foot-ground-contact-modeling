function [] = def_figures(simout, options)
arguments 
    simout              (:,1) Simulink.SimulationOutput

    options.plots       (1,:) double      = [1,2,3,4,5,6,7];
    options.fpath       (1,:) char        = ''
    options.save_name   (1,:) char        = ''
    options.save_flag   (1,1) logical     = 0;
    
    options.TUMColors   (1,1) double      = 2;
    options.LineWidth   (1,1) double      = 2; 
    options.FontSize    (1,1) double      = 10;
    options.patch_focus (1,1) logical     = 1;
    options.norm_to_bw  (1,1) logical     = 1;
end
% -------------------------------------------------------------------------
% [] = def_figures(simout, options)
%
% defines plot layout to visualize results from neuromuscular simulation
%
% Simualtion output data have to be preprocessed with data_PreProcess.m
% 
%             simout      (:,1) Simulink.SimulationOutput
%
%     options.plots       (1,:) double = [1,2,3,4,5,6,7,8];
%   
%           * 1 - Joint Kinematics
%           * 2 - Joint Torque
%           * 3 - Joint Power and Work
%           * 4 - Muscle ACT and Joint Angle
%           * 5 - Ground Reaction Forces (GRFs) and Center of Pressure(CoP)
%           * 6 - Foot Kinematics (TJ and MTJ)
%           * 7 - Foot Arch Power and Work
%           * 8 - Energy Foot Bar Plot
% 
%
%     options.fpath       (1,:) char    = ''  path to save figures
%     options.save_name   (1,:) char    = ''  plot name when saving
%     options.save_flag   (1,1) logical = 0;  
%    
%     options.TUMColors   (1,1) logical = 1;  set colors to TUMColors
%     options.LineWidth   (1,1) double  = 2;  LineWidth for plots
%     options.FontSize    (1,1) double  = 10;
%     options.patch_focus (1,1) logical = 1;  call help plot_tiledlayout
%     options.norm_to_bw  (1,1) logical = 1;  normalize data to %bw(*ll)
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       20-08-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Prepare Simulation Data
[simout]     = simout_rad2deg(simout); 
[norm_vars]  = norm_to_bw(options);
A4_h         = [1 1 15.4 20];

%% Load Human Data 
[human_data] = load_human_data(options.plots);
plot_human = {};

%% Plot Kinematics --------------------------------------------------------
if any(options.plots == 1)
    [fig_prop] = def_fig_prop('Range of Motion',...                        % fig_name
                 {},...                                                    % var_names
                 'RoM_All_Joints_',...                                     % fig_save_name
                 {'$\varphi_{ankle}$ [deg]','$\varphi_{knee}$ [deg]','$\varphi_{hip}$ [deg]'},... % ylabels
                 [-42 20; -5 85; -20 50],...                               % ylim_ax
                 norm_vars,...                                             % norm vars
                 {'Ankle', 'HipKnee', 'HipKnee'},...                       % annots
                  'standard');                                             % legend_type 
                 fig_prop.lines_per_ax = 1;
                 fig_prop.format = A4_h;
              

    plot_human = [{'AnkleRoM(:,1)*(1/100)','AnkleRoM(:,2)'};...
                  {'KneeRoM(:,1)*(1/100)' ,'KneeRoM(:,2)' };...
                  {'HipRoM(:,1)*(1/100)'  ,'HipRoM(:,2)'  }];
for i = 1:length(simout)
    
    plot_sim{i}  = [{simout(i).Dyn.Time, simout(i).Dyn.Data(:,1)};...
                    {simout(i).Dyn.Time, simout(i).Dyn.Data(:,3)};...
                    {simout(i).Dyn.Time, simout(i).Dyn.Data(:,5)}];
end


    plot_tiledlayout(fig_prop, options, simout, plot_sim, human_data, plot_human)                                    
end


%% Joint Torques ----------------------------------------------------------
if any(options.plots == 2)
    
if options.norm_to_bw % -----------------------------------Normalized to bw 
unit = ' [Nm/kg]';
else
unit = ' [Nm]';
end

[fig_prop] = def_fig_prop('Joint Torques',...                              % fig_name
             {},...                                                        % var_names
             'Joint_Torques_',...                                          % fig_save_name
             {['$T_{ankle}$' unit],['$T_{knee}$' unit],['$T_{hip}$' unit]},... % ylabels
             [-20 160; -80 70; -100 150]/norm_vars.m_bw,...                % ylim_ax
             norm_vars,...                                                 % norm vars
             {'Ankle', 'HipKnee', 'HipKnee'},...                           % annots
              'standard');                                                 % legend_type            
                                     
fig_prop.format = A4_h;

plot_human = [{'AnkleTorque(:,1)*(1/100)',...
                       'AnkleTorque(:,2)*fig_prop.norm_vars.m_bw_de_hum'};...
              {'KneeTorque(:,1)*(1/100)',...
                        'KneeTorque(:,2)*fig_prop.norm_vars.m_bw_de_hum'};...
              {'HipTorque(:,1)*(1/100)',...
                        'HipTorque(:,2)*fig_prop.norm_vars.m_bw_de_hum'}];
                    
for i = 1:length(simout)
    time = simout(i).P_T.Time;    
    plot_sim{i}  = [{time, simout(i).P_T.Data(:,4)/fig_prop.norm_vars.m_bw};...
                    {time, simout(i).P_T.Data(:,5)/fig_prop.norm_vars.m_bw};...
                    {time, simout(i).P_T.Data(:,6)/fig_prop.norm_vars.m_bw}];
end
plot_tiledlayout(fig_prop, options, simout, plot_sim, human_data, plot_human)                                     
end 

%% Joint Power
if any(options.plots == 3)
    
if options.norm_to_bw % -----------------------------------Normalized to bw 
  unit = ' [W/kg]';
                   
    else 
  unit = ' [W]';
    
end
    
[fig_prop] = def_fig_prop('Joint Power',...                                % fig_name
             {},...                                                        % var_names
             'Joint_Power_',...                                            % fig_save_name
             {['$P_{ankle}$' unit],['$P_{knee}$' unit],['$P_{hip}$' unit]},... % ylabels
             [-1 3.7; -2 2; -3 3]*norm_vars.m_bw_de,...                    % ylim_ax
             norm_vars,...                                                 % norm vars
             {'Ankle', 'HipKnee', 'HipKnee'},...                           % annots
              'standard');                                                 % legend_type 
             fig_prop.format = A4_h;

plot_human = [{'AnklePower(:,1)*(1/100)','AnklePower(:,2)*fig_prop.norm_vars.m_bw_de_hum'};...
              {'KneePower(:,1)*(1/100)' ,'KneePower(:,2)*fig_prop.norm_vars.m_bw_de_hum '};...
              {'HipPower(:,1)*(1/100)'  ,'HipPower(:,2)*fig_prop.norm_vars.m_bw_de_hum  '}];
            
for i = 1:length(simout)
    time = simout(i).P_T.Time;    
    plot_sim{i}  = [{time, simout(i).P_T.Data(:,1)/fig_prop.norm_vars.m_bw};...
                    {time, simout(i).P_T.Data(:,2)/fig_prop.norm_vars.m_bw};...
                    {time, simout(i).P_T.Data(:,3)/fig_prop.norm_vars.m_bw}];
end

plot_tiledlayout(fig_prop, options, simout,plot_sim, human_data, plot_human)                           
end

%% Ankle Joint Power and Work
if any(options.plots == 3)
if options.norm_to_bw % -----------------------------------Normalized to bw 
  unit1 = ' [W/kg]';
  unit2 = ' [J/kg]';
                   
else 
  unit1 = ' [W]';
  unit2 = ' [J]';
end
    
[fig_prop] = def_fig_prop('Ankle Power and Work',...                       % fig_name
             {},...                                                        % var_names
             'Ankle_Power_Work_',...                                       % fig_save_name
             {['$P_{ankle}$' unit1],['$W_{ankle}$' unit2]},...             % ylabels
             [-1 3.7; -0.25 0.15]*norm_vars.m_bw_de,...                    % ylim_ax
             norm_vars,...                                                 % norm vars
             {'Ankle', 'Ankle'},...                                        % Annots
              'standard');                                                 % Legend_type 
             fig_prop.format = [0 0   15.4 12.5  ];

human_data.AnkleWork = [ human_data.AnklePower(:,1)*(1/100),...
    0.0060*P_2_W(human_data.AnklePower(:,2)*fig_prop.norm_vars.m_bw_de_hum)];

plot_human = [{'AnklePower(:,1)*(1/100)','AnklePower(:,2)*fig_prop.norm_vars.m_bw_de_hum'};...
              {'AnkleWork(:,1)','AnkleWork(:,2)'}];
            
for i = 1:length(simout)
    time = simout(i).P_T.Time;    
    step = simout(i).stride_time/(length(time)-1);
    plot_sim{i}  = [{time, simout(i).P_T.Data(:,1)/fig_prop.norm_vars.m_bw};...
                    {time, P_2_W(simout(i).P_T.Data(:,1))*step/fig_prop.norm_vars.m_bw}];
end

plot_tiledlayout(fig_prop, options, simout,plot_sim, human_data, plot_human)                            
end


%% Joint Work
if any(options.plots == 3)
if options.norm_to_bw % -----------------------------------Normalized to bw 
  unit = ' [J/kg]';
                   
    else 
  unit = ' [J]';
    
end
    
[fig_prop] = def_fig_prop('Joint Power',...                                % fig_name
             {},...                                                        % var_names
             'Joint_Work_',...                                             % fig_save_name
             {['$W_{ankle}$' unit],['$W_{knee}$' unit],['$W_{hip}$' unit]},... % ylabels
             [-0.25 0.15; -0.4 0.1; -0.15 0.5]*norm_vars.m_bw_de,...       % ylim_ax
             norm_vars,...                                                 % norm vars
             {'Ankle', 'HipKnee', 'HipKnee'},...                           % annots
              'standard');                                                 % legend_type 
             
          fig_prop.format = A4_h;
          
human_data.AnkleWork = [ human_data.AnklePower(:,1)*(1/100),...
    0.0060*P_2_W(human_data.AnklePower(:,2)*fig_prop.norm_vars.m_bw_de_hum)];
human_data.KneeWork = [ human_data.KneePower(:,1)*(1/100),...
    0.0060*P_2_W(human_data.KneePower(:,2)*fig_prop.norm_vars.m_bw_de_hum)];
human_data.HipWork = [ human_data.HipPower(:,1)*(1/100),...
    0.0060*P_2_W(human_data.HipPower(:,2)*fig_prop.norm_vars.m_bw_de_hum)];

plot_human = [{'AnkleWork(:,1)','AnkleWork(:,2)'};...
              {'KneeWork(:,1)','KneeWork(:,2)'};...
              {'HipWork(:,1)','HipWork(:,2)'}];
            
for i = 1:length(simout)
    time = simout(i).P_T.Time; 
    step = simout(i).stride_time/(length(time)-1);
       
    plot_sim{i}  = [{time, P_2_W(simout(i).P_T.Data(:,1))*step/fig_prop.norm_vars.m_bw};...
                    {time, P_2_W(simout(i).P_T.Data(:,2))*step/fig_prop.norm_vars.m_bw};...
                    {time, P_2_W(simout(i).P_T.Data(:,3))*step/fig_prop.norm_vars.m_bw}];
end

plot_tiledlayout(fig_prop, options, simout,plot_sim, human_data, plot_human)                                    
end

%% Muscle Activation Patterns - Plots
if any(options.plots == 4)
% out.Muscle_Activations:   [HFL GLU HAM VAS GAS SOL TA]
%                           [ 1   2   3   4   5   6   7]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Hip Muscles - Activation and Joint Angle --------------------------------
[fig_prop] = def_fig_prop('Muscle Activations Hip',...                     % fig_name
             {},...                                                        % var_names
             'MA_Hip_',...                                                 % fig_save_name
             {'$ACT_{HFL}$ $[\%\,MMT]$','$ACT_{GLU}$ $[\%\,MMT]$'},...     % ylabels
             [0 45; 0 45],...                                              % ylim_ax
             norm_vars,...                                                 % norm vars
             {'none', 'none', 'HipKnee'},...                               % annots
              'standard',...                                               % legend_type 
              A4_h,...                                                     % fig_size
              {'plot','plot'});                                            % line_type            

plot_human = [{'ACTHFLal(:,1)*(1/100)'  ,'ACTHFLal(:,2)' };...
              {'ACTGLUmax(:,1)*(1/100)' ,'ACTGLUmax(:,2)'}];
                     
for i = 1:length(simout)
    time = simout(i).ACT.Time;    
    plot_sim{i}  = [{time, simout(i).ACT.Data(:,1)*100};...
                    {time, simout(i).ACT.Data(:,2)*100}];
end

plot_tiledlayout(fig_prop, options, simout, plot_sim, human_data, plot_human)  
                           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Knee Muscles - Activation and Joint Angle -------------------------------
[fig_prop] = def_fig_prop('Muscle Activations Knee',...                    % fig_name
             {},...                                                        % var_names
             'MA_Knee_',...                                                % fig_save_name
             {'$ACT_{HAM}$ $[\%\,MMT]$','$ACT_{VAS}$ $[\%\,MMT]$'},...     % ylabels
             [0 70; 0 35],...                                              % ylim_ax
             norm_vars,...                                                 % norm vars
             {'none', 'none'},...                                          % annots
              'standard',...                                               % legend_type 
              A4_h,...                                                     % fig_size
              {'plot','plot'});                                            % line_type            

plot_human = [{'ACTHAMsb(:,1)*(1/100)'  ,'ACTHAMsb(:,2)' };...
              {'ACTVASlat(:,1)*(1/100)' ,'ACTVASlat(:,2)'}];
          
for i = 1:length(simout)
    time = simout(i).ACT.Time;    
    plot_sim{i}  = [{time, simout(i).ACT.Data(:,3)*100};...
                    {time, simout(i).ACT.Data(:,4)*100}];
end                     

plot_tiledlayout(fig_prop, options, simout, plot_sim, human_data, plot_human)  
                           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ankle Muscles  - Activation and Joint Angle -----------------------------
[fig_prop] = def_fig_prop('Muscle Activations Ankle',...                   % fig_name
             {},...                                                        % var_names
             'MA_Ankle_',...                                               % fig_save_name
             {'$ACT_{GAS}$ $[\%\,MMT]$','$ACT_{SOL}$ $[\%\,MMT]$','$ACT_{TA}$ ,$[\%\,MMT]$'},...  % ylabels
             [0 100; 0 100; 0 70],...                                      % ylim_ax
             norm_vars,...                                                 % norm vars
             {'none', 'none', 'none'},...                                  % annots
              'standard',...                                               % legend_type 
              A4_h,...                                                     % fig_size
              {'plot','plot','plot'});                                     % line_type            

    
plot_human = [{'ACTGAS(:,1)*(1/100)'  ,'ACTGAS(:,2)'  };...
              {'ACTSOL(:,1)*(1/100)'  ,'ACTSOL(:,2)'  };...
              {'ACTTA(:,1)*(1/100)'   ,'ACTTA(:,2)'   }];

for i = 1:length(simout)
    time = simout(i).ACT.Time;    
    plot_sim{i}  = [{time, simout(i).ACT.Data(:,5)*100};...
                    {time, simout(i).ACT.Data(:,6)*100};...
                    {time, simout(i).ACT.Data(:,7)*100}];
end

plot_tiledlayout(fig_prop, options, simout, plot_sim, human_data, plot_human)                          
end



%% Ground Reaction Forces F_v (normalized to %bw*g) Left Leg
if any(options.plots == 5)
    
if options.norm_to_bw
unit = '[\%bw]';
else
unit = '[N]';
end

[fig_prop] = def_fig_prop('Ground Reaction Forces',...                     % fig_name
                   {},...                                                  % var_names
                    'GRFs_',...                                            % fig_save_name
                   {['$F_v$' unit], ['$F_h$' unit]},...                    % ylabels
                   [0 1300; -300 200]/norm_vars.GRFs,...                   % ylim_ax
                   norm_vars,...                                           % norm vars
                   {'none'},...                                            % annots
                    'standard',...                                         % legend_type 
                     A4_h,...                                              % fig_size
                   {'plot','plot'},...                                     % line_type
                    'stride',...                                           % axes_intercept
                      1);                                                  % lines_per_ax 

fig_prop.line_style = {'-'};
fig_prop.xlim_ax = [0 0.65];
 fig_prop.format = [0 0 15.4 12.5  ];
plot_human = [{'GRFvertical(:,1)*(1/100)', 'GRFvertical(:,2)/fig_prop.norm_vars.GRFs_hum'};...
              {'GRFprog(:,1)*(1/100)',     'GRFprog(:,2)*(-1)/fig_prop.norm_vars.GRFs_hum'}];
                     
for i = 1:length(simout)
    time = simout(i).GRFs.Time;    
    plot_sim{i}  = [{time, simout(i).GRFs.Data(:,2)/fig_prop.norm_vars.GRFs};...
                    {time, simout(i).GRFs.Data(:,1)/fig_prop.norm_vars.GRFs}];
end

plot_tiledlayout(fig_prop, options, simout,     plot_sim, ...
                                    human_data, plot_human)  
end

%% COP
if any(options.plots == 5)

ylabel = 'COP$ [m]';

[fig_prop] = def_fig_prop('Ground Reaction Forces',...  % fig_name
                   {},...                               % var_names
                    'COP_',...                          % fig_save_name
                   {ylabel},...                         % ylabels
                   [0 0.28],...                         % ylim_ax
                   norm_vars,...                        % norm vars
                   {'none'},...                         % annots
                    'nohuman',...                       % legend_type 
                     A4_h,...                           % fig_size
                   {'plot'},...                         % line_type
                    'stride',...                        % axes_intercept
                      1);                               % lines_per_ax 
fig_prop.format = [0 0   15 7.5  ];
fig_prop.line_style = {'-'};
fig_prop.xlim_ax = [0 0.65];
 
% plot_human = [{'COP(:,1)*(1/100)',     'COP(:,2)'}];
  plot_human={};                   
for i = 1:length(simout)
    time = simout(i).COP.Time;    
    plot_sim{i}  = [{time, simout(i).COP.Data(:,2)-simout(i).COP.Data(2,2)}];
end

plot_tiledlayout(fig_prop, options, simout,     plot_sim, ...
                                    human_data, plot_human)  
end

%% Foot ROM
if any(options.plots == 6)
    [fig_prop] = def_fig_prop('Range of Motion Foot',...                   % fig_name
                 {},...                                                    % var_names
                 'RoM_All_Foot_',...                                       % fig_save_name
                 {'$\varphi_{MTJ}$ [deg]','$\varphi_{TJ}$ [deg]'},...      % ylabels
                 [ -10 5; -5 65],...                                       % ylim_ax
                 norm_vars,...                                             % norm vars
                 {'none','none'},...                                       % annots
                  'reduced');                                              % legend_type 
              fig_prop.lines_per_ax = 1;

  fig_prop.format = [0 0   15.4 12.5  ];
  plot_human={};
for i = 1:length(simout)
    
    plot_sim{i} = [{simout(i).Dyn.Time, simout(i).Foot_Kinematics_L.MTJ_phi.Data};...
                   {simout(i).Dyn.Time, simout(i).Foot_Kinematics_L.MTPJ_phi.Data};...
                   ];
end
    plot_tiledlayout(fig_prop, options, simout, plot_sim, human_data, plot_human)                                     
end

%% Foot Arch Power and Work
if any(options.plots == 7)
if options.norm_to_bw
    unit1 = ' [W/kg]';
    unit2 = ' [J/kg]';
else
    unit1 = ' [W]';
  unit2 = ' [J]';
end
    [fig_prop] = def_fig_prop('Power Foot',...                             % fig_name
                 {},...                                                    % var_names
                 'Power_work_arch_',...                                    % fig_save_name
                 {['$P_{internal}$' unit1],['$W_{internal}$' unit2]},...   % ylabels
                 [-1 1 ; -0.08 0.04]*norm_vars.m_bw_de,...                 % ylim_ax
                 norm_vars,...                                             % norm vars
                 {'none'},...                                              % annots
                  'double');                                               % legend_type 
              
fig_prop.lines_per_ax = [3,3];
fig_prop.line_style = [{'-','--','-.'};{'-','--','-.'}];
fig_prop.format = [1 1 15.4 12.5];
fig_prop.xlim_ax = [0 0.65];

plot_human={};
for i = 1:length(simout)
    time = simout(i).Foot_arch_L.PA_power.Time;
    simout(i).Foot_arch_L.MTPJ_energy = simout(i).Foot_arch_L.MTPJ_energy - simout(i).Foot_arch_L.MTPJ_energy.Data(1);
    simout(i).Foot_arch_L.PA_energy = simout(i).Foot_arch_L.PA_energy - simout(i).Foot_arch_L.PA_energy.Data(1);
    simout(i).Foot_arch_L.PL_energy = simout(i).Foot_arch_L.PL_energy - simout(i).Foot_arch_L.PL_energy.Data(1);
    plot_sim{i}  = [{time, (simout(i).Foot_arch_L.MTPJ_power.Data +...
                            simout(i).Foot_arch_L.PA_power.Data +...
                            simout(i).Foot_arch_L.PL_power.Data)./fig_prop.norm_vars.m_bw};...
                    {time, simout(i).Foot_arch_L.MTPJ_power.Data./fig_prop.norm_vars.m_bw};...
                    {time, (simout(i).Foot_arch_L.PA_power.Data +...
                            simout(i).Foot_arch_L.PL_power.Data)./fig_prop.norm_vars.m_bw};...
                    {time, (simout(i).Foot_arch_L.MTPJ_energy.Data+...
                                simout(i).Foot_arch_L.PA_energy.Data+...
                                simout(i).Foot_arch_L.PL_energy.Data)./fig_prop.norm_vars.m_bw};...
                    {time, (simout(i).Foot_arch_L.MTPJ_energy.Data)./fig_prop.norm_vars.m_bw};...
                    {time, (simout(i).Foot_arch_L.PA_energy.Data+...
                                simout(i).Foot_arch_L.PL_energy.Data)./fig_prop.norm_vars.m_bw}];
end
    plot_tiledlayout(fig_prop, options, simout, plot_sim,human_data, plot_human)          

end


%% Energy Foot Bar
if any(options.plots == 8)
if options.norm_to_bw
    unit = ' [J/kg]';
else
    unit =  ' [J]';
end
    [fig_prop] = def_fig_prop('Energy',...          % fig_name
                 {},...                             % var_names
                 'Work_Foot_',...                   % fig_save_name
                 {['$W_{FGCM}$' unit]},...          % ylabels
                 [-0.2 0.0]*norm_vars.m_bw_de,...   % ylim_ax
                 norm_vars,...                      % norm vars
                 {'none'},...                       % annots
                  'standard');            
              
fig_prop.plot_DS = false;
fig_prop.plot_xlines = false;
fig_prop.lines_per_ax = 1;
fig_prop.line_style = {'-','-','-','-'};
fig_prop.line_type = {'bar','bar','bar','bar'};
fig_prop.bar_X = categorical({'Total','CE Heel','CE Ball','CE Toe', 'Internal'});
fig_prop.bar_X = reordercats(fig_prop.bar_X,{'Total','CE Heel','CE Ball','CE Toe', 'Internal'});
fig_prop.format = [0 0   15.4   7.5];

plot_human={};
for i = 1:length(simout)

    simout(i).Foot_arch_L.MTPJ_energy = simout(i).Foot_arch_L.MTPJ_energy - simout(i).Foot_arch_L.MTPJ_energy.Data(1);
    simout(i).Foot_arch_L.PA_energy = simout(i).Foot_arch_L.PA_energy - simout(i).Foot_arch_L.PA_energy.Data(1);
    simout(i).Foot_arch_L.PL_energy = simout(i).Foot_arch_L.PL_energy - simout(i).Foot_arch_L.PL_energy.Data(1);
    simout(i).Foot_CE_L.CE_heel_energy = simout(i).Foot_CE_L.CE_heel_energy - simout(i).Foot_CE_L.CE_heel_energy.Data(1);
    simout(i).Foot_CE_L.CE_ball_energy = simout(i).Foot_CE_L.CE_ball_energy - simout(i).Foot_CE_L.CE_ball_energy.Data(1);
    simout(i).Foot_CE_L.CE_toe_energy = simout(i).Foot_CE_L.CE_toe_energy - simout(i).Foot_CE_L.CE_toe_energy.Data(1);
    
    plot_sim{i}  = [{ sum(simout(i).Foot_arch_L.MTPJ_energy.Data(end)+...
                          simout(i).Foot_arch_L.PA_energy.Data(end)+...
                          simout(i).Foot_arch_L.PL_energy.Data(end)+...
                          simout(i).Foot_CE_L.CE_heel_energy.Data(end)+...
                          simout(i).Foot_CE_L.CE_ball_energy.Data(end)+...
                          simout(i).Foot_CE_L.CE_toe_energy.Data(end))./fig_prop.norm_vars.m_bw,...
                     sum(simout(i).Foot_CE_L.CE_heel_energy.Data(end))./fig_prop.norm_vars.m_bw,...
                     sum(simout(i).Foot_CE_L.CE_ball_energy.Data(end))./fig_prop.norm_vars.m_bw,...
                     sum(simout(i).Foot_CE_L.CE_toe_energy.Data(end))./fig_prop.norm_vars.m_bw,...
                     sum(simout(i).Foot_arch_L.MTPJ_energy.Data(end)+...
                         simout(i).Foot_arch_L.PA_energy.Data(end)+...
                         simout(i).Foot_arch_L.PL_energy.Data(end))./fig_prop.norm_vars.m_bw}];
end
    plot_tiledlayout(fig_prop, options, simout, plot_sim,human_data, plot_human)         
end
end

