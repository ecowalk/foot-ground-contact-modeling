function [] = link_x_axes (t, axes, axes_intercept)
arguments
    t               (1,1) matlab.graphics.layout.TiledChartLayout
    axes            (1,:) matlab.graphics.axis.Axes
    axes_intercept  (1,:) char {mustBeMember(axes_intercept,{'stride','stance','l_mtu'})}
end
% -------------------------------------------------------------------------
% [] = link_x_axes (t_no, axes, axes_intercept) 
%
% links all given axes of tiled layout t, stacks axes on top of grid lines 
% and limits axes to axes_intercept and labels x-axes according to the 
% defined axes_intercept case:
%                               * 'stride': 0-100      [\% stride]
%                               * 'stance': 0-60       [\% stride]
%                               * 'l_mtu':  auto-limit [l_MTU [m]]
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

linkaxes(axes,'x');

switch axes_intercept
    case 'stride'
        axes(1).XLim    = [0 1];
        x_tick_vector   = linspace(0, 1, 11);
        xticks(axes,x_tick_vector);
        xticklabels(axes,{'0','10','20','30','40','50',...
                                               '60','70','80','90','100'});
        axes_label      = '\% Stride';
        
        
    case 'stance'    
        axes(1).XLim    = [0 0.6];
        x_tick_vector   = linspace(0, 0.6, 7);
        xticks(axes,x_tick_vector);
        xticklabels(axes,{'0','10','20','30','40','50','60'});
        axes_label      = '\% Stride';
        
    case 'l_mtu'
        axes_label      = '$l_{MTU}$\,[m]';
        
    otherwise 
        error(['link_x_axes: invalid axes_intercept.'...
                'Supported options are ´stride´, ´stance´ and ´l_mtu´.'...
                'Check spelling or call help link_x_axes for more info!']);
end

  for i = 1:length(axes) % ------------------------------ Stack axes on top 
    axes(i).YGrid = 'on';
    axes(i).XGrid = 'on';
    axes(i).Layer = 'top';
    xlabel(t, axes_label, 'Interpreter', 'Latex')
  end
    
end