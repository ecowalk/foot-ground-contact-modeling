%% main_plot
% -------------------------------------------------------------------------
% main: standard plot outputs for neuromuscular simulation
%
%       1) Data PreProcessing: cropping to one step, normalizing to stride 
%                              time, filter GRF; see "help Data_PreProcess" 
%                              for more information)
% 
%       2) Standard plots: see 'help def_figures' for supported figures and
%                          options
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       25-11-2021, Munich
% Last modified:    14-12-2022, Munich 
% 
% -------------------------------------------------------------------------
%
%% References
%{ 
% #########################################################################
%
% Ankle Power -------------------------------------------------------------
% [1] Cronin, Neil J.; Prilutsky, Boris I.; Lichtwark, Glen A.; Maas, Huub 
%    (2013): Does ankle joint power reflect type of muscle action of soleus
%     and gastrocnemius during walking in cats and humans? In: Journal of 
%     Biomechanics 46 (7), S. 1383–1386. DOI: 10.1016/j.jbiomech.2013.02.023.
% 
% Ankle Power & Torque; Energy and Impulse --------------------------------
% [2] Lipfert, Susanne W.; Günther, Michael; Renjewski, Daniel; Seyfarth, 
%     André (2014): Impulsive ankle push-off powers leg swing in human 
%     walking. In: Journal of Experimental Biology 217 (8), S. 1218–1228. 
%     DOI: 10.1242/jeb.097345.
% 
% Joint Torques & Angles (RoM), GRFs, Muscle Activations ------------------
% [3] Perry, Jacquelin; Burnfield, Judith M.; Cabico, L. M. (2010): 
%     Gait Analysis. Normal and Pathological Function: SLACK.
% 
% Muscle Length-Time Relations for GAS & SOL ------------------------------
% [4] Ishikawa, Masaki; Komi, Paavo V.; Grey, Michael J.; Lepola, Vesa; 
%     Bruggemann, Gert-Peter (2005): Muscle-tendon interaction and elastic 
%     energy usage in human walking. In: Journal of Applied Physiology 
%     (Bethesda, Md. : 1985) 99 (2), S. 603–608. 
%     DOI: 10.1152/japplphysiol.00189.2005.
% 
% [5] Lichtwark, Glen A.; Wilson, Alan M. (2006): Interactions between the 
%     human gastrocnemius muscle and the Achilles tendon during incline, 
%     level and decline locomotion. In: The Journal of Experimental Biology 
%     209 (Pt 21), S. 4379–4388. DOI: 10.1242/jeb.02434.
%
% Human Gait Data ---------------------------------------------------------
% [6] van der Zee, Tim J.; Mundinger, Emily M.; Kuo, Arthur D. (2022): A 
%     biomechanics dataset of healthy human walking at various speeds, step 
%     lengths and step widths. In: Sci Data 9 (1), S. 704. 
%     DOI: 10.1038/s41597-022-01817-1.
%
% [7] Matlab Help: Resampling nonuniformly sampled signals
%     https://de.mathworks.com/help/signal/ug/resampling-nonuniformly-sampled-signals.html
%
% [8] https://de.wikipedia.org/wiki/Korrelation_(Signalverarbeitung)
% 
% [9] https://de.mathworks.com/help/matlab/ref/xcorr.html#mw_5302e782-55af-4f39-a990-a27d094ad3ea
% 
% Energetics of Swing and Stance ------------------------------------------
% [9] Umberger, Brian R. (2010): Stance and swing phase costs in human 
%     walking. In: J. R. Soc. Interface. 7 (50), S. 1329–1340. 
%     DOI: 10.1098/rsif.2010.0084.
% #########################################################################
%}
%
%% Init and Add Path ------------------------------------------------------
close all; clear; clc

stk = dbstack; filepath = which(stk(1).file);
local_path= filepath(1:strfind(filepath,'Visualization_and_Analysis')-1);

addpath ([local_path 'Visualization_and_Analysis']); 
addpath ([local_path 'Visualization_and_Analysis\Figures_Paper']); 
addpath ([local_path 'Visualization_and_Analysis\Human_Data']); 
addpath ([local_path 'Visualization_and_Analysis\Standard_Figures']); 

% Figure path and name
fpath_fig = fullfile(local_path, 'Visualization_and_Analysis');

%% Set Path to Working Directory (local)-----------------------------------
% 
fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_Opt_1000');
fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_Opt_1000');
fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_Opt_1000');
fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_Opt_1000');
fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_Opt_1000');
fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_Opt_1000');
fpath_ref_og  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1_og_geyer\Result_1');
res_var = 'Result_Opt_1000';
save_name_fig = 'Opt';
% 
% fpath_ref3w  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_windlass\Result_No_Opt_1000');
% fpath_ref2t  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_toe\Result_No_Opt_1000');
% fpath_ref2m  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_2seg_MTJ\Result_No_Opt_1000');
% fpath_ref3n  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_3seg_no\Result_No_Opt_1000');
% fpath_ref1h  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_high\Result_No_Opt_1000');
% fpath_ref1l  = fullfile(local_path, 'Visualization_and_Analysis\Figures_Paper\Case_1seg_low\Result_No_Opt_1000');
% res_var = 'Result_No_Opt_1000';
% save_name_fig = 'No_Opt';


%% Initialize Variables --------------------------------------------------- 
% Step Number to be evaluted (-1 for last full step)
step_no = -1;

% Data PreProcess
filter.flag   = 1; 
filter.Fpass  = 50;       % Fpass = passband frequency of lowpass filter [Hz];

%% Process Simulation Output ---------------------------------------------- 

[out1] = data_preprocess(fpath_ref1l, res_var, step_no, filter);
[out2] = data_preprocess(fpath_ref1h, res_var, step_no, filter);
[out4] = data_preprocess(fpath_ref2m, res_var, step_no, filter);
[out3] = data_preprocess(fpath_ref2t, res_var, step_no, filter);
[out5] = data_preprocess(fpath_ref3n, res_var, step_no, filter);
[out6] = data_preprocess(fpath_ref3w, res_var, step_no, filter);
[out7] = data_preprocess(fpath_ref_og, 'Result_1', step_no, filter);



%% Plotting --------------------------------------------------------------- 

save_Flag = 1;
simout=[out1 out2 out3 out4 out5 out6 out7];

set(groot,'defaultAxesTickLabelInterpreter','latex');  
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
set(groot,'defaultFigureunits','centimeters');
def_figures(simout,'fpath',fpath_fig, 'save_name',save_name_fig,...
                    'save_flag', save_Flag, 'plots',[1 2 3 4 5 6 7 8],...
                     'TUMColors',2, 'LineWidth',0.85, 'FontSize',10,...
                     'patch_focus',1,'norm_to_bw',1);
                 
