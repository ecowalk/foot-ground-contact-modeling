function [] = name_lines(simout, out_no, ax_name_no, line,  line_names_ax, ax_no, line_style_ax)
arguments 
    simout          (1,:) Simulink.SimulationOutput
    out_no          (1,1) double
    ax_name_no      
    line                 % Patchasd, area or line 
    line_names_ax         cell
    ax_no           (1,1) double
    line_style_ax cell
end
% -------------------------------------------------------------------------
% [] = name_lines(simout, out_no, lines,  line_names_ax, ax_no) 
%
% assignes name to line of a plot to be displayed in the legend 
% 
% Three options are avaliable: 
% 
%    1) no line names specified and only one line per axis: Sim1 (ST 1.21s)
%
%    2) no line names specified and two lines in per axis: Sim1 (ST 1.21s)
%                       only for first line, others not visible (GRF plots)
%
%    3) line names specified: names lines with 
%                                          line_names_ax(ax_no, ax_name_no)
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

% -- no line names specified, only one plot at a time: e.g. Sim1 (ST 1.21s)
if isempty(line_names_ax) %&& ax_name_no == 1 
    line.DisplayName  = ...
        sprintf('Sim %.0f (ST %.2fs)', out_no, simout(out_no).stride_time);
end

% ---------------- GRF plots: same color for horizontal and vertical forces
% if isempty(line_names_ax) && ax_name_no > 1 
%             cols = colororder;
%             current_color = find(line.Color(1) == cols(:,1) &...
%                                     line.Color(2) == cols(:,2) &...
%                                         line.Color(3) == cols(:,3));
%             line.HandleVisibility = 'off';
%             line.LineStyle = '--';
%             line.Color = cols(current_color-length(simout),:);
% end

% ------------------- multiple lines with specified line names in each axis 
if ~isempty(line_names_ax) 
line.DisplayName = char(line_names_ax(ax_no,ax_name_no));
end
if ~isempty(line_style_ax) && length(line_style_ax)>1
line.LineStyle = line_style_ax{ax_no,ax_name_no};
end

end
