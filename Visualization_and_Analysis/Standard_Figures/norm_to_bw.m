function [norm] = norm_to_bw(options)
% -------------------------------------------------------------------------
% [norm] = normalize_bw(options) 
% 
% set normalization parameters to normalize joint torques, joint power and 
% ground reaction force plots to body weight
% 
%         norm.m_bw
%         norm.m_bw_de
%         nomr.GRFs      m*g/100
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    03-01-2023, Munich 
% 
% -------------------------------------------------------------------------

    if options.norm_to_bw % --------------------------------normalize to bw 
              norm.m_bw_de = 1;           %[kg]
              norm.m_bw_de_hum = 1;           %[kg]
              norm.m_bw    = 80;          %[Nm/s^2]
              norm.m_bw_hum    = 74.9667;          %[Nm/s^2]
              norm.GRFs    = 80*9.81/100; %[%bw]
              norm.GRFs_hum    = 74.9667*9.81/100; %[%bw]

        else 
              norm.m_bw_de = 80; 
              norm.m_bw_de_hum = 74.9667;
              norm.m_bw    = 1; 
              norm.m_bw_hum    = 1; 
              norm.GRFs    = 1;
              norm.GRFs_hum    = 1;
              norm.mg      = 80*9.81;
    end
    
end

