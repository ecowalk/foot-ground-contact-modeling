function [] = patch_colormap(ax)
arguments
    ax  (1,1) matlab.graphics.axis.Axes
end
% -------------------------------------------------------------------------
% [] = patch_colormap(ax) 
%
% creates colormap for patch plots with labeling in \% stride
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

cbh                   = colorbar(ax);

cbh.Label.Interpreter = 'Latex';
cbh.Label.String      = {'\%\,Stride'};
cbh.Label.Position    = [1.4 1.075 0];
cbh.Label.Rotation    = 0;

cbh.Layout.Tile       = 'east';

cbh.Ticks             = linspace(0, 1, 11);
cbh.TickLabels        = {'0','10','20','30','40','50','60','70','80','90','100'}; 
end