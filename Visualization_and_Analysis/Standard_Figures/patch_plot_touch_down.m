function [] = patch_plot_touch_down (out, muscle_index,  ax_no, out_no)  
arguments
    out                 Simulink.SimulationOutput
    muscle_index (1,1)  double 
    ax_no        (1,1)  double
    out_no       (1,1)  double
end
% -------------------------------------------------------------------------
% [] = plot_touch_down_patch (out, muscle_index,  ax_no, out_no) 
%
% set markers for touch down, heel off, opposite heel touch down & toe off 
% in muscle force length patch plots
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------


if ~isempty(out.SimulationMetadata)
    % ---------------------------------------------------------- touch down  
    p_td = plot(out.L_MTU.Data(1,muscle_index),...
                                     out.F_MTU.Data(1,muscle_index), 'kv');
      
    % ------------------------------------------------------- heel take off 
    index_offh = find(out.L_MTU.Time >= out.heel_off_norm,1); 
    p_ho = plot(out.L_MTU.Data(index_offh,muscle_index),...
                             out.F_MTU.Data(index_offh,muscle_index),'bd');
                          
    % ------------------------------------------------ opp. heel touch down 
    index_opph = find(out.L_MTU.Time >= out.heel_opp_norm,1); 
    p_opp = plot(out.L_MTU.Data(index_opph,muscle_index),...
                             out.F_MTU.Data(index_opph,muscle_index),'go');
                           
    % ------------------------------------------------------------ take off 
    index_to = find(out.L_MTU.Time >= out.take_off_norm,1); 
    p_to = plot(out.L_MTU.Data(index_to,muscle_index),...
                               out.F_MTU.Data(index_to,muscle_index),'r^');
end
    
set([p_td, p_to], 'LineStyle', 'none', 'MarkerSize', 5);
 
    if (out_no == 1) && (ax_no == 1) % --------------- Show maker only once
            p_td.DisplayName      = 'Touch Down';
            p_ho.DisplayName      = 'Heel Off';
            p_opp.DisplayName     = 'Opp. Heel Touch Down';
            p_to.DisplayName      = 'Take Off';
            
        else 
           set([p_td, p_ho, p_opp, p_to], 'HandleVisibility', 'off'); 
    end
end