function [] = patch_zoom (simout, ax, scale, plot_commands_sim, ...
                                             new_ax_pos_shift, zoom_region)
arguments
    simout            (1,:) Simulink.SimulationOutput
    ax                      matlab.graphics.axis.Axes
    scale             (1,1) double 
    
    plot_commands_sim (:,2) cell
    new_ax_pos_shift  (1,2) double 
    zoom_region       (1,2) double 
end
% -------------------------------------------------------------------------
%[] = patch_zoom (simout, ax, scale, plot_commands_sim, ...
%                                            new_ax_pos_shift, zoom_region)
%
% creates a new axis inside exisiting axes to show a focus plots on zoom 
% region in \% stride
% 
%    - scale: scales size of new with [width height]/scale compared to
%                                                             existing axis
%    - new_ax_pos_shift: [left bottom] position shift of new axis inside 
%                                                            exisiting axis  
%    - zoom_region: region of zoom in % stride        
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

for ax_no = 1:length(ax)
    % ------------------------------------- Extract muscles index in simout  
    muscle_index = str2double(regexp(char(plot_commands_sim(ax_no,1)),...
                                                           '\d*','Match'));
                                                       
    % ------------------------ New axis position [left bottom width height]                                              
    pos = [(ax(ax_no).Position([1 2]) + new_ax_pos_shift)...
                                          ax(ax_no).Position([3 4])/scale];
    
    axes('position', pos); % -------- create new pair of axes inside figure
    title(['\textbf{Focus: ' num2str(zoom_region(1)) '-'...
             num2str(zoom_region(2)) '\%\,stride}'],'Interpreter','Latex');
    hold on
    c = colormap; % -------------- Get colors to encode start and end point 
    
     for out_no = 1:length(simout)    
        % ------------------------------------- region of interest for zoom
        RoI = find(simout(out_no).F_MTU.Time > zoom_region(1)/100 & ...
                   simout(out_no).F_MTU.Time < zoom_region(2)/100);

        % ---------------------------------------------- Start point marker
        plot(simout(out_no).L_MTU.Data(RoI(1),muscle_index),...
             simout(out_no).F_MTU.Data(RoI(1),muscle_index),...
             'Marker', '*', 'Color', c(3,:), 'HandleVisibility', 'off');

        % ---------------------------------------------- zoom line in black
        plot(simout(out_no).L_MTU.Data(RoI,muscle_index),...
             simout(out_no).F_MTU.Data(RoI,muscle_index),...
             'Color', 'k', 'HandleVisibility', 'off' );

        % ------------------------------------------------ End point marker
        plot(simout(out_no).L_MTU.Data(RoI(end),muscle_index),...
            simout(out_no).F_MTU.Data(RoI(end),muscle_index),...
            'Marker', '+', 'Color', c(4,:), 'HandleVisibility', 'off');
     end
    
    box on    
    grid on
    hold off
    axis padded
end

end