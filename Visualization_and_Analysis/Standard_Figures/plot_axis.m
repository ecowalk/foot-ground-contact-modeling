function [ax] = plot_axis(ax_no, fig_prop, options, simout,     plot_sim,...
                                                    human_data, plot_human)
arguments
    ax_no           (1,1)   double
    fig_prop        (1,1)   struct
    options         (1,1)   struct
    
    simout                  Simulink.SimulationOutput
    plot_sim                cell 
    human_data              struct  
    plot_human              cell    
end
% -------------------------------------------------------------------------
% [ax] = plot_axis(ax_no, fig_prop, options, simout,     plot_sim,...
%                                            human_data, plot_human)
%
% plots axis inside tiledlayout. Three different lines types are supported:
%               - 'plot':   line plot e.g. for RoM, Torques, Power
%               - 'area':   area plot e.g. for muscle activations
%               - 'patch':  color coded line for force length curves
% 
% show legend and annotations is called for each axis
%
% add on for GRF plots:
%       - yline at body weight is plotted 
%       - two human reference data for horizontal and vertical forces 
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

ax = nexttile; % ------------------------------------------ Create new axis
if ~isempty(fig_prop.ax_title)
title (char(fig_prop.ax_title(:,ax_no)),'Interpreter','Latex')
end
hold on

% ----------------------------- plot double support for representative sim1
if isfield(human_data,'DS') && fig_prop.plot_DS
[DS_patch_1, DS_patch_2] = ...
                plot_double_support_human (fig_prop.ylim_ax(ax_no,:), human_data.DS);
            
elseif ~isfield(human_data,'DS') && fig_prop.plot_DS
    [DS_patch_1, DS_patch_2] = ...
                plot_double_support (fig_prop.ylim_ax(ax_no,:), simout(1));
end   

ax.Box = 'on';
ax.Layer = 'top';
ax.XGrid = 'on';
ax.YGrid = 'on';

%% Plot Human Data (if avaliable)
if ~isempty(human_data) && ~isempty(plot_human)
    time_human = char(plot_human(ax_no,1));
    data_human = char(plot_human(ax_no,2));
    
  switch char(fig_prop.line_type(ax_no)) 
    case 'plot' % ------------------------------------- line plot (default)
        l_hum = eval(['plot(human_data.' time_human ...
                                          ',human_data.' data_human ');']); 
                                      
        % two human data for GRF plots only (horizontal and vertical force)                        
            if strcmp(fig_prop.legend_type, 'GRFs')
               l_hum(2) = eval(['plot(human_data.' char(plot_human(2,1)) ...
                                ',human_data.' char(plot_human(2,2)) ')']);
            end

        for i = 1:length(l_hum)
           l_hum(i).LineStyle          = ':';
           l_hum(i).Color              = [0 0 0].*(1/255);
           l_hum(i).HandleVisibility   = 'off';
        end

    case 'area' % ------------------------ area plot for muscle acitvations
        l_hum = eval(['area(human_data.' time_human ...
                                          ',human_data.' data_human ');']); 
                                      
        l_hum.FaceColor = [0 0 0];
        l_hum.FaceAlpha = 0.6;
        l_hum.LineStyle = 'none';

    case 'patch' % ------------ patch for muscle force-length relationships
      disp('plot_ax: No human reference data avaliable for patch-command');

    otherwise
        error(['plot_ax: invalid fig_prop.line_type for human data.'...
                'Must be "plot", "area" or "patch"! Check spelling!']);
  end
    
  % ----------------------------------- Diplay name for exemplary line sim1
    l_hum(1).DisplayName      = 'Human';
    l_hum(1).HandleVisibility = 'on';
    
end
cols = colororder;
%% Plot Simualtion Data 
for l_ax = 1:fig_prop.lines_per_ax(ax_no) % -------- for all lines per axis
    if ax_no == 1
        index_count = l_ax;
    else
        index_count = sum(fig_prop.lines_per_ax(1:ax_no-1)) + l_ax;
    end

    for i = 1:length(simout) % -------------- for all simulations in simout
        time_sim = plot_sim{i}{index_count,1};
        data_sim = plot_sim{i}{index_count,2};
        
        switch char(fig_prop.line_type(ax_no))
            case 'plot' % ----------------------------- line plot (default)
                    
                    l_sim = plot(time_sim, data_sim);
                    l_sim.Color = cols(i,:);
            

            case 'area' % ---------------- area plot for muscle acitvations
                l_sim = area(time_sim, data_sim);
                l_sim.FaceAlpha = 0.6/i;
                l_sim.FaceColor = cols(i,:);
                l_sim.EdgeColor = cols(i,:);
                l_sim.LineWidth = 1;
                
            case 'patch' % ---- patch for muscle force-length relationships
                Time  = simout(i).F_MTU.Time;
                F_MTU = eval(['simout(i).' char(plot_sim(ax_no,1)) ';']);
                L_MTU = eval(['simout(i).' char(plot_sim(ax_no,2)) ';']);
                
                l_sim = patch([L_MTU' nan],[F_MTU' nan],[Time' nan],...
                    'FaceColor', 'none', 'EdgeColor', 'interp',...
                    'LineWidth', options.LineWidth-1+i);
                
                % ------------------------- Indicate take off an touch down
                muscle_index = str2double(regexp(char(plot_sim(ax_no,1)),...
                    '\d*','Match'));
                patch_plot_touch_down (simout(i), muscle_index,  ax_no, i)
                
                
                % -------------------- hide xlines and double support plots
                set([DS_patch_1, DS_patch_2, x_take_off, x_heel_off, x_heel_opp],...
                    'visible','off');
                
            otherwise
                error(['plot_ax: invalid fig_prop.line_type for sim data.'...
                    'Must be "plot", "area" or "patch"! Check spelling!']);
        end

            name_lines(simout, i, l_ax, l_sim, fig_prop.line_names_ax, ax_no, fig_prop.line_style);

    end
    
end
hold off
%% xlabel to percentage
for i = 1:length(ax.XTickLabel)
    ax.XTickLabel{i}=num2str(str2double(ax.XTickLabel{i})*100);
end


%% Show Legend and Annotations
if ~isempty(fig_prop.ylim_ax)
    ylim(fig_prop.ylim_ax(ax_no,:))
end
if ~isempty(fig_prop.ylabel)
    ylabel(char(fig_prop.ylabel(:,ax_no)),'Interpreter','Latex')
end
if ~isempty(fig_prop.xlim_ax)
    xlim(fig_prop.xlim_ax(ax_no,:))
end
if ~isempty(fig_prop.xlabel)
    xlabel(char(fig_prop.xlabel(:,ax_no)),'Interpreter','Latex')
end

if ax_no == 1 && isempty(fig_prop.line_names_ax)
    show_legend(fig_prop.legend_type)
    
elseif ~isempty(fig_prop.line_names_ax)
    show_legend(fig_prop.legend_type)
end


if ~isempty(fig_prop.annots)
show_annots(fig_prop.ylim_ax(ax_no,:), char(fig_prop.annots(ax_no)))
end


%% Plot Body Weight Line for GRF plots
if strcmp(fig_prop.legend_type, 'GRFs')
    
        if options.norm_to_bw 
            y_val = 100;
            else
            y_val = fig_prop.norm_vars.mg;
        end
        
yline(y_val,'k-',{'BW'},'Interpreter','Latex','LineWidth',...
                                               1,'HandleVisibility','off'); 
end

end

