function [ax] = plot_bar(fig_prop, plot_sim)
% -------------------------------------------------------------------------
% [ax] = plot_bar(fig_prop, plot_sim)
%
% creates a bar plot to visualize FGCM work contributions
%
% -------------------------------------------------------------------------
% 
% © Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-08-2023, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

ax = nexttile;    % --------------------------------------- Create new axis
ax.Box = 'on';
ax.Layer = 'top';
ax.YMinorTick='on';
ax.YGrid = 'on';
ax.YMinorGrid = 'on';
ax.XAxisLocation='top';
ax.XLabel.Interpreter = 'Latex';
ax.XAxis.TickLength = [0 0];
ax.YGrid = 'on';
ax.YLabel.String = fig_prop.ylabel{1};

hold on
X = fig_prop.bar_X;
Y = zeros(length(X),length(plot_sim));

for i = 1:length(plot_sim)
    for j = 1:length(X)
        Y(j,i)=plot_sim{i}{j};
    end
end

% -------------------------------------------------------------- Set legend
    l_sim = bar(X,Y);
    l = legend({'1s-lA' '1s-hA' '2s-TJ'  '2s-MTJ' '3s-nW' '3s-W' '1s-Ref'});
    set(l, 'FontName', 'math','Interpreter','latex')
    l.Location = 'southeast';
    l.ItemTokenSize(1) = 15;
    l.Orientation = 'horizontal';
    l.NumColumns = 4;
end

