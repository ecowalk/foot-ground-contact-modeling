function [ds_patch_1, ds_patch_2] = plot_double_support (ylim_ax, simout)
arguments
    ylim_ax (1,2) double 
    simout  (1,1) Simulink.SimulationOutput
end
% -------------------------------------------------------------------------
% [ds_patch_1, ds_patch_2] = plot_double_support (ylim_ax, simout) 
%
% creates gray areas (patch handle ds_patch_*) in the figures to indicate 
% double support phases in beginning and mid of stride
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    11-05-2023, Munich 
% 
% -------------------------------------------------------------------------

heel_toe = simout.Heel_Toe.Data;
time = simout.Heel_Toe.Time;
first_toe_off_R = find((diff(heel_toe(:,6))~=0)~=0);
first_toe_off_R = first_toe_off_R(1);
[CI_L, CI_R, ~, ~] = get_CI(simout.GRFs.Data(:,2:2:16), 1, 10);

% ---------------------------------------------- first double support phase
x      = [time(1)   time(first_toe_off_R)...
          time(first_toe_off_R)   time(1)];

% --------------------------------------------- second double support phase
x2     = [time(CI_R(1,1))   time(CI_L(1,2))...
          time(CI_L(1,2))   time(CI_R(1,1))];

% --------------------------------- y-axis boundaries to fill entire figure  
y      = [ylim_ax(1,1)   ylim_ax(1,1)...
          ylim_ax(1,2)   ylim_ax(1,2)];

% ------------------------------------- patch double support phases in gray
ds_patch_color  = [240 240 240]*(1/255);

ds_patch_1      = patch(x,y,ds_patch_color);
ds_patch_2      = patch(x2,y,ds_patch_color);

set([ds_patch_1,ds_patch_2],'LineStyle','none','HandleVisibility','off');
end