function [ds_patch_1, ds_patch_2] = plot_double_support_human (ylim_ax, DS)
arguments
    ylim_ax (1,2) double 
    DS  (3,1) double
end
% -------------------------------------------------------------------------
% [ds_patch_1, ds_patch_2] = plot_double_support (ylim_ax, simout) 
%
% creates gray areas (patch handle ds_patch_*) in the figures to indicate 
% double support phases in beginning and mid of stride
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    13-07-2023, Munich 
% 
% -------------------------------------------------------------------------



% ---------------------------------------------- first double support phase
x      = [0   DS(1)...
          DS(1)   0];

% --------------------------------------------- second double support phase
x2     = [DS(2)   DS(3)...
          DS(3)   DS(2)];

% --------------------------------- y-axis boundaries to fill entire figure  
y      = [ylim_ax(1,1)   ylim_ax(1,1)...
          ylim_ax(1,2)   ylim_ax(1,2)];

% ------------------------------------- patch double support phases in gray
ds_patch_color  = [240 240 240]*(1/255);

ds_patch_1      = patch(x,y,ds_patch_color);
ds_patch_2      = patch(x2,y,ds_patch_color);

set([ds_patch_1,ds_patch_2],'LineStyle','none','HandleVisibility','off');
end