function [] = plot_tiledlayout(fig_prop, options, simout, plot_sim, ...
                                                  human_data, plot_human)
arguments
    fig_prop (1,1) struct
    options  (1,1) struct
    simout          Simulink.SimulationOutput
    plot_sim        cell 
    human_data      struct = []
    plot_human      cell   = {}
end
% -------------------------------------------------------------------------
% [] = plot_tiledlayout(fig_prop, options, simout,plot_sim, ...
%                                                   human_data, plot_human)
%
% creates new figure with tiled layout based on specifications defined in 
% def_figures: sets colororder, number of axes, calls plot_axis, links x 
% axes and saves figure if save_flag
%
% for patch plots the colorbar is shown and with option patch_focus the 
% subplot on a defined zoom zone is created 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

%% Init and Set Colors
set_colors(options);
ax      = matlab.graphics.axis.Axes();

if fig_prop.lines_per_ax == 1
    no_ax   = size(plot_sim{1},1);
else
    no_ax = length(fig_prop.lines_per_ax);
end



% ----- Dublicate line_type and annots if only specified once for all plots 
    if length(fig_prop.line_type) < no_ax
        fig_prop.line_type   = repmat(fig_prop.line_type, no_ax, 1);
    end
    
    if size(fig_prop.xlim_ax,1) < no_ax
        fig_prop.xlim_ax   = repmat(fig_prop.xlim_ax, no_ax, 1);
    end

    if length(fig_prop.annots) < no_ax
        fig_prop.annots      = repmat(fig_prop.annots, no_ax, 1);
    end
    
    if length(fig_prop.lines_per_ax) < no_ax
        fig_prop.lines_per_ax = repmat(fig_prop.lines_per_ax, no_ax, 1);
    end

%% New Figure with Tiledlayout 
h              = figure('Name',fig_prop.name);
h.Position     = fig_prop.format;
h.Color        = 'w';
t              = tiledlayout(no_ax,1);
t.TileSpacing  = 'compact';
t.Padding      = 'tight';
t.XLabel.Interpreter = 'latex';
t.XLabel.FontSize=10;
t.XLabel.String       = 'Stride $[ \% $ ]';

% --------------------------------------------------------------- Plot axes

for i = 1:no_ax
    if fig_prop.line_type{i} == "bar"
        ax(i) = plot_bar( fig_prop, plot_sim);
        t.XLabel.String       = '';
    else
        ax(i) =  plot_axis(i, fig_prop, options, simout,     plot_sim, ...
                                         human_data, plot_human); 
    end
    
end
for i = 1:no_ax
    %% don't show lines with no data
    l = findobj(ax,'Type','line');
    for j = 1:length(l)
        if ~any(l(j).YData)
            l(j).XData = NaN;
            l(j).YData = NaN;
        end
    end
end
    
%% Save Plot
save_plot (h, fig_prop.save_name, h.Position(3:4), options);
end

