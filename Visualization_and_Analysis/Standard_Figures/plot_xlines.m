function [x_take_off, x_heel_off, x_heel_opp] = plot_xlines(simout)
arguments
    simout                    Simulink.SimulationOutput
end
% -------------------------------------------------------------------------
% [] = plot_xlines(simout)
%
% creates vertical lines for heel off, opposite heel touch down and take 
% off for an arbitrary number of simulation outputs in simout
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

    for j = 1:length(simout) 

        x_heel_off = xline(simout(j).heel_off_norm, ...
                           'LineStyle','-.', 'Label','heel\,off');
                        
        x_heel_opp = xline(simout(j).heel_opp_norm, ...
                           'LineStyle','-.', 'Label','opp.\,heel\,strike');                          
        
        x_take_off = xline(simout(j).take_off_norm, ...
                           'LineStyle','--', 'Label','toe\,off');

        set([x_take_off, x_heel_off, x_heel_opp],... 
                                'LineWidth',1,...
                                'HandleVisibility','off',...
                                'LabelVerticalAlignment','top',...
                                'LabelHorizontalAlignment','left',...
                                'Interpreter','Latex')    
    end
end