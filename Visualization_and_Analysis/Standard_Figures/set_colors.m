function [] = set_colors(options)
arguments
    options 
end
% -------------------------------------------------------------------------
% [] = set_colors(options) 
%
% sets default values for:  
%       * color     (DefaultAxesColorOrder, DefaultFigureColormap), 
%       * linewidth (DefaultLineLineWidth)
%       * units     (DefaultFigureUnits)
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-12-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

[TUMColors, TUMColormap] = def_TUM_colors();
[Colors] = def_colors();

if options.TUMColors == 1
    set(0,'DefaultAxesColorOrder',TUMColors);
    set(0,'DefaultFigureColormap',TUMColormap);
elseif options.TUMColors == 2
    set(0,'DefaultAxesColorOrder',Colors);
else
    set(0,'DefaultAxesColorOrder','default');
end
set(0, 'DefaultLineLineWidth', options.LineWidth);
set(groot,'DefaultFigureUnits','centimeters');
end

