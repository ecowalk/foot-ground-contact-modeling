function [] = show_annots(ylim_ax, annot_joint)
arguments
   ylim_ax           double
   annot_joint (1,:) char   {mustBeMember(annot_joint,{'HipKnee', 'Ankle', 'none'})}
end
% -------------------------------------------------------------------------
% [] = show_annots(ylim_ax, annot_joint, options) 
%
% creates annotations to indicate the direction of flexion and extension 
% for each individual axis
% 
% options for annot_joint: 
%               * 'HipKnee': shows "Extension" and "Flexion"
%               * 'Ankle':   shows "Plantarflexion" and "Dorsiflexion"
%               * 'none':    shows no annotations
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

    x_shift    = 0.01;
    y_shift(1) = (ylim_ax(1)+(ylim_ax(2)-ylim_ax(1))*0.03);
    y_shift(2) = (ylim_ax(2)-(ylim_ax(2)-ylim_ax(1))*0.03); 
    
switch strtrim(annot_joint)
    case 'HipKnee'
    txt(1) = text(x_shift, y_shift(1),'Extension');
    txt(2) = text(x_shift, y_shift(2),'Flexion');
   
    case 'Ankle'
    txt(1) = text(x_shift, y_shift(1),'Plantarflexion');
    txt(2) = text(x_shift, y_shift(2),'Dorsiflexion');
    
    case 'none' % ------------------------------------------- no annotation 
    txt(1) = text();
    txt(2) = text();
    
    otherwise 
    error(['show_annots: invalid joint option. '...
           'Supported options are ´HipKnee´, ´Ankle´ or ´none´. ' ...
           'Check spelling or call help show_annots for more information.']);
end

set(txt(1),'VerticalAlignment','bottom');
set(txt(2),'VerticalAlignment','top');

set(txt,'Interpreter','Latex',...
        'Color','black','BackgroundColor',[180 180 180]*1/255,...
        'FontWeight','bold');

end