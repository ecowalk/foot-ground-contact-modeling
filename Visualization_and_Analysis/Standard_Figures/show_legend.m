function [] = show_legend(legend_type)
arguments
    legend_type (1,:) char {mustBeMember(legend_type,{'standard','GRFs','double','nohuman','reduced'})}
end
% -------------------------------------------------------------------------
% [] = show_legend(legend_type) 
%
% displays legend and sets legend properties location, orientation, 
% numcolumns, interpreter. 
%
% legend_type options:
%           * 'standard'
%           * 'GRFs'
%           * 'double'
%           * 'nohuman'
%           * 'reduced'
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------


switch legend_type
    case 'standard' % -------------------------------------- No adaptations
        ax = gca;
        l = legend(ax,{'human' '1s-lA' '1s-hA' '2s-TJ'  '2s-MTJ' '3s-nW' '3s-W' '1s-Ref'});
        l.Location = 'northoutside';
        l.Orientation = 'horizontal';
        l.NumColumns = 4;
        l.ItemTokenSize(1) = 15;
        
    case 'nohuman' % ---------------------------- Legend without human data 
        ax = gca;
        l=legend(ax,{'1s-lA' '1s-hA' '2s-TJ'  '2s-MTJ' '3s-nW' '3s-W' '1s-Ref'});
        l.Location='northoutside';
        l.Orientation = 'horizontal';
        l.NumColumns = 4;
        l.ItemTokenSize(1)=15;
        
    case 'reduced' % ------------------------------ For non-rigid feet only
        ax = gca;
        p = ax.Children;
        l = legend(p(length(p)-2:-1:length(p)-5),{'2s-TJ'  '2s-MTJ' '3s-nW' '3s-W'});
        l.Location = 'northoutside';
        l.Orientation = 'horizontal';
        l.NumColumns = 4;
        l.ItemTokenSize(1) = 15;
        
    case 'double' % ------------------------------- For non-rigid feet only
        ax = gca;
        p = ax.Children;
        l=legend(p(length(p)-2:-1:length(p)-5),{'2s-TJ' '2s-MTJ' '3s-nW' '3s-W'});
        l.Orientation = 'horizontal';
        l.NumColumns = 4;
        l.Location = 'northoutside';
        l.ItemTokenSize(1) = 15;
        r = groot;
        r.ShowHiddenHandles = 1;
        ax2 = copyobj(ax,gcf);
        delete(get(ax2, 'Children') )
        r.ShowHiddenHandles = 0;
        
        % ---------------------- Plot helper data, but invisible for legend
        hold(ax2,'on')
        H1 = plot(ax2,NaN,NaN, '-', 'Color', [0 0 0]);
        H2 = plot(ax2,NaN,NaN, '--', 'Color', [0 0 0]);
        H3 = plot(ax2,NaN,NaN, '-.', 'Color', [0 0 0]);
        hold(ax2,'off')
        
        % -------------------------------------- Make second axes invisible
        set(ax2, 'Color', 'none', 'XTick', [], 'YAxisLocation', 'right', 'Box', 'Off', 'Visible', 'off')
        
        % ---------------------------------------------- Add legend entires
        ax2.OuterPosition = ax.OuterPosition;
        ax2.InnerPosition = ax.InnerPosition;
        lgd2 = legend(ax2, {'Total', 'TJ','PF + PL'});
        lgd2.Location = 'northwest';
        lgd2.ItemTokenSize(1) = 15;
        
    case 'GRFs'
        ax = gca;
        l = legend(ax,{'human' '1s-lA' '1s-hA' '2s-TJ'  '2s-MTJ' '3s-nW' '3s-W' '1s-Ref'});
        l.Orientation = 'horizontal';
        l.NumColumns = 4;
        l.Location = 'northoutside';
        l.ItemTokenSize(1) = 15;
        r = groot;
        r.ShowHiddenHandles = 1;
        ax2 = copyobj(ax,gcf);
        delete(get(ax2, 'Children') )
        r.ShowHiddenHandles = 0;
        
        % ---------------------- Plot helper data, but invisible for legend
        hold(ax2,'on')
        H1 = plot(ax2,NaN,NaN, '-', 'Color', [0 0 0]);
        H2 = plot(ax2,NaN,NaN, '--', 'Color', [0 0 0]);
   
        hold(ax2,'off')
        % -------------------------------------- Make second axes invisible
        set(ax2, 'Color', 'none', 'XTick', [], 'YAxisLocation', 'right', 'Box', 'Off', 'Visible', 'off')
        
        ax2.OuterPosition = ax.OuterPosition;
        ax2.InnerPosition = ax.InnerPosition;
        lgd2 = legend(ax2, {'$F_v$', '$F_h$'});
        lgd2.Location = 'northeast';
        lgd2.ItemTokenSize(1) = 15;
        
    otherwise
        error(['show_legend: invalid legend_type. '...
            'Supported options are ´standard´ or ´GRFs´. ' ...
            'Check spelling or call help show_legend for more information.']);
end
end