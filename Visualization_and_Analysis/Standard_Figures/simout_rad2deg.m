function [simout] = simout_rad2deg(simout)
% -------------------------------------------------------------------------
% [simout] = simout_rad2deg(simout) 
%
% converts ankle, knee and hip angle to degree according to ISB convention 
% for flexion and extension
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       01-12-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

for i = 1:length(simout)
% ------------------------------------------------------------------- ankle
simout(i).Dyn.Data(:,1)   = rad2deg(simout(i).Dyn.Data(:,1))*(-1)+90; 

% -------------------------------------------------------------------- knee
simout(i).Dyn.Data(:,3) = rad2deg(simout(i).Dyn.Data(:,3))*(-1)+180; 

% --------------------------------------------------------------------- hip
simout(i).Dyn.Data(:,5) = rad2deg(simout(i).Dyn.Data(:,5))*(-1)+180; 

% --------------------------------------------------------------------- MTJ
simout(i).Foot_Kinematics_L.MTJ_phi.Data = rad2deg(simout(i).Foot_Kinematics_L.MTJ_phi.Data);

% --------------------------------------------------------------------- MTPJ
simout(i).Foot_Kinematics_L.MTPJ_phi.Data = rad2deg(simout(i).Foot_Kinematics_L.MTPJ_phi.Data);
end

end

