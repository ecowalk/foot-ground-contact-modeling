function [out] = data_preprocess (fpath, res_var, step_no, filter_var)
arguments 
    fpath     (1,:) char
    res_var   (1,:) char
    step_no   (1,1) double 
    filter_var      struct
end
% -------------------------------------------------------------------------
% [out] = data_preprocess (fpath, res_var, step_no, filter)
% Preprocesses output data from the neuromuscular simulation to prepare 
% data for plotting and cross correlation analysis
% 
% Inputs:
%   - fpath      (char)    - directory where data file is saved 
%   - res_var    (char)    - naming of matlab variable in workspace
%   - step_no    (double)  - step number (of model) used for analysis
%   - filter_var (struct)  - passband frequency of GRFs-lowpass filter
%                            [Hz] and filter flag
% 
% -------------------------------------------------------------------------
% PreProcessing steps:
%       1) Low pass filter vertical and horizontal GRFs
%       2) Limit data to one stride (evaluated step = step_no) & Calculate 
%          duty factor (stance time/stride time)
%       3) Normalize time series for all data to % of stride
%       
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann and Simon Wenzler, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initialized:      20-08-2021, Munich
% Last modified:    12-12-2023, Munich 
% 
% -------------------------------------------------------------------------

%% Load data set & variables ----------------------------------------------
out = load(fpath,res_var);
eval(['out = out.' res_var ';']);

% ------------------------------------------------------ Get variable names
vars = who(out); 

%% 1. Filter GRF data
if filter_var.flag
    Fs_grfs = 1/(out.GRFs.Time(2) - out.GRFs.Time(1)); %Get sample frequency
    [b,a] = butter(1,filter_var.Fpass/(Fs_grfs/2));
    out.GRFs.Data=filtfilt(b,a,out.GRFs.Data);
end

%% 2. Limit Data to one Step (step_no) ------------------------------------
% Stride time: heel to heel, steady state walking; model starts on left leg

% ----------------------- Find changes of stance signal - CI = change index
% CI: first column: stance starts; second column stance ends; rows: step_no
% CI_heel: first column: heel on; second column: heel off; rows: step_no

% ------------------------ Only vertical grfs needed; cut = 1N thresh = 10N
[CI_L,CI_R,CI_heel_L,~] = get_CI(out.GRFs.Data(:,2:2:16), 1, 10); 

% step_no == -1: search for last possible step; otherwise use defined step
if step_no == -1
    step_no = length(CI_L)-1;
elseif step_no > length(CI_L)-1
    error("selected step_no is too big")
end

% -------------------- Calculate start and end time of stride & duty factor
out.start_time  = out.Heel_Toe.Time(CI_L(step_no,1));
out.take_off    = out.Heel_Toe.Time(CI_L(step_no,2));
out.end_time    = out.Heel_Toe.Time(CI_L(step_no+1,1));

out.stride_time = out.end_time  - out.start_time;
out.DF          = (out.take_off - out.start_time)/out.stride_time;

% --------------------------------------- Heel off and opposite heel strike
out.heel_off = out.Heel_Toe.Time(CI_heel_L(step_no,2));
                 
out.heel_opp = out.Heel_Toe.Time(CI_R(...
       find(CI_R(:,1) > CI_L(step_no,1) & CI_R(:,1) < CI_L(step_no,2),1)));


% --------------------------- Start index and final index to delete samples 
I_s             = (1:(CI_L(step_no,1)-1));
I_f             = ((CI_L(step_no+1,1)+1):length(out.Heel_Toe.Data));

assert(max(I_s) < min(I_f), ['Data_PreProcess: Step extraction failed. '...
                             'Check out.Heel_Toe.Data for irregularities.']);

% -------------------------------- Limit timeseries data to step_no strides
for i =1:length(vars)
    name = char(strcat('out.',vars(i)));
    if isa(eval(name),'timeseries') && any(strfind(name,'Metab')==5)
        % Fix for metabolic energy
        indx1 = eval(['1:find(' name '.Time>=out.start_time,1)-1']);
        indx2 = eval(['find(' name '.Time<=out.start_time+out.stride_time,1,''last'')+1 : length(' name '.Time)']);
        eval([name '= del_sample(' name ', [indx1, indx2]);']);
        
    elseif isa(eval(name),'timeseries')
        eval([name '= del_sample(' name ', [I_s, I_f]);']);
    end
    
    if isa(eval(name),'struct')
        vars_temp = fieldnames(eval(name));
        for j =1:length(vars_temp)
            name_temp = char(strcat(name,'.',vars_temp(j)));
            if isa(eval(name_temp),'timeseries')
                eval([name_temp '= del_sample(' name_temp ', [I_s, I_f]);']);
            end
        end
    end
end


%% 3. Normalize Time for All Data to % of Stride -------------------
for i =1:length(vars)
    name = char(strcat('out.',vars(i)));
    if isa(eval(name),'timeseries')
        eval([name '.Time = (' name '.Time - out.start_time)/out.stride_time;']);
    end
    
    if isa(eval(name),'struct')
        vars_temp = fieldnames(eval(name));
        for j =1:length(vars_temp)
            name_temp = char(strcat(name,'.',vars_temp(j)));
            if isa(eval(name_temp),'timeseries')
                eval([name_temp '.Time = (' name_temp '.Time - out.start_time)/out.stride_time;']);
            end
        end
    end
end

out.take_off_norm = (out.take_off -out.start_time)/out.stride_time;
out.heel_off_norm = (out.heel_off -out.start_time)/out.stride_time;
out.heel_opp_norm = (out.heel_opp -out.start_time)/out.stride_time;

out.VelHAT_mean   = mean(out.VelHAT.Data);                                    
out.stride_length = out.VelHAT_mean(1)*out.stride_time;                     
out.MetabEnergy_Umberger = trapz(out.MetabPower.Time, out.MetabPower.Data); 

%% Filter and process CoP
out.COP.Data(isnan(out.COP.Data))=0;
if isa(eval('out.Foot_pos_L'),'timeseries')
    temp_cop = out.COP.Data(:,2);
    
    temp_cop(temp_cop==0) = out.Foot_pos_L.Data(1,1);
    temp_cop              = temp_cop - out.Foot_pos_L.Data(1,1);
    
    out.COP.Data(:,2) = temp_cop;
end

if filter_var.flag
    Fs_cop = 1/(out.COP.Time(2)-out.COP.Time(1));
    [b,a]  = butter(1,filter_var.Fpass/(Fs_cop/2));
    
    out.COP.Data=filtfilt(b,a,out.COP.Data);
end
end
