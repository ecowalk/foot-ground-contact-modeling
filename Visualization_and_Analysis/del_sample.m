function [out] = del_sample(in, index)
    arguments 
        in      timeseries
        index   double 
    end
% -------------------------------------------------------------------------
% [out] = del_sample(in, index)
% 
% removes samples from the timeseries object IN. 
% INDEX specifies the indices of the TS time vector that correspond to the
% samples you want to remove.
%  
% Uses the function TS = delsample(TS,'Index',VALUE) 
% 
% Inputs:
%   - in     timeseries  
%   - index  double      
%
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       02-08-2022, Munich
% Last modified:    08-12-2022, Munich 
% 
% -------------------------------------------------------------------------

        out  =  delsample(in, 'Index', index);
end


