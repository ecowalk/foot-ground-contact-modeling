function [CI_L, CI_R, CI_heel_L, CI_heel_R ] = get_CI(grfs,cut,thresh)
arguments
    grfs   (:,8) double % GRFs in y direction [l_sum, l_heel, l_ball, l_toe, r_sum ...]
    cut    (1,1) double
    thresh (1,1) double
end
% -------------------------------------------------------------------------
% [CI_L, CI_R, CI_heel_L, CI_heel_R] = get_CI(grfs,cut,thresh)
% 
% Extract stance phases from vertical GRFs. A valid stance phase is defined
% when the GRFs at the heel, ball, and toe contact elements simultaneously 
% exceed the threshold value. This ensures that slight "scrapes" are ignored.
%
% To find the beginning of a valid stance phase, we search for the first 
% time step (starting from a found "valid" time step and going back) where 
% the GRF of the ball CE exceeds the cut value for the first time.
% 
% The same principle is used to find the end of a valid swing phase, where 
% the total GRFs must fall below the cut value.
%
% -------------------------------------------------------------------------
%
% © Simon Wenzler, Chair of Applied Mechanics, TUM
%
% -------------------------------------------------------------------------
%
% Initialized:      16-09-2023, Munich
% Last modified:    12-12-2022, Munich by Alexandra Buchmann
%
% -------------------------------------------------------------------------

% ----------------------- Change GRFs indicees for feet without toe element
if ~any(any(grfs(:,4:4:8))) 
    grfs(:,4) = grfs(:,3);
    grfs(:,8) = grfs(:,7);
end

% ----------------------------- Find timesteps where grfs exceed thresh/cut
log_step = grfs > thresh;
log_cut = grfs > cut;
CI = cell(2,4);

for i = 0:1
    % --- Find valid steps: heel, ball and toe exceed thresh simulatanously
    temp   = diff(sum(log_step(:, 2+4*i : 4+4*i),2)==3);
    step_s = find(temp==1);
    step_e = find(temp==-1);
    
    % ------------- Fix start index when contact is true from timestep zero
    if step_s(1) >= step_e(1)
        step_s = [0; step_s];
    end
    
    % --------------------- Fix last index for steps which are not finished
    if length(step_s) - length(step_e) == 1
        step_e = [step_e; size(grfs,1)];
    end
    
    if length(step_s) ~= length(step_e)
        error("sth went wrong in get_CI")
    end
    
    % From valid stance: find first timestep where GRFss exceed or fall 
    % below cut-threshold to find start (ind_s) and end (ind_e) of stance
    for k = 1:4
        temp = diff(log_cut(:,k+4*i));
        for j=1:length(step_s)
            ind_s = find(temp(1:step_s(j))== 1, 1,'last');
            ind_e = find(temp(step_e(j):end) == -1, 1, 'first') + step_e(j) - 1;
            
            if isempty(ind_e) && j == length(step_s)
                ind_e = size(grfs,1);
            elseif isempty(ind_e) && j ~= length(step_s)
                error("sth went wrong in get_CI")
            end
            
            if isempty(ind_s) && j == 1
                ind_s = 1;
            elseif isempty(ind_e) && j ~= 1
                error("sth went wrong in get_CI")
            end
            
            CI{i+1,k}(j,:) = [ind_s, ind_e];
        end
    end
end

% CI contains all indicees combined;structure of CI: 2x4 cell array CI{a,b}
%   - a: left foot (1), right foot (2)
%   - b: CEs with sum (1), heel (2), ball (3) and toe (4)
% Each cell is filled with an nx2 matrix containing the start(n,1) and
% end(n,2) indices of the valid stance phases 

% CI_L, CI_R, CI_heel_L and CI_heel_R are the seperated outputs
% Start index uses grfs of ball ce to robustly synchronise grfs
CI_L = [CI{1,3}(:,1) CI{1,1}(:,2)]; 
CI_R = [CI{2,3}(:,1) CI{2,1}(:,2)];

CI_heel_L = [CI{1,2}(:,1) CI{1,2}(:,2)];
CI_heel_R = [CI{2,2}(:,1) CI{2,2}(:,2)];
end
