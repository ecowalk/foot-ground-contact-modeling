function [] = save_plot (h_no, save_name, paper_size, options) 
arguments
    h_no        (1,1) matlab.ui.Figure
    save_name   (1,:) char
    paper_size  (1,2) double      
    options     (1,1) struct 
end
% -------------------------------------------------------------------------
% [] = save_plot (h_no, save_name, paper_size, options)
%
% saves figure h_no with the name ['save_name options.save_name'] in fpath
% 
% the figures font size is set to options.FontSize and paper_size defines
% the size of the saved figure.
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       28-11-2021, Munich
% Last modified:    02-12-2022, Munich 
% 
% -------------------------------------------------------------------------

if options.save_flag
    set(findall(gcf,'-property','FontSize'),'FontSize',options.FontSize)
    set(h_no,'PaperSize',paper_size);                           
    set(h_no, 'Renderer', 'painters');
    %
    name = [save_name options.save_name];
    print(h_no,fullfile(options.fpath,name),'-dpdf','-bestfit');
    saveas(h_no,fullfile(options.fpath,name),'fig');
end
        
end