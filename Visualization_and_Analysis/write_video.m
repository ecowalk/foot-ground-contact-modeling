function [] = write_video(fullname, frames, framerate)
arguments
    fullname  (1,:) char
    frames    (1,:) struct
    framerate (1,1) double
end
% -------------------------------------------------------------------------
% [] = write_video(fullname, frames, framerate)
%
% writes a video of all frames of the plot with framesrate in mp4 format
% 
% -------------------------------------------------------------------------
% 
% © Alexandra Buchmann, Chair of Applied Mechanics, TUM
% 
% -------------------------------------------------------------------------
% 
% Initiaized:       08-12-2021, Munich
% Last modified:    08-12-2022, Munich 
% 
% -------------------------------------------------------------------------

    % --------------------------------------------- Create the video writer
    writerObj           = VideoWriter([fullname '.mp4'], 'MPEG-4');
    writerObj.FrameRate = framerate; 

    open(writerObj);
    % --------------------------------------- Write the frames to the video
        for i=1:length(frames)
            % -------------------------------- Convert the image to a frame
            frame = frames(i) ;    
            writeVideo(writerObj, frame);
        end
        
    close(writerObj);
    close all
    
end

